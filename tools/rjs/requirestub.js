var modules = {};

function define(id, dependencies, definition) {
  if (typeof id !== 'string') {
    throw 'invalid module definition, module id must be defined and be a string';
  }

  if(typeof dependencies === 'function') {
    definition = dependencies;
    dependencies = [];
  }

  if (dependencies === undefined) {
    throw 'invalid module definition, dependencies must be specified';
  }

  if (definition === undefined) {
    throw 'invalid module definition, definition function must be specified';
  }

  require(dependencies, function() {
    modules[id] = definition.apply(null, arguments);
  });
}

function require(ids, callback) {
  var module, defs = [];

  for (var i = 0; i < ids.length; ++i) {
    module = modules[ids[i]];
    if (!modules.hasOwnProperty(ids[i])) {
      throw 'module definition dependency not found: ' + ids[i];
    }

    defs.push(module);
  }

  callback.apply(null, defs);
}
