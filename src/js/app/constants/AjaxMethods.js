'use strict';
define('app/constants/AjaxMethods', function() {
  return {
    GETTOKEN: 'authGetToken',
    GETSESSION: 'getSession',
    GETGENRES: 'getGenres',
    GETBOOKS: 'getBooks',
    GETMYBOOKS: 'getUserBooks',
    GETLANGS: 'getAvailableLanguagesForBooks',
    GETNEWBOOKS: 'getBooks', //TODO: set original method
    GETBOOKINFO: 'getBookInfo',
    SEARCH: 'getSearchResults'
  }
});
