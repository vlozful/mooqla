'use strict';
define('app/services/AuthService',
    ['app/core/AngularClass',
      'app/modules/api/ApiService',
      'app/services/SessionService'],
    function(AC, ApiService, SessionService) {

      var $apiService, $sessionService, $q;

      return AC.extend('AuthService', {
        constructor: function(_$ApiService, _$SessionService, _$q) {
          $apiService = _$ApiService;
          $sessionService = _$SessionService;
          $q = _$q;
        },

        login: function(email, password) {
          return $apiService.login(email, password);
        },

        register: function(email, password, confirm, country) {
          return $apiService.register(email, password, confirm, country);
        },

        isLoggedIn: function() {
          if ($sessionService.ssid) {
            if ($sessionService.userID) return true;
            else {
              var deferred = $q.defer();
              deferred.reject($sessionService.ssid);
              return deferred.promise;
            }
          } else {
            return $sessionService.load();
          }
        }
      }).inject(ApiService.fullName, SessionService.fullName, '$q');
    });
