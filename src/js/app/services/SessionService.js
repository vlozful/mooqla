'use strict';
define('app/services/SessionService', ['app/core/AngularClass', 'app/modules/api/ApiService'], function(AC, ApiService) {

  var $apiService;

  return AC.extend('SessionService', {

    userID: '',

    ssid: '',

    languageID: '',

    constructor: function(_$apiService) {
      $apiService = _$apiService;
    },

    load: function() {
      var self = this,
          request = $apiService.getSession();

      request.then(function(data) {
        self.create(data);
        return data;
      }, function(error) {
        return error;
      });

      return request;
    },

    create: function(data) {
      this.userID = data['idUser'];
      this.languageID = data['idLanguage'];
      this.ssid = data['ssid'];
    },

    reset: function() {
      this.userID = this.idLanguage = this.ssid = '';
    }
  }).inject(ApiService.fullName);
});
