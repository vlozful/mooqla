'use strict';
define('app/modules/api/ApiService',
    ['app/core/AngularClass', 'app/modules/api/HttpService', 'app/models/Genre', 'app/models/Book', 'app/models/Language'],
    function(AC, HttpService, GenreModel, BookModel, LanguageModel) {

      var $httpService,
          GENREBOOKS = 1;

      function createLoginRequestData(email, password) {
        return {
          'email': email,
          'password': password
        }}

      function createRegisterRequestData(email, password, passwordConfirm, country) {
        return {
          'email': email,
          'password': password,
          'password1': passwordConfirm,
          'country': country
        }}

      function createBookListRequestData(offset, count, genre_id, lang_id, in_minimart) {
        var result = {
          'offset': offset,
          'count': count,
          'idGenresList': genre_id,
          'idLanguagesList': lang_id,
          'canBeUsedInMinimart': in_minimart,
          'epub': 1
        };

        angular.forEach(result, function(value, key) {
          if (!value) delete result[key];
        });

        return result;
      }

      function createBookDetailsRequest(book_id) {
        return {
          'idBook': book_id
        }}

      function createSearchRequest(searchString) {
        return {
          'searchString': searchString
        }}

      function parseBookResponse(data) {
        var result = [];
        data.forEach(function(value) {
          result.push(new BookModel(value));
        });
        return result;
      }

      return AC.extend('ApiService', {

        constructor: function(_$httpService) {
          $httpService = _$httpService;
          if (window['gSettings']['DEVELOPER']) {
            window['$httpService'] = $httpService;
            window['$apiService'] = this;
          }
        },

        getSession: function() {
          return $httpService.request('sessionsSelect');
        },

        login: function(email, password) {
          return $httpService.submit('g-themes-webapp-forms-login', createLoginRequestData(email, password));
        },

        register: function(email, password, passwordConfirm, country) {
          return $httpService.submit('g-themes-webapp-forms-register', createRegisterRequestData(email, password, passwordConfirm, country));
        },

        getUserBooks: function(start, end) {
          return $httpService.request('booksSelect', createBookListRequestData(0, 20));
        },

        getNewBooks: function(start, end) {
          return $httpService.request('getNewBooks', createBookListRequestData(start, end));
        },

        getGenresList: function() {
          function parseResponse(data) {
            var result = [];
            data.forEach(function(value) {
              result.push(new GenreModel(value));
            });
            return result;
          }
          return $httpService.request('genresSelect', {'countBooks': 1, 'showEmpty': 0, 'canBeUsedInMinimart': 1, 'epub': 1}).then(parseResponse);
        },

        getBooksByGenre: function(start, end, genre_id) {
          return $httpService.request('booksSelect', createBookListRequestData(start, (end - start), genre_id, null, true)).then(parseBookResponse);
        },

        getBookDetails: function(book_id) {
          function parseResponse(data) {
            if (angular.isArray(data)) {
              return new BookModel(data[0]);
            }
            return null;
          }
          return $httpService.request('booksSelect', createBookDetailsRequest(book_id)).then(parseResponse);
        },

        getBookLanguages: function() {
          function parseResponse(data) {
            var result = [];
            data.forEach(function(value) {
              result.push(new LanguageModel(value));
            });
            return result;
          }
          return $httpService.request('languagesSelect', {'countBooks': 1, 'showEmpty': 0, 'canBeUsedInMinimart': 1, 'epub': 1}).then(parseResponse);
        },

        getBooksByLanguage: function(start, end, language_id) {
          return $httpService.request('booksSelect', createBookListRequestData(start, (end - start), null, language_id, true)).then(parseBookResponse);
        },

        getSearchResults: function(searchString) {
          return $httpService.requestWithToken('getSearchResults', createSearchRequest(searchString));
        },

        getFile: function(path, progressCallback, type) {
          return $httpService.requestFile(path, progressCallback, type);
        },

        getPreviewEpubLink: function(book_id) {
          return $httpService.requestWithToken('booksPreview', {'epub': 1, 'mobi': 0, 'idBook': book_id});
        },

        getFullEpubLink: function(book_id) {
          return $httpService.requestWithToken('booksDownload', {'epub': 1, 'mobi': 0, 'idBook': book_id});
        },

        getEpub: function(path, progressCallback) {
          return $httpService.requestFile(path, progressCallback, 'arraybuffer');
        },

        test: function() {
          return $httpService.request('testFunction');
        }

      }).inject(HttpService.fullName);
    });
