'use strict';
define('app/modules/api/HttpService', ['app/core/AngularClass', 'app/constants/Events'], function(AC, Events) {

  var tokens = [],
      $http,
      $q,
      $rootScope,
      $spinService,
      gSettings = window['gSettings'],
      host = (gSettings !== undefined && gSettings['backendURL'] !== undefined) ? gSettings['backendURL'] : document.location.protocol +
          '//' + document.location.host,
      //ajaxURL = host + '/ajax.php?selectTheme=webapp',
      ajaxURL = host + '/ajax.php',
      formURL = host + '/forms.php',
      isDeveloper = gSettings['DEVELOPER'];

  function urlEncode(data) {
    var str = [];
    for (var p in data)
      str.push(encodeURIComponent(p) + '=' + encodeURIComponent(data[p]));
    return str.join('&');
  }

  return AC.extend('HttpService', {

    _$http: '',

    _$q: '',

    showSpinner: true,

    startStopSpinner: function(start) {
      var method = start ? 'spin' : 'stop';
      if (this.showSpinner) $spinService[method]('http-spinner');
    },

    constructor: function(_$rootScope, _$http, _$q, _spinService) {
      $http = _$http;
      $q = _$q;
      $spinService = _spinService;
      $rootScope = _$rootScope;
    },

    getToken: function() {
      var token = tokens.pop(),
          deferred = $q.defer();
      if (token) {
        deferred.resolve(token);
        return deferred.promise;
      } else {
        return this._doAJAX('authGetToken', {});
      }
    },

    request: function(classMethod, parameters, withToken) {
      var self = this;
      self.startStopSpinner(true);
      parameters = parameters || {};

      if (withToken) {
        return self.getToken().then(
            function(token) {
              return self._doAJAX(classMethod, angular.extend(parameters, {'token': token}));
            }
        );
      } else {
        return this._doAJAX(classMethod, parameters);
      }
    },

    requestWithToken: function(classMethod, parameters) {
      return this.request(classMethod, parameters, true);
    },

    requestFile: function(path, progressCallback, type) {
      this.startStopSpinner(true);
      return this._doAJAX('_getFile', {path: path, type: type || 'blob', onProgress: progressCallback});
    },

    submit: function(formName, parameters) {
      parameters['formName'] = formName;
      parameters['ajaxMode'] = 1;
      return this.request(null, parameters);
    },

    submitWithToken: function(formName, parameters) {
      parameters['formName'] = formName;
      parameters['ajaxMode'] = 1;
      return this.requestWithToken(null, parameters, true);
    },

    _doAJAX: function(classMethod, parameters) {

      var self = this,
          lang = gSettings['idLanguage'],
          httpConfig = {
            'headers': {
              'Content-Type': 'application/x-www-form-urlencoded'
              //'XDEBUG_SESSION': 'b94278ff590611e4bd00525400826491'
            },
            'transformRequest': urlEncode
          },
          postUrl = classMethod ? ajaxURL : formURL,
          deferred = $q.defer();

      //if (!isDeveloper && config['headers']) delete config['headers']['XDEBUG_SESSION'];

      function successFileCallback(result) {
        deferred.resolve(result['data']);
      }

      function successCallBack(result) {
        var error = null,
            data = result['data'];
        if (
            !data || data['data'] === undefined ||
            data['result'] === undefined ||
            data['errorCode'] === undefined ||
            data['errorMessage'] === undefined ||
            data['token'] === undefined) {    //Check packet format
          error = {errorCode: -1, errorMessage: 'Bad server answer: ' + data};
        }
        if (data['token']) {
          tokens.push(data['token']);
        }
        if (!data['result']) {
          error = {errorCode: data['errorCode'] || parseInt(data, 10), errorMessage: data['errorMessage'] || data};
        }
        if (error) {
          if (error.errorCode == 401) $rootScope.$broadcast(Events.NEEDAUTH);
          console.log(error);
          deferred.reject(error);

        } else {
          deferred.resolve(data['data']);
        }
      }

      function errorCallBack(response) {
        var error = {};
        /*if (errorThrown !== undefined) {
          error = {errorCode: -1, errorMessage: 'Ajax error: ' + errorThrown};
        } else {*/
        error = {errorCode: -1, errorMessage: 'Ajax error: ' + response['status'] + ' ' + response['statusText']};
        //}

        //$(document).trigger("gAjaxError.g", error);
        console.log(error);
        deferred.reject(error);
      }

      function finallyCallback() {
        self.startStopSpinner();
      }


      if (classMethod != '_getFile') {
        /*if (lang !== undefined) {
          parameters['idLanguage'] = lang;
        }*/

        if (classMethod) {
          parameters['ajaxCall'] = classMethod;
        }

        $http.post(postUrl, parameters, httpConfig)
          .then(successCallBack, errorCallBack)
          .finally (finallyCallback);
      } else {
        $http({
          'method': 'GET',
          'url': parameters.path,
          'data': {onprogress: parameters.onProgress},
          'responseType': parameters.type,
          'transformResponse': function(result) {
            return result;
          },
          'transformRequest': function(data) {
            return data;
          }
        })
          .then(successFileCallback, errorCallBack)
          .finally (finallyCallback);
      }

      return deferred.promise;

    }

  }).inject('$rootScope', '$http', '$q', 'SpinnerService');

});
