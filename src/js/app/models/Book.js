'use strict';
define('app/models/Book', ['lib/Class'], function(Class) {

  var settings = window['gSettings'],
      imageHost = settings['backendURL'],
      thumbSettings = settings['THUMB'];

  function getThumbPath(thumbId) {
    thumbId = thumbId ? thumbId : 2;
    return thumbId ? imageHost + '/userfiles/img/id' + thumbId + 'w' + thumbSettings['width'] + 'h' + thumbSettings['height'] : null;
  }

  function parseAuthors(authors) {
    var result = [];
    authors.forEach(function(author) { result.push(author['lastName'] + ' ' + author['firstName']) });
    return result.join(', ');
  }

  function parseGenres(genres) {
    var result = [];
    genres.forEach(function(genre) {
      result.push(genre['name']);
    });
    return result;
  }

  return Class.create('Book', {
    'id': '',
    'lang_id': '',
    'thumbBlob': '',
    'thumb': '',
    'epubBlob': '',
    'title': '',
    'author': '',
    'price': '',
    'status': '',
    'stored': '',
    'opened': '',
    'summary': '',
    'bookAuthors': '',
    'bookGenres': '',
    'VAT': '',
    'genres': '',
    'bookmark': '',

    constructor: function(data) {
      if (angular.isObject(data)) {
        this['id'] = data['id'] || data['idBook'];
        this['title'] = data['title'];
        this['bookAuthors'] = data['bookAuthors'];
        this['bookGenres'] = data['bookGenres'];
        this['genres'] = parseGenres(data['bookGenres']);
        this['author'] = data['author'] || parseAuthors(data['bookAuthors']);
        this['price'] = data['price'];
        this['VAT'] = data['VAT'];
        this['thumb'] = data['thumb'] || getThumbPath(data['images'][0]);
        this['summary'] = data['summary'] || data['annotation'];
        this['status'] = data['status'];
        this['stored'] = data['stored'];
        this['lang_id'] = data['lang_id'] || data['idLanguage'];
        this['opened'] = data['opened'];
        this['bookmark'] = data['bookmark'];
      }
    }
  });

});
