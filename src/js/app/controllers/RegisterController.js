'use strict';
define('app/controllers/RegisterController',
    ['app/core/BaseController',
      'app/services/AuthService',
      'app/services/SessionService'
    ],
    function(BaseController, AuthService, SessionService) {
      var $authService,
          $sessionService,
          $location;

      return BaseController.extend('RegisterController', {
        constructor: function(_$scope, _$location, _$authService, _$SessionService) {
          $location = _$location;
          $authService = _$authService;
          $sessionService = _$SessionService;
          this._super([_$scope]);
        },

        defineScope: function(_$scope) {
          _$scope['regInfo'] = {
            'email': '',
            'password': '',
            'password1': '',
            'country': ''
          };
          _$scope['register'] = function(regInfo) {
            var form = _$scope['registerForm'];

            angular.forEach(regInfo, function(value, key) {
              var field = form[key];
              field.$setTouched();
              field.$setDirty();
            });

            if (form.$invalid) {
              alert('Invalid data');
            } else {
              $authService.register(regInfo['email'], regInfo['password'], regInfo['password1'], regInfo['country']).then(function() {
                $sessionService.create({'userID': 1}); //TODO: remove this in production
                $location.path('/nav/shelf');
              });
            }
          };
        }
      }).inject('$location', AuthService.fullName, SessionService.fullName);

    });
