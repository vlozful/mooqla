'use strict';
define('app/controllers/PreviewController', ['app/core/BaseController', 'app/modules/api/ApiService'], function(BaseController, ApiService) {

  var $apiService;

  return BaseController.extend('PreviewController', {

    bookId: '',

    bookData: '',

    loadStatus: '',

    constructor: function(_$scope, _$route, _$apiService) {
      var self = this;
      $apiService = _$apiService;
      self.bookData = {'book': {}};
      self.bookId = _$route.current.params['id'];
      self._super([_$scope]);

      $apiService.getPreviewEpubLink(self.bookId).then(
          function(link) {
            $apiService.getEpub(link, _$scope['ondownload']).then(
                function(epub) {
                  self.bookData['book'] = {'epubBlob': epub};
                },
                function(error) {
                  console.log(error, 'Error retrieving preview');
                }
            );
          },
          function(error) {
            console.log(error, 'Error getting preview link');
          }
      );
    },

    defineScope: function(_$scope) {
      _$scope['bookData'] = this.bookData;
    }
  }).inject('$route', ApiService.fullName);
});
