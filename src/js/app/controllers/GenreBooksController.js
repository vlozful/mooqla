'use strict';
define('app/controllers/GenreBooksController', ['app/core/ListController'], function(ListController) {
  return ListController.extend('GenreBooksController', {

    fetchAll: false,

    path: '/nav/book',

    constructor: function() {
      this.addToScope = {
        'title' : arguments[arguments.length - 1]
      };
      this._super(arguments);
      this.api = this.apiService.getBooksByGenre;
    }
  }).inject('title');
});
