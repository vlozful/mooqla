'use strict';
define('app/controllers/AppController',
    ['app/core/BaseController'],
    function(BaseController) {

      function setMenuState(state) {
        menuState['opened'] = !!state;
      }

      function toggleMenuState() {
        setMenuState(!menuState['opened']);
      }

      function doNavigate(path, params) {
        setMenuState();
        if (params) {
          $location.search(params);
        } else {
          $location['$$search'] = {};
        }
        $location.path(path);
      }

      function downLoadHandler(progressEvent) {
        var progress = Math.floor(progressEvent['loaded'] * 100 / progressEvent['total']);
        this['loading'] = {'progress': String(progress)};
        if (progress == 100) this['loading'] = {progress: ''};
        if (!this['$$phase']) this.$apply();
      }

      var menuState = {'opened': false},
          genreTitle = '',
          navigate = {
            'login': function() {doNavigate('/login')},
            'innerLogin': function() {doNavigate('/nav/innerlogin')},
            'home': function() {doNavigate('/nav/shelf')},
            'genres': function() {doNavigate('/nav/genres')},
            'new': function() {doNavigate('/nav/new')},
            'registration': function() {doNavigate('/nav/registration')},
            'forgot': function() {doNavigate('/nav/forgot')},
            'showGenreBooks': function(genre) {doNavigate('/nav/genres/' + genre['id'])},
            'showBook': function(book) {doNavigate('/nav/book/' + book['id'])},
            'buyBook': function(book, $event) {if ($event) $event.stopPropagation(); console.log('buy book')},
            'readBook': function(book) {doNavigate('/nav/book/' + book['id'] + '/read')},
            'previewBook': function(book) {doNavigate('/nav/book/' + book['id'] + '/preview')},
            'langs': function() {doNavigate('/nav/languages')},
            'showLangBooks': function(lang) {doNavigate('/nav/languages/' + lang['id'])},
            'search': function(value) {var params = value ? {'str': value} : ''; doNavigate('/nav/search', params)},
            'go': function(path) {
              doNavigate(path);
            }
          },
          $location,
          AppController = BaseController.extend('AppController', {
            constructor: function(_$scope, _$location) {
              $location = _$location;
              this._super([_$scope]);
            },

            defineScope: function(_$scope) {
              var scopeExtension = {
                'log': function(arg) {
                  window.console.log(arg);
                },
                'navigate': navigate,
                'menuState': menuState,
                'toggleMenu': toggleMenuState,
                'closeMenu': function() {
                  setMenuState();
                },
                'loading': {'progress': ''},
                'ondownload': downLoadHandler.bind(_$scope)
              };

              angular.extend(_$scope, scopeExtension);
            }
          });

      AppController.inject('$location'/*, SessionService.fullName, HttpService.fullName*/);
      return AppController;
    });
