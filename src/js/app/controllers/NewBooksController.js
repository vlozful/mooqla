'use strict';
define('app/controllers/NewBooksController', ['app/core/ListController'], function(ListController) {

  return ListController.extend('MyBooksController', {

    path: '/nav/book',

    fetchAll: false,

    constructor: function() {
      this._super(arguments);
      this.api = this.apiService.getNewBooks;
    }

  });

});
