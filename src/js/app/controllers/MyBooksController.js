'use strict';
define('app/controllers/MyBooksController', ['app/core/ListController', 'app/factories/BookFactory'], function(ListController, BookFactory) {
  var SHELFCAPACITY = 3,
      $localForage,
      $apiService,
      $bookFactory;

  return ListController.extend('MyBooksController', {

    path: 'book',

    fetchAll: true,

    bookShelf: '',

    bookList: '',

    constructor: function(_$scope, _$route, _$apiService, _$localForage, _$bookFactory) {
      $localForage = _$localForage;
      $apiService = _$apiService;
      $bookFactory = _$bookFactory;
      this.bookShelf = [];
      this.bookList = new Array(SHELFCAPACITY);
      var self = this, i = 0;

      for (; i < SHELFCAPACITY; i++) {
        this.bookShelf.push({});
      }

      $localForage.getItem('bookshelf').then(
          function(data) {
            if (!data) {
              $localForage.setItem('bookshelf', self.bookList);
            } else {
              data.forEach(function(book_id, index) {
                $bookFactory.bookFromStored(book_id).then(
                    function(book) {
                      self.bookShelf[index] = book;
                    }
                );
              });

            }
          },
          function(error) {console.log('error', error)}
      );
      this._super(arguments);
      this.api = $apiService.getUserBooks;
    },

    parseResponse: function(data) {
      data.forEach(function(item, index) {
        data[index] = $bookFactory.bookFromData(item);
      });
      return data;
    },

    defineScope: function(_$scope) {
      this._super([_$scope]);
      _$scope['addToShelf'] = this.addToShelf.bind(this);
      _$scope['bookShelf'] = this.bookShelf;
    },

    defineListeners: function(_$scope) {
      var self = this;
      _$scope.$on('$destroy', function() {
        self.bookShelf.forEach(function(book) {
          $bookFactory.revokeThumb(book);
        });
      });
    },

    addToShelf: function(book) {
      var self = this,
          place = -1,
          min = Date.now(),
          i = 0,
          book_id = book['id'];

      //if we have this book already
      for (i = 0; i < SHELFCAPACITY; i++) {
        if (book['id'] == self.bookList[i]) return;
      }

      //seek free space
      for (i = 0; i < SHELFCAPACITY; i++) {
        if (!self.bookList[i]) {
          place = i;
          break;
        }
      }

      //seek first opened
      if (place < 0) {
        self.bookShelf.forEach(function(book, index) {
          var opened = book['opened'];
          if (angular.isDefined(opened) && opened < min) {
            min = opened;
            place = index;
          }
        });
      }

      $apiService.getFullEpubLink(book_id).then(
          function(link) {
            $apiService.getEpub(link, self.$scope['ondownload']).then(
                function(epubArray) {
                  $bookFactory.storeEpub(book_id, epubArray).then(
                      function() {
                        book['epubBlob'] = epubArray;
                        $bookFactory.storeBook(book).then(
                            function(storedBook) {
                              var oldBook = self.bookShelf[place];
                              if (oldBook && oldBook['stored']) {
                                $bookFactory.removeFromStore(oldBook);
                              }
                              self.bookShelf[place] = storedBook;
                              self.bookList[place] = book_id;
                              $localForage.setItem('bookshelf', self.bookList);
                            },
                            function(error) {
                              console.log('Error storing book', error);
                              $bookFactory.removeFromStore(book);
                            }
                        );
                      })})});
    }

  }).inject('$localForage', BookFactory.fullName);

});
