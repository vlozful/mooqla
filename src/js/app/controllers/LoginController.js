'use strict';
define('app/controllers/LoginController',
    ['app/core/BaseController',
      'app/services/AuthService',
      'app/services/SessionService',
      'app/modules/api/ApiService',
      'lib/md5'
    ],
    function(BaseController, AuthService, SessionService, ApiService, MD5) {

      var $authService,
          $sessionService,
          $apiService;

      return BaseController.extend('LoginController', {

        constructor: function(_$scope, _$authService, _$sessionService, _$apiService) {
          $authService = _$authService;
          $sessionService = _$sessionService;
          $apiService = _$apiService;
          this._super([_$scope]);
        },

        defineScope: function(_$scope) {
          _$scope['credentials'] = {'email': '', 'password': ''};
          _$scope['login'] = function(credentials) {

            var form = _$scope['loginForm'];
            angular.forEach(credentials, function(value, key) {
              var field = form[key];
              field.$setTouched();
              field.$setDirty();
            });
            /*if (form.$invalid) {
              alert('Invalid data');
            } else {*/
            $authService.login(credentials['email'], credentials['password']).then(function() {
              $sessionService.load().then(function() {
                _$scope['navigate']['home']();
              });
            });
          };
          //};
        }
      }).inject(AuthService.fullName, SessionService.fullName, ApiService.fullName);

    });
