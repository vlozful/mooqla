'use strict';
define('app/controllers/GenresController', ['app/core/ListController'], function(ListController) {

  return ListController.extend('GenresController', {
    constructor: function() {
      this.buffer = arguments[arguments.length - 1]; //data
      this._super(arguments);
      this.api = this.apiService.getGenresList;
    }
  }).inject('data');

});
