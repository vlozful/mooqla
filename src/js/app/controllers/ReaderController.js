'use strict';
define('app/controllers/ReaderController',
    ['app/core/BaseController', 'app/factories/BookFactory', 'app/modules/api/ApiService'],
    function(BaseController, BookFactory, ApiService) {

      var $bookFactory,
          $apiService;

      return BaseController.extend('ReaderController', {

        bookId: '',

        bookData: '',

        loadStatus: '',

        constructor: function(_$scope, _$route, _$bookFactory, _$apiService) {
          var self = this;
          $bookFactory = _$bookFactory;
          $apiService = _$apiService;
          self.bookData = {'book': {}};
          self.bookId = _$route.current.params['id'];
          self._super([_$scope]);

          $bookFactory.bookFromStored(_$route.current.params['id']).then(
              function(book) {
                self.bookData['book'] = book;
              },
              function(error) {
                console.log('Error retrieving book');
              }
          );
        },

        defineScope: function(_$scope) {
          _$scope['bookData'] = this.bookData;
        }
      }).inject('$route', BookFactory.fullName, ApiService.fullName);
    });
