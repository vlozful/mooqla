'use strict';
define('app/controllers/LanguagesController', ['app/core/ListController'], function(ListController) {
  return ListController.extend({
    constructor: function() {
      this.buffer = arguments[arguments.length - 1]; //data
      this._super(arguments);
      this.api = this.apiService.getBookLanguages;
    }
  });
});
