'use strict';
define('app/factories/BookFactory', ['app/models/Book', 'app/modules/api/ApiService'], function(BookModel, ApiService) {

  function generateBookKey(book_id) {
    if (angular.isDefined(book_id) && book_id >= 0) return 'book_' + book_id;
  }

  function generateThumbKey(book_id) {
    return generateBookKey(book_id) + '_thumb';
  }

  function generateEpubKey(book_id) {
    return generateBookKey(book_id) + '_epub';
  }

  var urlCreator = window['URL'] || window['webkitURL'],
      $apiService,
      $storageService,
      $q,

      factory = function(_$apiService, _$storageService, _$q) {
        $apiService = _$apiService;
        $storageService = _$storageService;
        $q = _$q;
        return {

          bookFromData: function(data) {
            return new BookModel(data);
          },

          bookFromStored: function(book_id) {
            var deferred = $q.defer(), result = {};
            if (angular.isDefined(book_id) && book_id >= 0) {
              $storageService.getItem(generateBookKey(book_id))
                .then(
                  function(book) {
                    result = new BookModel(book);
                    return book;
                  },
                  function(error) {
                    deferred.reject(error);
                  })
                .then(
                  function() {
                    $storageService.getItem(generateThumbKey(book_id))
                      .then(
                        function(blob) {
                          result['thumbBlob'] = blob;
                          result['thumb'] = urlCreator['createObjectURL'](blob);
                          $storageService.getItem(generateEpubKey(book_id)).then(
                            function(epub) {
                              result['epubBlob'] = epub;
                              deferred.resolve(result);
                            },
                            function(error) {
                              deferred.reject(error);
                              console.log('error: ', error);
                            }
                          );
                        },
                        function(error) {
                          deferred.reject(error);
                        });
                  });
            }
            return deferred.promise;
          },

          storeEpub: function(book_id, blob) {
            var epub_id = generateEpubKey(book_id);
            return $storageService.setItem(epub_id, blob);
          },

          removeEpub: function(book_id) {
            return $storageService.removeItem(generateEpubKey(book_id));
          },

          removeThumb: function(book_id) {
            return $storageService.removeItem(generateThumbKey(book_id));
          },

          storeBook: function(book) {
            var deferred = $q.defer();
            if (book['stored']) return book;
            var storedBook = new BookModel(book);
            storedBook['stored'] = storedBook['opened'] = Date.now();

            $apiService.getFile(book['thumb'])
              .then(
                function(blob) {
                  storedBook['thumbBlob'] = blob;
                  return blob;
                },
                function(httpError) {
                  deferred.reject(httpError);
                })
              .then(function(blob) {
                  if (!blob) return false;
                  var blob_id = generateThumbKey(storedBook['id']);
                  $storageService.setItem(blob_id, blob)
                    .then(
                      function(ok) {
                        $storageService.setItem(generateBookKey(storedBook['id']), storedBook)
                          .then(
                          function(ok) {
                            storedBook['thumb'] = urlCreator['createObjectURL'](blob);
                            deferred.resolve(storedBook);
                          },
                          function(saveBookError) {
                            $storageService.removeItem(blob_id);
                            deferred.reject(saveBookError);
                          }
                          )},
                      function(saveBlobError) {
                        deferred.reject(saveBlobError);
                      });
                });

            return deferred.promise;
          },

          removeFromStore: function(book) {
            var self = this,
                deferred = $q.defer(),
                book_id = book['id'];

            function errorHandler(error) {
              deferred.reject(error);
            }

            if (!book.stored) {
              deferred.resolve();
              return;
            }

            self.removeEpub(book_id).then(function() {
              self.removeThumb(book_id).then(function() {
                $storageService.removeItem(generateBookKey(book_id)).then(function() {
                  self.revokeThumb(book);
                  deferred.resolve();
                }, errorHandler)}, errorHandler)}, errorHandler);

            return deferred.promise;
          },

          revokeThumb: function(book) {
            urlCreator['revokeObjectURL'](book['thumb']);
          }
        }};

  factory.fullName = 'BookFactory';
  factory['$inject'] = [ApiService.fullName, '$localForage', '$q'];
  return factory;
});
