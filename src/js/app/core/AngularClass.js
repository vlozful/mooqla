'use strict';
define('app/core/AngularClass', ['lib/Class'], function(Class) {
  return Class.create({
    inheritedStatics: {
      '$inject': [],
      inject: function() {
        for (var i = 0; i < arguments.length; i++) this['$inject'].push(arguments[i]);
        return this;
      }
    }
  });
});
