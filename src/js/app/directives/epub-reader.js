'use strict';
define('app/directives/epub-reader', ['epub/Book', 'epub/BookEvent'], function(Book, BookEvent) {

  var SOURCEATTRIBUTE = 'epubSource',
      epub;

  function Controller(_$scope, _$q, _$swipe) {
    this.$scope = _$scope;
    this.$q = _$q;
    this.$swipe = _$swipe;
    this.resetSlider();
    return this;
  }

  var proto = {

    blob: '',

    book: '',

    sliderData: {

    },

    createBook: function($el, blob) {
      if (this.book) this.destroyBook();
      this.resetSlider();

      var self = this,
          $scope = self.$scope,
          el = $el[0],
          width = el.offsetWidth,
          height = el.offsetHeight,
          start,
          bookData = $scope['bookData'],
          book = this.book = new Book(
              {
                container: el.querySelector('#ebook'),
                width: width,
                height: height - 75,
                minSpreadWidth: width,
                styles: {'padding': '0px 15px'},
                direction: bookData['book']['rtl'] ? 'rtl' : 'ltr'
              }, blob, this.$q, this.$scope),
          additionalData = {
            'totalPages': '',
            'currentPage': '',
            'ready': false
          };

      if (bookData) {
        angular.extend(bookData['book'], additionalData);
      } else {
        bookData = $scope['bookData'] = {'book': additionalData};
      }

      book.on(BookEvent.PAGECHANGED, function() {
        var percent = book.currentPage / book.totalPages;
        bookData['book']['currentPage'] = book.currentPage;
        //$scope.apply() goes in slider
        $scope['setSliderPosition'](percent);
      });

      book.on(BookEvent.VIEWCREATED, function() {
        bookData['book']['ready'] = true;
        book.view.on(BookEvent.IFRAMELOADED, function() {
          self.$swipe.bind(angular.element(book.view.window), {
            'start': function(coords) {
              start = coords;
            },
            'end': function(coords) {
              var endX = coords['x'],
                  startX = start['x'];
              if (Math.abs(endX - startX) < 10) return;
              if (endX < startX) book.nextPage(); else book.prevPage();
            }
          });
        });
      });

      book.on(BookEvent.PAGELIST, function(event, data) {
        $scope['slider']['width'] = data.value;
      });

      book.generatePages().then(
          function(book) {
            $scope['slider']['pagination'] = false;
            bookData['book']['totalPages'] = book.totalPages;
            book.show();
          }
      );

      $scope.$on('slider: position', function() {
        var pos = self.$scope['slider']['position'];
        //pos = (pos < 0) ? 0 : (pos > 1) ? 1 : pos;
        book.gotoPage(Math.floor(pos * book.totalPages));
      });

      $scope.$on('$destroy', function() {
        self.destroyBook();
      });
    },

    resetSlider: function() {
      this.sliderData = {
        'width' : 0,
        'position': 0,
        'pagination': true,
        'moving': false
      };
      this.$scope['slider'] = this.sliderData;
    },

    destroyBook: function() {
      this.book.destroy();
    }
  };

  angular.extend(Controller.prototype, proto);

  function directive() {
    return {
      templateUrl: 'epub',
      scope: true,
      link: function() {
        var a = arguments,
            $scope = a[0],
            $element = a[1],
            $attrs = a[2],
            controller = a[3];

        $scope.$watch($attrs[SOURCEATTRIBUTE], function(value) {
          if (!value) return;
          controller.createBook($element, value);
        });
      },
      controller: ['$scope', '$q', '$swipe', Controller]
    };
  }
  directive.fullName = 'epubReader';
  return directive;
});
