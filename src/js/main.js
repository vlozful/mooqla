(function(){

/*
  function orientationChange() {

    var style = getComputedStyle(document.body),
        mainContainer = document.querySelector('#main-container'),
        width = parseInt(style.width, 10),
        height = parseInt(style.height, 10),
        half = width/2 + 'px';

    if(width > height) {
      mainContainer.style.width = style.height;
      mainContainer.style.height = style.width;
      mainContainer.style['transform-origin-x'] = mainContainer.style['-webkit-transform-origin-x'] = mainContainer.style['-ms-transform-origin-x'] = half;
    } else {
      mainContainer.setAttribute('style', '');
    }
  }
  var supportsOrientationChange = "onorientationchange" in window,
      orientationEvent = supportsOrientationChange ? "orientationchange" : "resize";

  window.addEventListener(orientationEvent, orientationChange);

  document.addEventListener("DOMContentLoaded", function() {
    if (window.devicePixelRatio && window.devicePixelRatio >= 2) {
      var testElem = document.createElement('div');
      testElem.style.border = '.5px solid transparent';
      document.body.appendChild(testElem);
      console.log('offsetHeight: ', testElem.offsetHeight);
      if (testElem.offsetHeight == 1)
      {
        document.querySelector('body').className +=' hairline';
      }
      document.body.removeChild(testElem);
    }

    var mainmenu = document.querySelector('#app-menu');
    if(mainmenu) {
      document.querySelector('#nav-menu').addEventListener('click', function(e) {
        if(mainmenu.className.indexOf('opened') !== -1) {
          mainmenu.className = '';
        } else {
          mainmenu.className = 'opened';
        }
      });
    }

    orientationChange();

  });
*/
})();



/**
 * loads sub modules and wraps them up into the main module
 * this should be used for top-level module definitions only
 */
'use strict';
define(['app', 'app.templates'], function (app) {
  //app.run();
  angular.bootstrap(document, ['app']);
});
