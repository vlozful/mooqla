define("app.templates", ["app"], function(){angular.module("app").run(["$templateCache", function($templateCache) {  'use strict';

  $templateCache.put('bookinfo',
    "<div id=\"book-details\" class=\"page-container container\" ng-if=\"::book.id\"><section class=\"book-details hairline-border-bottom\"><div class=\"book-field\"><div class=\"book-cover\"><img ng-src=\"{{book.thumb}}\"></div><div class=\"book-info\"><h2>{{::book.title}}</h2><h3>{{::book.author}}</h3><div class=\"button\"><button class=\"btn btn-buy round blue\" translate ng-click=\"navigate.previewBook(book)\">Preview</button> <button class=\"btn btn-buy round green\" translate ng-click=\"navigate.buyBook(book)\">Buy</button></div></div></div><p><strong translate ng-if=\"book.pages\">Number of pages:</strong> {{::book.pages}}</p></section><section class=\"genres hairline-border-bottom\"><h1>Genres</h1><ul class=\"taglist hlist\"><li ng-repeat=\"tag in book.genres\">{{tag}}</li></ul></section><section class=\"handling hairline-border-bottom\" ng-if=\"::book.summary\"><h1 translate>Summary</h1><p ng-if=\"book.shortSummary\">{{book.shortSummary}} <a href=\"#\" class=\"blue no-underline\" ng-click=\"showSummary()\"><strong translate>More</strong></a></p><p ng-if=\"::book.fullSummary\">{{book.fullSummary}}</p></section><section class=\"bookshelf\" ng-if=\"book.similar\"><h1 translate>Similar books</h1><div class=\"book-field hairline-border-bottom\" ng-repeat=\"book in book.similar\" ng-click=\"navigate.showBook(book)\"><div class=\"book-cover\"><img ng-src=\"{{::book.thumb}}\"></div><div class=\"book-info\"><h2>{{::book.title}}</h2><h3>{{::book.author}}</h3><div class=\"button\"><button class=\"btn btn-buy round\" ng-click=\"navigate.buyBook(book, $event)\">{{::book.price}}:-</button></div></div></div></section></div>"
  );


  $templateCache.put('booklist',
    "<section class=\"bookshelf\"><div class=\"book-field hairline-border-bottom\" ui-scroll=\"item in provider\" adapter=\"booksAdapter\" padding=\"0\" ng-click=\"addToShelf ? addToShelf(item) : navigate.showBook(item)\"><div class=\"book-cover\"><img ng-src=\"{{item.thumb}}\"></div><div class=\"book-info\"><h2>{{::item.title}}</h2><h3>{{::item.author}}</h3><div class=\"button\" ng-if=\"::item.price\"><button class=\"btn btn-buy round\" ng-click=\"navigate.buyBook(item, $event)\">{{::item.price}}:-</button></div></div></div></section>"
  );


  $templateCache.put('checkout',
    ""
  );


  $templateCache.put('dynamicbooks',
    "<div id=\"bookshelf\" class=\"page-container container\" ui-scroll-viewport><h1>{{::title}}</h1><ng-include src=\"'booklist'\"></ng-include></div>"
  );


  $templateCache.put('epub',
    "<div id=\"ebook\"><div class=\"info author\"></div><div class=\"info page\" ng-if=\"bookData.book.ready\">{{bookData.book.currentPage}} / {{::bookData.book.totalPages}}</div><div class=\"slider-container\" epub-slider></div><div class=\"slider-info\"><div class=\"chapter\"></div><div class=\"number\"></div></div></div>"
  );


  $templateCache.put('forgot',
    "<div id=\"restore-pwd\" class=\"page-container container\"><section class=\"login-register\"><h1 translate>Reset password</h1><h2 translate>If you have lost your password you can request that a new password be assigned to your account and e-mailed to you by filling out the following form:</h2><form novalidate method=\"post\"><div class=\"form-field\"><input class=\"hairline-border\" type=\"email\" placeholder=\"{{'Enter email'|translate}}\"></div><div class=\"form-field button-field\"><button class=\"btn btn-green w100 round\" type=\"submit\" translate>Continue</button></div></form></section></div>"
  );


  $templateCache.put('genres',
    "<div id=\"genres\" class=\"page-container container\" ui-scroll-viewport><h1 translate>Genres</h1><section class=\"genres-list link hairline-border-bottom\" ui-scroll=\"item in provider\" padding=\"0\" ng-click=\"navigate.showGenreBooks(item)\"><p><strong>{{item.title}}</strong></p><span>{{item.quantity}}</span></section></div>"
  );


  $templateCache.put('innerLogin',
    "<div id=\"some-login\" class=\"page-container container\"><section class=\"login-register\"><h1>Logga in</h1><h2>Du måste logga in för att fortsätta. Har du inte redan ett konto så måste du <a href=\"#\">registrera</a> dig för att komma vidare.</h2><form><div class=\"form-field highlight has-success\"><input type=\"email\" class=\"hairline-border\" placeholder=\"Användarnamn\"></div><div class=\"form-field highlight has-error\"><input type=\"password\" class=\"hairline-border\" placeholder=\"Lösenord\"></div><div class=\"form-field button-field\"><button class=\"btn btn-green w100\" type=\"button\">FORTSÄTT</button></div></form><a class=\"text-centered wb100\" href=\"#\">Har du glömt lösenordet?</a></section></div>"
  );


  $templateCache.put('languages',
    "<div id=\"languages\" class=\"page-container container\" ui-scroll-viewport><h1 translate>Languages</h1><section class=\"genres-list link hairline-border-bottom\" ui-scroll=\"item in provider\" padding=\"0\" ng-click=\"navigate.showLangBooks(item)\"><p><strong>{{item.title}}</strong></p><span>{{item.quantity}}</span></section></div>"
  );


  $templateCache.put('login',
    "<div id=\"pages-container\" class=\"container\"><div id=\"start\" class=\"page-container container\"><div class=\"logo-container\"><div class=\"logo\"></div></div><form name=\"loginForm\" novalidate method=\"post\" ng-submit=\"login(credentials)\"><div class=\"form-field\" ng-class=\"{'highlight': loginForm.email.$dirty && loginForm.email.$touched, 'has-success': loginForm.email.$valid, 'has-error': !loginForm.email.$valid}\"><input name=\"email\" ng-model=\"credentials.email\" class=\"hairline-border\" type=\"email\" placeholder=\"{{'Enter email'|translate}}\" required></div><div class=\"form-field\" ng-class=\"{'highlight': loginForm.password.$dirty && loginForm.password.$touched, 'has-success': loginForm.password.$valid, 'has-error': !loginForm.password.$valid}\"><input name=\"password\" ng-model=\"credentials.password\" class=\"hairline-border\" type=\"password\" placeholder=\"{{'Password'|translate}}\" required></div><div class=\"form-field button-field\"><button class=\"btn btn-green w100\" type=\"submit\" translate>Sign in</button></div></form><br><a class=\"text-centered wb100 white\" ng-click=\"navigate.forgot()\" translate>Forgot your password?</a><br><a class=\"text-centered wb100 white uppercase no-underline\" ng-click=\"navigate.registration()\" translate>Sign up</a></div></div>"
  );


  $templateCache.put('mybooks',
    "<div id=\"main-bookshelf\" class=\"page-container container\" ui-scroll-viewport><section class=\"chosen-books\"><div class=\"book-cover\" ng-class=\"{empty: !book.thumb}\" ng-repeat=\"book in bookShelf\" ng-click=\"navigate.readBook(book)\"><img ng-if=\"book.thumb\" ng-src=\"{{book.thumb}}\"></div></section><h1 translate>Your books</h1><ng-include src=\"'booklist'\"></ng-include></div>"
  );


  $templateCache.put('nav',
    "<div id=\"app-menu\" ng-class=\"{'opened': menuState.opened}\"><div class=\"search\"><input class=\"hairline-border\" type=\"search\" placeholder=\"{{'Search'|translate}}\" ng-blur=\"searchString.length ? navigate.search(searchString) : ''\" ng-model=\"searchString\"></div><ul class=\"vlist\"><li class=\"books hairline-border-bottom\" translate ng-click=\"navigate.home()\">My books</li><li class=\"genres hairline-border-bottom\" translate ng-click=\"navigate.genres()\">Genres</li><li class=\"new-books hairline-border-bottom\" translate ng-click=\"navigate.new()\">New books</li><li class=\"book-language hairline-border-bottom\" translate ng-click=\"navigate.langs()\">Books by language</li></ul></div><div id=\"pages-container\" class=\"container\"><nav id=\"navbar-main\"><ul class=\"hlist\"><li id=\"nav-menu\" ng-click=\"toggleMenu()\"></li><li id=\"nav-reader\"></li><li id=\"nav-settings\" ng-click=\"navigate.innerLogin()\"></li><li id=\"nav-seek\" ng-click=\"navigate.search()\"></li><li id=\"nav-home\" ng-click=\"navigate.home()\"></li></ul></nav><div app-view-segment=\"1\"></div></div>"
  );


  $templateCache.put('newbooks',
    "<div id=\"bookshelf\" class=\"page-container container\" ui-scroll-viewport><h1 translate>New books</h1><ng-include src=\"'booklist'\"></ng-include></div>"
  );


  $templateCache.put('reader',
    "<div id=\"book-reader\" class=\"page-container container\"><div class=\"content-container\" epub-reader epub-source=\"bookData.book.epubBlob\"></div></div>"
  );


  $templateCache.put('registration',
    "<div id=\"register\" class=\"page-container container\"><section class=\"login-register\"><h1 translate>Create an account</h1><h2 translate>Register at Booqlas webapp and take advantage of all the books that are available. Your active books are available free online.</h2><form name=\"registerForm\" novalidate method=\"post\" ng-submit=\"register(regInfo)\"><div class=\"form-field\" ng-class=\"{'highlight': registerForm.email.$dirty && registerForm.email.$touched, 'has-success': registerForm.email.$valid, 'has-error': !registerForm.email.$valid}\"><input name=\"email\" class=\"hairline-border\" type=\"email\" placeholder=\"{{'Enter email'|translate}}\" ng-model=\"regInfo.email\" required></div><div class=\"form-field\" ng-class=\"{'highlight': registerForm.password.$dirty && registerForm.password.$touched, 'has-success': registerForm.password.$valid, 'has-error': !registerForm.password.$valid}\"><input name=\"password\" class=\"hairline-border\" type=\"password\" placeholder=\"{{'Password'|translate}}\" ng-model=\"regInfo.password\" required></div><div class=\"form-field\" ng-class=\"{'highlight': registerForm.password1.$dirty && registerForm.password1.$touched, 'has-success': registerForm.password1.$valid, 'has-error': !registerForm.password1.$valid}\"><input name=\"password1\" class=\"hairline-border\" type=\"password\" placeholder=\"{{'Repeat password'|translate}}\" ng-model=\"regInfo.password1\" required compare-to=\"regInfo.password\"></div><div class=\"form-field\" ng-class=\"{'highlight': registerForm.country.$dirty && registerForm.country.$touched, 'has-success': registerForm.country.$valid, 'has-error': !registerForm.country.$valid}\"><div class=\"select-field\"><div class=\"select-arrows\"></div><select name=\"country\" class=\"hairline-border\" ng-model=\"regInfo.country\" required convert-to-number><option value=\"\" disabled selected translate>Your country</option><option value=\"1\">Something</option><option value=\"2\">Something 2</option></select></div></div><div class=\"form-field button-field\"><button class=\"btn btn-green w100 round\" type=\"submit\" translate>Continue</button></div></form></section></div>"
  );


  $templateCache.put('search',
    "<div id=\"search\" class=\"page-container container\"><section class=\"search\"><h1 translate>Search</h1><h2 translate>Search book titles, author, ISBN number or take you on via some of our most popular searches below.</h2><div class=\"search\"><input class=\"hairline-border\" type=\"search\" placeholder=\"{{'Search'|translate}}\" ng-model=\"searchString\" ng-blur=\"searchString.length ? search(searchString) :''\"> <button class=\"btn\" translate>Search</button></div></section><ng-include src=\"'booklist'\"></ng-include><section class=\"search-popular\" ng-if=\"similar.length\"><h1 translate>Popular searches</h1><div><a ng-click=\"search(request)\" ng-repeat=\"request in similar\">{{::request}}</a></div></section></div>"
  );


  $templateCache.put('slider',
    "<div class=\"fixer\"></div><div class=\"slider\"><div class=\"viewed\"></div><div class=\"handler\"></div></div>"
  );
}]); return true});