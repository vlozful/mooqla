'use strict';
define('app',
    ['app/RouteResolver',
      'app/controllers/AppController',
      'app/controllers/LoginController',
      'app/controllers/RegisterController',
      'app/controllers/MyBooksController',
      'app/controllers/GenresController',
      'app/controllers/GenreBooksController',
      'app/controllers/LanguagesController',
      'app/controllers/LanguageBooksController',
      'app/controllers/BookDetailsController',
      'app/controllers/NewBooksController',
      'app/controllers/SearchController',
      'app/controllers/ReaderController',
      'app/controllers/PreviewController',
      'app/core/ListController',
      'i18n/translations',
      'app/factories/BookFactory',
      'app/services/SessionService',
      'app/services/AuthService',
      'app/directives/compareTo',
      'app/directives/epub-reader',
      'app/directives/slider',
      'app/modules/api',
      'lib/local-forage',
      'lib/ui-scroll',
      'lib/RouteSegment',
      'lib/jqlite-add'
    ],
    function(RouteResolver,
             ApplicationController,
             LoginController,
             RegisterController,
             MyBooksController,
             GenresController,
             GenreBooksController,
             LanguagesController,
             LanguageBooksController,
             BookDetailsController,
             NewBooksController,
             SearchController,
             ReaderController,
             PreviewController,
             ListController,
             i18n,
             BookFactory,
             SessionService,
             AuthService,
             CompareD,
             EpubReaderD,
             SliderD,
             API,
             LOCALFORAGE,
             UISCROLL) {

      function routerConfig($routeSegmentProvider, $routeProvider) {

        $routeSegmentProvider
          .when('/', 'root', {redirectTo: '/nav/shelf'})
          .when('/login', 'login', {public: true})
            .segment('login', {
              templateUrl: 'login',
              controller: LoginController
            })
          .when('/nav/registration', 'nav.registration', {public: true})
          .when('/nav/forgot', 'nav.forgot', {public: true})
          .when('/nav/shelf', 'nav.shelf')
          .when('/nav/new', 'nav.new')
          .when('/nav/genres', 'nav.genres')
          .when('/nav/languages', 'nav.languages')
          .when('/nav/languages/:id', 'nav.languagebooks')
          .when('/nav/genres/:id', 'nav.genrebooks')
          .when('/nav/book/:id', 'nav.bookinfo')
          .when('/nav/book/:id/preview', 'nav.bookpreview')
          .when('/nav/book/:id/buy', 'nav.bookbuy')
          .when('/nav/book/:id/read', 'nav.bookread')
          .when('/nav/book/:id/preview', 'nav.bookpreview')
          .when('/nav/search', 'nav.search')
          .when('/nav/innerlogin', 'nav.innerlogin')
          .segment('nav', {
              templateUrl: 'nav'
            })
            .within()
              .segment('registration', {
                  templateUrl: 'registration',
                  controller: RegisterController
                })
              .segment('forgot', {
                  templateUrl: 'forgot'
                })
              .segment('shelf', {
                  templateUrl: 'mybooks',
                  controller: MyBooksController
                })
              .segment('genres', {
                  templateUrl: 'genres',
                  controller: GenresController,
                  resolve: RouteResolver.GENRES,
                  untilResolved: { template: '' }
                })
              .segment('new', {
                  templateUrl: 'newbooks',
                  controller: NewBooksController
                })
              .segment('genrebooks', {
                  templateUrl: 'dynamicbooks',
                  controller: GenreBooksController,
                  resolve: RouteResolver.GENREBOOKS,
                  untilResolved: {
                    template: ''
                  },
                  dependencies: ['id']
                })
                .segment('languages', {
                  templateUrl: 'languages',
                  controller: LanguagesController,
                  resolve: RouteResolver.LANGUAGES,
                  untilResolved: {
                    template: ''
                  }
                })
                .segment('languagebooks', {
                  templateUrl: 'dynamicbooks',
                  controller: LanguageBooksController,
                  resolve: RouteResolver.LANGUAGEBOOKS,
                  untilResolved: {
                    template: ''
                  },
                  dependencies: ['id']
                })
                .segment('bookinfo', {
                  templateUrl: 'bookinfo',
                  controller: BookDetailsController,
                  dependencies: ['id']
                })
                .segment('search', {
                  controller: SearchController,
                  templateUrl: 'search'
                })
                .segment('bookread', {
                  templateUrl: 'reader',
                  controller: ReaderController,
                  dependencies: ['id']
                })
                .segment('bookpreview', {
                  templateUrl: 'reader',
                  controller: PreviewController,
                  dependencies: ['id']
                })
                .segment('innerlogin', {
                  templateUrl: 'innerLogin'
                });

        $routeProvider.otherwise({ redirectTo: '/nav/shelf' });
      }
      routerConfig['$inject'] = ['$routeSegmentProvider', '$routeProvider'];

      function runTextCatalogBlock(getTextCatalog) {
        angular.forEach(i18n, function(value, key) {
          getTextCatalog['currentLanguage'] = 'sw'; //TODO: get from somewhere
          getTextCatalog['debug'] = false;
          getTextCatalog['setStrings'](key, value);
        });
      }

      function preventRouting($rootScope, $location, $route, $authService) {
        $rootScope.$on('$routeChangeStart', function(event, next, current) {
          if (!next) return;
          var nextSegment = next.segment,
              currentSegment = current ? current.segment : '',
              isPublic = next['$$route'].public || false;

          if (currentSegment && nextSegment === 'root' && currentSegment !== nextSegment) {
            event.preventDefault();
            return;
          }

          if (!isPublic) {
            var isLoggedIn = $authService.isLoggedIn();
            if (isLoggedIn !== true) {
              event.preventDefault();
              isLoggedIn.then(
                  function(success) {
                    $route.reload();
                  },
                  function(error) {
                    $location.path('/login');
                  });
            }
          }
        });
      }

      preventRouting['$inject'] = ['$rootScope', '$location', '$route', AuthService.fullName];

      function httpConfig($httpProvider) {
        var JSON_START = /^\[|^\{(?!\{)/;
        var JSON_ENDS = {
          '[': /]$/,
          '{': /}$/
        };

        function isJsonLike(str) {
          var jsonStart = str.match(JSON_START);
          return jsonStart && JSON_ENDS[jsonStart[0]].test(str);
        }

        function responseTransform(data, headers) {
          if (angular.isString(data)) {
            // Strip json vulnerability protection prefix and trim whitespace
            var tempData = data.replace(/^\)\]\}',?\n/, '').trim();

            if (tempData) {
              var contentType = headers('Content-Type');
              if ((contentType && (contentType.indexOf('application/json') === 0)) && isJsonLike(tempData)) {
                try {
                  data = JSON.parse(tempData);
                } catch (e) {
                  console.info(e);
                }
              }

            }
          }
          return data;
        }

        $httpProvider.defaults.withCredentials = true;
        $httpProvider.defaults.transformResponse = responseTransform;
      }

      httpConfig['$inject'] = ['$httpProvider'];

      var app = angular.module('app', ['ng',
                                       'ngRoute',
                                       'ngTouch',
                                       LOCALFORAGE.name,
                                       API.name,
                                       UISCROLL.name,
                                       'route-segment',
                                       'view-segment',
                                       'gettext',
                                       'ui.scroll.jqlite']);

      app
        .factory(BookFactory.fullName, BookFactory)
        .service(AuthService.fullName, AuthService)
        .service(SessionService.fullName, SessionService)
        .directive(CompareD.fullName, CompareD)
        .directive(EpubReaderD.fullName, EpubReaderD)
        .directive(SliderD.fullName, SliderD)
        .run(preventRouting)
        .run(['gettextCatalog', runTextCatalogBlock])
        .config(routerConfig)
        .config(httpConfig)
        .config(['$localForageProvider', function($localForageProvider) {
            $localForageProvider.config({
              //driver: 'localStorageWrapper', // if you want to force a driver
              'name': 'mooqla', // name of the database and prefix for your data, it is "lf" by default
              //version: 1.0, // version of the database, you shouldn't have to use this
              'storeName': 'mstore' // name of the table
              //description: 'some description'
            });
          }])
        .controller(ApplicationController.fullName, ApplicationController);

      return app;

    }
);
