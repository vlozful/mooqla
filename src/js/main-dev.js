'use strict';
define('main-dev', ['app', 'app.templates'], function(app) {

  var genresData = [{'id': 0, 'title': 'Action', 'quantity': 17},
        {'id': 1, 'title': 'Classic', 'quantity': 10},
        {'id': 2, 'title': 'Comic/Graphic Novel', 'quantity': 4},
        {'id': 3, 'title': 'Crime/Detective', 'quantity': 25},
        {'id': 4, 'title': 'Fable', 'quantity': 3},
        {'id': 5, 'title': 'Fairy tale', 'quantity': 44},
        {'id': 6, 'title': 'Fanfiction', 'quantity': 28},
        {'id': 7, 'title': 'Fantasy', 'quantity': 12},
        {'id': 8, 'title': 'Folklore', 'quantity': 34},
        {'id': 9, 'title': 'Historical fiction', 'quantity': 14},
        {'id': 10, 'title': 'Horror', 'quantity': 22},
        {'id': 11, 'title': 'Humor', 'quantity': 1},
        {'id': 12, 'title': 'Legend', 'quantity': 34},
        {'id': 13, 'title': 'Crime/Detective', 'quantity': 33},
        {'id': 14, 'title': 'Magical Realism', 'quantity': 67},
        {'id': 15, 'title': 'Mystery', 'quantity': 11},
        {'id': 16, 'title': 'Mythology', 'quantity': 5},
        {'id': 17, 'title': 'Science fiction', 'quantity': 32},
        {'id': 18, 'title': 'Short story', 'quantity': 19},
        {'id': 19, 'title': 'Tall tale', 'quantity': 27},
        {'id': 20, 'title': 'Western', 'quantity': 56}
      ],
      langData = [
        {'id': 0, 'title': 'English', 'quantity': 100},
        {'id': 1, 'title': 'Russian', 'quantity': 5},
        {'id': 2, 'title': 'Swedish', 'quantity': 5},
        {'id': 3, 'title': 'Some other', 'quantity': 24}
      ],
      myBooksData = [
        {'id': 14, 'title': 'MyBook 1', 'author': 'Some author 1', 'thumb': 'i/book-cover-1.jpg', 'status': ''},
        {'id': 15, 'title': 'MyBook 2', 'author': 'Some author 1', 'thumb': 'i/book-cover-2.jpg', 'status': ''},
        {'id': 16, 'title': 'MyBook 3', 'author': 'Some author 5', 'thumb': 'i/bc3.jpg', 'status': ''},
        {'id': 17, 'title': 'MyBook 4', 'author': 'Some author 1', 'thumb': 'i/book-cover-1.jpg', 'status': ''},
        {'id': 18, 'title': 'MyBook 5', 'author': 'Some author 1', 'thumb': 'i/bc5.jpg', 'status': ''},
        {'id': 19, 'title': 'MyBook 6', 'author': 'Some author 2', 'thumb': 'i/book-cover-2.jpg', 'status': ''},
        {'id': 20, 'title': 'MyBook 7', 'author': 'Some author 1', 'thumb': 'i/book-cover-1.jpg', 'status': ''},
        {'id': 21, 'title': 'MyBook 8', 'author': 'Some author 1', 'thumb': 'i/bc1.jpg', 'status': ''},
        {'id': 22, 'title': 'MyBook 9', 'author': 'Some author 5', 'thumb': 'i/bc2.jpg', 'status': ''},
        {'id': 23, 'title': 'MyBook 10', 'author': 'Some author 6', 'thumb': 'i/book-cover-1.jpg', 'status': ''},
        {'id': 24, 'title': 'MyBook 11', 'author': 'Some author 1', 'thumb': 'i/bc4.jpg', 'status': ''},
        {'id': 25, 'title': 'MyBook 12', 'author': 'Some author 8', 'thumb': 'i/book-cover-1.jpg', 'status': ''}
      ],
      bookListData = [
        {'id': 0, 'title': 'Cognitive Cooking with chef Watson', 'author': 'Gunn, Deana, Miniati, Wona', 'thumb': 'i/bc1.jpg', 'status': '', 'price': 123},
        {'id': 1, 'title': 'The Kindergarten Wars', 'author': 'Eisenstock, Alan', 'thumb': 'i/bc2.jpg', 'status': '', 'price': 120},
        {'id': 2, 'title': 'Go Set a Watchman', 'author': 'Lee, Harper', 'thumb': 'i/bc3.jpg', 'status': '', 'price': 100},
        {'id': 3, 'title': 'The Girl on the Train', 'author': 'Hawkins, Paula', 'thumb': 'i/bc4.jpg', 'status': '', 'price': 200},
        {'id': 4, 'title': 'Luckiest Girl Alive', 'author': 'Knoll, Jessica', 'thumb': 'i/bc5.jpg', 'status': '', 'price': 200},
        {'id': 5, 'title': 'Everything I Never Told You', 'author': 'Ng, Celeste', 'thumb': 'i/bc6.jpg', 'status': '', 'price': 123},
        {'id': 6, 'title': 'Dummy title-6', 'author': 'Javascript', 'thumb': 'i/book-cover-1.jpg', 'status': '', 'price': 120},
        {'id': 7, 'title': 'Dummy title-7', 'author': 'Lee, Harper', 'thumb': 'i/book-cover-2.jpg', 'status': '', 'price': 100},
        {'id': 8, 'title': 'Dummy title-8', 'author': 'Hawkins, Paula', 'thumb': 'i/book-cover-1.jpg', 'status': '', 'price': 200},
        {'id': 9, 'title': 'Dummy title-9', 'author': 'Knoll, Jessica', 'thumb': 'i/book-cover-2.jpg', 'status': '', 'price': 200},
        {'id': 10, 'title': 'Dummy title-10', 'author': 'Eisenstock, Alan', 'thumb': 'i/book-cover-1.jpg', 'status': '', 'price': 120},
        {'id': 11, 'title': 'Dummy title-11', 'author': 'Lee, Harper', 'thumb': 'i/book-cover-2.jpg', 'status': '', 'price': 100},
        {'id': 12, 'title': 'Dummy title-12', 'author': 'Hawkins, Paula', 'thumb': 'i/book-cover-1.jpg', 'status': '', 'price': 200},
        {'id': 13, 'title': 'Dummy title-13', 'author': 'Knoll, Jessica', 'thumb': 'i/book-cover-2.jpg', 'status': '', 'price': 200},
        {'id': 14, 'title': 'Dummy title-14', 'author': 'Gunn, Deana, Miniati, Wona', 'thumb': 'i/book-cover-1.jpg', 'status': '', 'price': 123},
        {'id': 15, 'title': 'Dummy title-15', 'author': 'Eisenstock, Alan', 'thumb': 'i/book-cover-2.jpg', 'status': '', 'price': 120},
        {'id': 16, 'title': 'Dummy title-16', 'author': 'Lee, Harper', 'thumb': 'i/bc3.jpg', 'status': '', 'price': 100},
        {'id': 17, 'title': 'Dummy title-17', 'author': 'Hawkins, Paula', 'thumb': 'i/bc4.jpg', 'status': '', 'price': 200},
        {'id': 18, 'title': 'Dummy title-18', 'author': 'Knoll, Jessica', 'thumb': 'i/bc5.jpg', 'status': '', 'price': 200},
        {'id': 19, 'title': 'Dummy title-19', 'author': 'Hawkins, Paula', 'thumb': 'i/bc4.jpg', 'status': '', 'price': 200},
        {'id': 20, 'title': 'Dummy title-20', 'author': 'Knoll, Jessica', 'thumb': 'i/bc5.jpg', 'status': '', 'price': 200},
        {'id': 21, 'title': 'Dummy title-21', 'author': 'Gunn, Deana, Miniati, Wona', 'thumb': 'i/bc1.jpg', 'status': '', 'price': 123},
        {'id': 22, 'title': 'Dummy title-22', 'author': 'Eisenstock, Alan', 'thumb': 'i/bc2.jpg', 'status': '', 'price': 120},
        {'id': 23, 'title': 'Dummy title-23', 'author': 'Lee, Harper', 'thumb': 'i/bc3.jpg', 'status': '', 'price': 100},
        {'id': 24, 'title': 'Dummy title-24', 'author': 'Hawkins, Paula', 'thumb': 'i/bc4.jpg', 'status': '', 'price': 200},
        {'id': 25, 'title': 'Dummy title-25', 'author': 'Knoll, Jessica', 'thumb': 'i/bc5.jpg', 'status': '', 'price': 200}
      ],
      bookInfoData = {
        'id': '0',
        'title': 'Cognitive Cooking with chef Watson',
        'author': 'Gunn, Deana, Miniati, Wona',
        'thumb': 'i/bc1.jpg',
        'status': '',
        'price': 123,
        'pages': 256,
        'genres': ['Action', 'Detective', 'Fantasy'],
        'summary': 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?',
        'similar': [
          {'id': 2, 'title': 'Go Set a Watchman', 'author': 'Lee, Harper', 'thumb': 'i/bc3.jpg', 'status': '', 'price': 100},
          {'id': 3, 'title': 'The Girl on the Train', 'author': 'Hawkins, Paula', 'thumb': 'i/bc4.jpg', 'status': '', 'price': 200},
          {'id': 4, 'title': 'Luckiest Girl Alive', 'author': 'Knoll, Jessica', 'thumb': 'i/bc5.jpg', 'status': '', 'price': 200}
        ]
      },
      searchData = {
        'result': bookListData,
        'similar': ['Something', 'How can I delete account?']
      };

  app.requires.push('ngMockE2E');

  app.config(['$provide', function($provide) {
    $provide.decorator('$httpBackend', ['$delegate', function($delegate) {
      var proxy = function(method, url, post, callback, headers, timeout, withCredentials, responseType) {
        var interceptor = function() {
          var _this = this,
              _arguments = arguments;
          setTimeout(function() {
            callback.apply(_this, _arguments);
          }, 700);
        };
        return $delegate.call(this, method, url, post, interceptor, headers, timeout, withCredentials, responseType);
      };
      for (var key in $delegate) {
        proxy[key] = $delegate[key];
      }
      return proxy;
    }]);
  }]);

  app.run(['$httpBackend', function($httpBackend) {
    //$httpBackend.whenGET(/tmpl/)['passThrough']();
    $httpBackend.whenGET(/.jpg/)['passThrough']();
    $httpBackend.whenGET(/.epub/)['passThrough']();
    $httpBackend.whenPOST(/ajax.php/).respond(function(method, url, data) {
      var response = {'result': true, 'data': '', 'token': null, 'errorCode': null, 'errorMessage': null},
          code = 200;
      data = JSON.parse(data);
      switch (data['ajaxCall']) {
        case 'authGetToken': response['data'] = 'f2ba10a9cfee436e85e119fc8bf25540'; break;
        case 'getSession': response['data'] = {'userID': '123', 'email': 'e@gmail.com'}; break;
        //case 'getSession': response = {'result': false, 'data': '', 'token': null, 'errorCode': 401, 'errorMessage': 'Not logged in'}; break;
        case 'getGenresList':
          var result;
          if (data.start >= genresData.length) result = [];
          else result = genresData.slice(data.start, data.end);
          response['token'] = 'f2ba10a9cfee436e85e119fc8bf25540';
          response['data'] = genresData;
          break;
        case 'getBooksByGenre':
        case 'getNewBooks':
        case 'getBooks':
          var result;
          if (data.start && data.start >= bookListData.length) result = [];
          else result = bookListData.slice(data.start, data.end);
          response['token'] = 'f2ba10a9cfee436e85e119fc8bf25540';
          response['data'] = result;
          break;
        case 'getUserBooks':
          var result;
          if (data.start && data.start >= myBooksData.length) result = [];
          else result = myBooksData.slice(data.start, data.end);
          response['token'] = 'f2ba10a9cfee436e85e119fc8bf25540';
          response['data'] = result;
          break;
        case 'getAvailableLanguagesForBooks':
          response['token'] = 'f2ba10a9cfee436e85e119fc8bf25540';
          response['data'] = langData;
          break;
        case 'getBookDetails':
          response['token'] = 'f2ba10a9cfee436e85e119fc8bf25540';
          response['data'] = bookInfoData;
          break;
        case 'getSearchResults':
          response['token'] = 'f2ba10a9cfee436e85e119fc8bf25540';
          response['data'] = searchData;
          break;
        default: response = {}; code = 404;
      }

      return [code, response];
    });

    $httpBackend.whenPOST(/forms.php/).respond(function(method, url, data) {
      var response = {'result': true, 'data': '', 'token': null, 'errorCode': null, 'errorMessage': null},
          code = 200;
      data = JSON.parse(data);
      switch (data['formName']) {
        case 'g-themes-webapp-login': response['token'] = 'f2ba10a9cfee436e85e119fc8bf25541'; break;
        case 'g-themes-webapp-register': response['token'] = 'f2ba10a9cfee436e85e119fc8bf25542'; break;
        default: code = 404; response = {};
      }

      return [code, response];
    });

  }]);

  angular.bootstrap(window.document, ['app'], {strictDi: true, debugInfoEnabled: true});
});
