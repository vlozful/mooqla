'use strict';
define('i18n/translations', function() {
  return {
    'sw': {
      'Enter email' : 'Ange email',
      'Password': 'Lösenord',
      'Forgot your password?': 'Har du glömt lösenordet?',
      'Sign in': 'Logga in',
      'Sign up': 'Registrera',
      'Create an account': 'Skapa ett konto',
      'Continue': 'Fortsätt',
      'Register at Booqlas webapp and take advantage of all the books that are available. Your active books are available free online.':
          'Registrera dig på Booqlas webapp och ta del av alla de böcker som finns tillgängliga. Dina aktiva böcker finns tillgängliga utan internet.',
      'Repeat password': 'Uprepa lösenord',
      'Your country': 'Ditt land',
      'Reset password': 'Återställ lösenord',
      'Your books': 'Dina böcker'
    }
  };
});
