'use strict';
define('epub/BookEvent', function() {
  return {
    IFRAMELOADED: 'iframe:loaded',
    VISIBLERANGECHANGED: 'view:visibleRangeChanged',
    PAGECHANGED: 'book:pageChanged',
    VIEWCREATED: 'book:viewcreated',
    CHAPTERUNLOADED: 'view:chapterunloaded',
    PAGELIST: 'book:pagelist',
    LAYOUT: 'view:layout'
  };
});
