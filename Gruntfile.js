module.exports = function(grunt) {
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-angular-templates');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.task.loadTasks('./tasks');

  var modules = {}, symbols = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
      symLength = symbols.length, sIndex = 0, nIndex = 0;

  grunt.initConfig({
    concat: {
      options: {
        separator: ';'
      },
      dev: {
        src: ['./bower_libs/angular/angular.min.js',
              './bower_libs/angular-route/angular-route.min.js',
              './src/js/angular-mocks.js',
              './bower_libs/angular-gettext/dist/angular-gettext.min.js',
              './bower_libs/angular-touch/angular-touch.min.js',
              './bower_libs/localforage/dist/localforage.nopromises.min.js',
              './bower_libs/jszip/dist/jszip.min.js'
        ],
        dest: 'build/js/lib.js'
      },
      dist: {
        src: ['./bower_libs/angular/angular.min.js',
          './bower_libs/angular-route/angular-route.min.js',
          './bower_libs/angular-gettext/dist/angular-gettext.min.js',
          './bower_libs/localforage/dist/localforage.nopromises.min.js',
          './bower_libs/jszip/dist/jszip.min.js'
        ],
        dest: 'build/js/lib.js'
      }
    },
    ngtemplates: {
      app: {
        src: 'src/templates/**.html',
        dest: 'src/js/app.templates.js',
        options: {
          htmlmin: {
            collapseBooleanAttributes: true,
            collapseWhitespace: true,
            removeAttributeQuotes: false,
            removeComments: true,// Only if you don't use comment directives!
            removeEmptyAttributes: true,
            removeRedundantAttributes: true,
            removeScriptTypeAttributes: true,
            removeStyleLinkTypeAttributes: true
          },
          url: function(url) {
            return url.replace('.tmpl.html', '').replace('src/templates/', '');
          },
          bootstrap: function(module, script) {
            return 'define("app.templates", ["app"], function(){angular.module("app").run(["$templateCache", function($templateCache) {' +
                script + '}]); return true});';
          }
        }
      }
    },
    compass: {
      dist: {
        options: {
          sassDir: './src/sass',
          cssDir: './src/css',
          imagesDir: './src/i',
          fontsDir: './src/fonts',
          outputStyle: 'compressed',
          //outputStyle: 'compact',
          relativeAssets: true,
          noLineComments: true,
          environment: 'production',
          sourcemap: true
        }
      },
      dev: {
        options: {
          sassDir: './src/sass',
          cssDir: './src/css',
          imagesDir: './src/i',
          fontsDir: './src/fonts',
          outputStyle: 'compact',
          relativeAssets: true,
          environment: 'development',
          debugInfo: false,
          sourcemap: false
        }
      }
    },
    'closure-compiler': {
      frontend: {
        closurePath: './bower_libs/closure-compiler',
        javaPath: '/Library/"Internet Plug-Ins"/JavaAppletPlugin.plugin/Contents/Home/bin/java',
        js: './build/js/main-optimized.js',
        jsOutputFile: './build/js/main.js',
        maxBuffer: 500,
        options: {
          compilation_level: 'ADVANCED_OPTIMIZATIONS',
          language_in: 'ECMASCRIPT5_STRICT',
          //formatting: 'pretty_print',
          externs: ['./tools/closure-compiler/externs-angular.js',
                    './tools/closure-compiler/externs-angular-mocks.js',
                    './tools/closure-compiler/externs-angular-q.js'
          ],
          warning_level: 'DEFAULT',
          create_source_map: './build/js/main.js.map',
          source_map_format: 'V3'
        }
      }
    },
    'requirejs': {
      compile: {
        options: {
          wrap: {
            startFile: ['./tools/rjs/wrapstart.txt', './tools/rjs/requirestub.js'],
            endFile: ['./tools/rjs/wrapend.txt']
          },
          baseUrl: './src/js',
          paths: {
          },
          name: 'main-dev',
          out: './build/js/main-optimized.js',
          optimize: 'none'/*,
          onBuildRead: function(moduleName, path, contents) {
            var newName;
            if (!modules[moduleName]) {
              if (nIndex == 0) {
                newName = symbols[sIndex];
                sIndex += 1;
              } else {
                newName = symbols[sIndex] + nIndex;
              }
              modules[moduleName] = newName;
              if (sIndex >= symLength) {
                sIndex = 0;
                nIndex += 1;
              }
            }
            return contents;
          },
          onBuildWrite: function(moduleName, path, contents) {
            var result = contents, regexp, newName, searchString;
            for (var p in modules) {
              searchString = "'" + p + "'|" + '"' + p + '"';
              regexp = new RegExp(searchString);
              newName = "'" + modules[p] + "'";
              result = result.replace(regexp, newName);
            }
            return result;
          }*/
        }
      }
    }
  });

  grunt.registerTask('default', ['cssdist', 'ngtemplates', 'requirejs', 'closure-compiler', 'concat:dev']);
  grunt.registerTask('compile', ['ngtemplates', 'requirejs', 'closure-compiler']);
  grunt.registerTask('cssdev', ['compass:dev']);
  grunt.registerTask('cssdist', ['compass:dist']);
};
