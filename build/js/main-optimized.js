(function() {

var modules = {};

function define(id, dependencies, definition) {
  if (typeof id !== 'string') {
    throw 'invalid module definition, module id must be defined and be a string';
  }

  if(typeof dependencies === 'function') {
    definition = dependencies;
    dependencies = [];
  }

  if (dependencies === undefined) {
    throw 'invalid module definition, dependencies must be specified';
  }

  if (definition === undefined) {
    throw 'invalid module definition, definition function must be specified';
  }

  require(dependencies, function() {
    modules[id] = definition.apply(null, arguments);
  });
}

function require(ids, callback) {
  var module, defs = [];

  for (var i = 0; i < ids.length; ++i) {
    module = modules[ids[i]];
    if (!modules.hasOwnProperty(ids[i])) {
      throw 'module definition dependency not found: ' + ids[i];
    }

    defs.push(module);
  }

  callback.apply(null, defs);
}

define('lib/Class', [],function() {
	/*!
 * Class.js
 * Version 0.6.0
 *
 * Copyright(c) 2014 Gregory Jacobs <greg@greg-jacobs.com>
 * MIT Licensed. http://www.opensource.org/licenses/mit-license.php
 *
 * http://www.class-js.com
 */

	/*global window, ClassBuilder, Util */
	
	// Utility functions / variables. 
	
	// NOTE: This entire source file is wrapped in a UMD block / factory function when built, 
	//       so these are not global.
	
	
	// A variable, which is incremented, for assigning unique ID's to classes (constructor functions), allowing 
	// for caching through lookups on hash maps
	var classIdCounter = 0;
	
	
	/**
	 * @class Class
	 * @singleton
	 * 
	 * Simple utility for powerful JavaScript class creation. This provides a number of features for OOP in JavaScript, including:
	 * 
	 * - Single inheritance with subclasses (like Java, C#, etc.)
	 * - Mixin classes
	 * - Static methods, which can optionally be automatically inherited by subclasses
	 * - A static method which is placed on classes that are created, which can be used to determine if the *class* is a subclass of 
	 *   another (unlike the `instanceof` operator, which checks if an *instance* is a subclass of a given class).
	 * - An `instanceOf()` method, which should be used instead of the JavaScript `instanceof` operator, to determine if the instance 
	 *   is an instance of a provided class, superclass, or mixin (the JavaScript `instanceof` operator only covers the first two).
	 * - The ability to add static methods while creating/extending a class, right inside the definition using special properties `statics`
	 *   and `inheritedStatics`. The former only applies properties to the class being created, while the latter applies properties to the
	 *   class being created, and all subclasses which extend it. (Note that the keyword for this had to be `statics`, and not `static`, as 
	 *   `static` is a reserved word in Javascript). 
	 * - A special static method, onClassCreated(), which can be placed in either the `statics` or `inheritedStatics` section, that is
	 *   executed after the class has been extended.
	 * 
	 * Note that this is not the base class of all `Class` classes. It is a utility to create classes, and extend other classes. The
	 * fact that it is not required to be at the top of any inheritance hierarchy means that you may use it to extend classes from
	 * other frameworks and libraries, with all of the features that this implementation provides. 
	 *  
	 * Simple example of creating classes:
	 *     
	 *     var Animal = Class.create( {
	 *         constructor : function( name ) {
	 *             this.name = name;
	 *         },
	 *         
	 *         sayHi : function() {
	 *             alert( "Hi, my name is: " + this.name );
	 *         },
	 *         
	 *         eat : function() {
	 *             alert( this.name + " is eating" );
	 *         }
	 *     } );
	 *     
	 *     
	 *     var Dog = Animal.extend( {
	 *         // Override sayHi method from superclass
	 *         sayHi : function() {
	 *             alert( "Woof! My name is: " + this.name );
	 *         }
	 *     } );
	 *     
	 *     var Cat = Animal.extend( {
	 *         // Override sayHi method from superclass
	 *         sayHi : function() {
	 *             alert( "Meow! My name is: " + this.name );
	 *         }
	 *     } );
	 *     
	 *     
	 *     var dog1 = new Dog( "Lassie" );
	 *     var dog2 = new Dog( "Bolt" );
	 *     var cat = new Cat( "Leonardo Di Fishy" );
	 *     
	 *     dog1.sayHi();  // "Woof! My name is: Lassie"
	 *     dog2.sayHi();  // "Woof! My name is: Bolt"
	 *     cat.sayHi();   // "Meow! My name is: Leonardo Di Fishy"
	 *     
	 *     dog1.eat();  // "Lassie is eating"
	 *     dog2.eat();  // "Bolt is eating"
	 *     cat.eat();   // "Leonardo Di Fishy is eating"
	 */
  var Class = {

    /**
     * A function which can be referenced from class definition code to specify an abstract method.
     * This property (a function) simply throws an error if called, meaning that the method must be overridden
     * in a subclass. This allows the function to be used to create interfaces.
     *
     * This is also a special reference where if an abstract class references this property, a concrete subclass
     * must implement the method.
     *
     * Ex:
     *
     *     var AbstractClass = Class.create( {
     *         abstractClass: true,
     *
     *         myMethod : Class.abstractMethod
     *     } );
     *
     *     var SubClass = AbstractClass.extend( {
     *         myMethod : function() {   // if this implementation method was missing, code would throw an error
     *             ...
     *         }
     *     } );
     *
     */
    abstractMethod: function() {
      throw new Error('method must be implemented in subclass');
    },

    /**
     * Creates a new class. Equivalent to calling `Class.extend( Object, { ... } )`.
     * See {@link #extend} for more details.
     *
     * Example:
     *
     *     var Animal = Class.create( {
     *         // class definition here
     *     } );
     *
     * Example with `name` argument (see below):
     *
     *     var Animal = Class.create( 'Animal', {
     *         // class definition here
     *     } );
     *
     * @param {String} [name] A name for the class. It is recommended that you provide this, as this is set to the constructor's
     *   `displayName` property to assist in debugging (esp. on Firefox), and is also used in error messages for subclasses
     *   not implementing abstract methods, orphaned "override" methods, etc.
     * @param {Object} classDefinition The class definition. See the `overrides` parameter of {@link #extend}.
     * @return {Function} The generated class (constructor function).
     */
    create: function(name, classDefinition) {
      if (typeof name !== 'string') {
        classDefinition = name;
        name = '';
      }

      return Class.extend(name, Object, classDefinition);
    },

    /**
     * Extends one class to create a subclass of it based on a passed object literal (`overrides`), and optionally any mixin
     * classes that are desired.
     *
     * This method adds a few methods to the class that it creates:
     *
     * - override : Method that can be used to override members of the class with a passed object literal.
     *   Same as {@link #override}, without the first argument.
     * - extend : Method that can be used to directly extend the class. Same as this method, except without
     *   the first argument.
     * - hasMixin : Method that can be used to find out if the class (or any of its superclasses) implement a given mixin.
     *   Accepts one argument: the class (constructor function) of the mixin. Note that it is preferable to check if a given
     *   object is an instance of another class or has a mixin by using the {@link #isInstanceOf} method. This hasMixin()
     *   method will just determine if the class has a given mixin, and not if it is an instance of a superclass, or even an
     *   instance of itself.
     *
     *
     * For example, to create a subclass of MySuperclass:
     *
     *     MyComponent = Class.extend( MySuperclass, {
     *
     *         constructor : function( config ) {
     *             // Call superclass constructor
     *             MyComponent.superclass.constructor.call( this, config );
     *
     *             // Your postprocessing here
     *         },
     *
     *         // extension of another method (assuming MySuperclass had this method)
     *         someMethod : function() {
     *             // some preprocessing, if needed
     *
     *             MyComponent.superclass.someMethod.apply( this, arguments );  // send all arguments to superclass method
     *
     *             // some post processing, if needed
     *         },
     *
     *         // a new method for this subclass (not an extended method)
     *         yourMethod: function() {
     *             // implementation
     *         }
     *     } );
     *
     * This is an example of creating a class with a mixin called MyMixin:
     *
     *     MyComponent = Class.extend( Class.util.Observable, {
     *         mixins : [ MyMixin ],
     *
     *         constructor : function( config ) {
     *             // Call superclass constructor
     *             MyComponent.superclass.constructor.call( this, config );
     *
     *             // Call the mixin's constructor
     *             MyMixin.constructor.call( this );
     *
     *             // Your postprocessing here
     *         },
     *
     *
     *         // properties/methods of the mixin will be added automatically, if they don't exist already on the class
     *
     *
     *         // method that overrides or extends a mixin's method
     *         mixinMethod : function() {
     *             // call the mixin's method, if desired
     *             MyMixin.prototype.mixinMethod.call( this );
     *
     *             // post processing
     *         }
     *
     *     } );
     *
     * Note that calling superclass methods can be done with either the [Class].superclass or [Class].__super__ property.
     *
     * @param {String} [name] A name for the new subclass. It is recommended that you provide this, as this is set to the constructor's
     *   `displayName` property to assist in debugging (esp. on Firefox), and is also used in error messages for subclasses
     *   not implementing abstract methods, orphaned "override" methods, etc.
     * @param {Function} superclass The constructor function of the class being extended. If making a brand new class with no superclass, this may
     *   either be omitted, or provided as `Object`.
     * @param {Object} overrides An object literal with members that make up the subclass's properties/method. These are copied into the subclass's
     *   prototype, and are therefore shared between all instances of the new class. This may contain a special member named
     *   `constructor`, which is used to define the constructor function of the new subclass. If this property is *not* specified,
     *   a constructor function is generated and returned which just calls the superclass's constructor, passing on its parameters.
     *   **It is essential that you call the superclass constructor in any provided constructor.** See example code.
     * @return {Function} The subclass constructor from the `overrides` parameter, or a generated one if not provided.
     */
    extend: function(name, superclass, overrides) {
      var args = Array.prototype.slice.call(arguments);

      if (typeof name !== 'string') {
        args.unshift('');  // `name` arg
      }

      // The second argument may be omitted, making Object the superclass
      if (args.length === 2) {
        args.splice(1, 0, Object);
      }

      name = args[0];
      superclass = args[1];
      overrides = args[2];

      return ClassBuilder.build(name, superclass, overrides);
    },


    /**
     * Adds a list of functions to the prototype of an existing class, overwriting any existing methods with the same name.
     * Usage:
     *
     *     Class.override( MyClass, {
     *         newMethod1 : function() {
     *             // etc.
     *         },
     *         newMethod2 : function( foo ) {
     *             // etc.
     *         }
     *     } );
     *
     * @param {Object} origclass The class to override
     * @param {Object} overrides The list of functions to add to origClass.  This should be specified as an object literal
     * containing one or more methods.
     */
    override: function(origclass, overrides) {
      if (overrides) {
        var proto = origclass.prototype;
        Util.assign(proto, overrides);

        if (Util.isIe() && overrides.hasOwnProperty('toString')) {
          proto.toString = overrides.toString;
        }
      }
    },



    /**
     * Determines if a given object (`obj`) is an instance of a given class (`jsClass`). This method will
     * return true if the `obj` is an instance of the `jsClass` itself, if it is a subclass of the `jsClass`,
     * or if the `jsClass` is a mixin on the `obj`. For more information about classes and mixins, see the
     * {@link #extend} method.
     *
     * @param {Mixed} obj The object (instance) to test.
     * @param {Function} jsClass The class (constructor function) of which to see if the `obj` is an instance of, or has a mixin of.
     * @return {Boolean} True if the obj is an instance of the jsClass (it is a direct instance of it,
     *   it inherits from it, or the jsClass is a mixin of it)
     */
    isInstanceOf : function( obj, jsClass ) {
      if( typeof jsClass !== 'function' ) {
        throw new Error( "jsClass argument of isInstanceOf method expected a Function (constructor function) for a JavaScript class" );
      }

      if( !Util.isObject( obj ) ) {
        return false;
      } else if( obj instanceof jsClass ) {
        return true;
      } else if( Class.hasMixin( obj.constructor, jsClass ) ) {
        return true;
      } else {
        return false;
      }
    },


    /**
     * Determines if a class (i.e. constructor function) is, or is a subclass of, the given `baseClass`.
     *
     * The order of the arguments follows how {@link #isInstanceOf} accepts them (as well as the JavaScript
     * `instanceof` operator. Try reading it as if there was a `subclassof` operator, i.e. `subcls subclassof supercls`.
     *
     * Example:
     *     var Superclass = Class.create( {} );
     *     var Subclass = Superclass.extend( {} );
     *
     *     Class.isSubclassOf( Subclass, Superclass );   // true - Subclass is derived from (i.e. extends) Superclass
     *     Class.isSubclassOf( Superclass, Superclass ); // true - Superclass is the same class as itself
     *     Class.isSubclassOf( Subclass, Subclass );     // true - Subclass is the same class as itself
     *     Class.isSubclassOf( Superclass, Subclass );   // false - Superclass is *not* derived from Subclass
     *
     * @param {Function} subclass The class to test.
     * @param {Function} superclass The class to test against.
     * @return {Boolean} True if the `subclass` is derived from `superclass` (or is equal to `superclass`), false otherwise.
     */
    isSubclassOf : function( subclass, superclass ) {
      if( typeof subclass !== 'function' || typeof superclass !== 'function' ) {
        return false;

      } else if( subclass === superclass ) {
        // `subclass` is `superclass`, return true
        return true;

      } else {
        // Walk the prototype chain of `subclass`, looking for `superclass`
        var currentClass = subclass,
            currentClassProto = currentClass.prototype;

        while( ( currentClass = ( currentClassProto = currentClass.__super__ ) && currentClassProto.constructor ) ) {  // extra set of parens to get JSLint to stop complaining about an assignment inside a while expression
          if( currentClassProto.constructor === superclass ) {
            return true;
          }
        }
      }

      return false;
    },



    /**
     * Determines if a class has a given mixin. Note: Most likely, you will want to use {@link #isInstanceOf} instead,
     * as that will tell you if the given class either extends another class, or either has, or extends a class with
     * a given mixin.
     *
     * @param {Function} classToTest
     * @param {Function} mixinClass
     * @return {Boolean}
     */
    hasMixin : function( classToTest, mixinClass ) {
      // Assign the mixinClass (the class we're looking for as a mixin) an ID if it doesn't yet have one. This is done
      // here (instead of in extend()) so that any class can be used as a mixin, not just ones extended from Class.js)
      var mixinClassId = mixinClass.__Class_classId;
      if( !mixinClassId ) {
        mixinClassId = mixinClass.__Class_classId = ++classIdCounter;  // classIdCounter is from outer anonymous function of this class, and is used to assign a unique ID
      }

      // Create a cache for quick re-lookups of the mixin on this class
      var hasMixinCache = classToTest.__Class_hasMixinCache;
      if( !hasMixinCache ) {
        hasMixinCache = classToTest.__Class_hasMixinCache = {};
      }

      // If we have the results of a call to this method for this mixin already, returned the cached result
      if( mixinClassId in hasMixinCache ) {
        return hasMixinCache[ mixinClassId ];

      } else {
        // No cached result from a previous call to this method for the mixin, do the lookup
        var mixins = classToTest.mixins,
            superclass = ( classToTest.superclass || classToTest.__super__ );

        // Look for the mixin on the classToTest, if it has any
        if( mixins ) {
          for( var i = 0, len = mixins.length; i < len; i++ ) {
            if( mixins[ i ] === mixinClass ) {
              return ( hasMixinCache[ mixinClassId ] = true );  // mixin was found, cache the result and return
            }
          }
        }

        // mixin wasn't found on the classToTest, check its superclass for the mixin (if it has one)
        if( superclass && superclass.constructor && superclass.constructor !== Object ) {
          var returnValue = Class.hasMixin( superclass.constructor, mixinClass );
          return ( hasMixinCache[ mixinClassId ] = returnValue );  // cache the result from the call to its superclass, and return that value

        } else {
          // mixin wasn't found, and the class has no superclass, cache the result and return false
          return ( hasMixinCache[ mixinClassId ] = false );
        }
      }
    }

  };
  /*global Class, Util */

  // A regular expression which is used to determine if a method calls its superclass method (using this._super())
  var superclassMethodCallRegex = /xyz/.test( function(){ var a = "xyz"; } ) ? /\b_super\b/ : /.*/;  // a regex to see if the _super() method is called within a function, for JS implementations that allow a function's text to be converted to a string. Note, need to keep the "xyz" as a string, so minifiers don't re-write it.


  // inline override() function which is attached to subclass constructor functions
  var inlineOverride = function( obj ) {
    for( var p in obj ) {
      this[ p ] = obj[ p ];
    }
  };


  /**
   * @private
   * @class Class.ClassBuilder
   * @singleton
   *
   * Performs the functionality of building a class. Used from {@link Class#extend}.
   */
  var ClassBuilder = {

    /**
     * Builds a class from the `superclass` and `overrides` for the subclass.
     *
     * @param {String} name A string name for the class.
     * @param {Function} superclass The constructor function of the class being extended.
     * @param {Object} overrides An object literal with members that make up the subclass's properties/method.
     * @return {Function} The subclass constructor from the `overrides` parameter, or a generated one if not provided.
     */
    build : function( name, superclass, overrides ) {
      // Grab any special properties from the overrides, and then delete them (except for `abstractClasss`) so that they
      // aren't applied to the subclass's prototype when we copy all of the 'overrides' properties there
      var abstractClass = !!overrides.abstractClass,
          statics = overrides.statics,
          inheritedStatics = overrides.inheritedStatics,
          mixins = overrides.mixins;

      delete overrides.statics;
      delete overrides.inheritedStatics;
      delete overrides.mixins;

      // --------------------------

      // Before creating the new subclass pre-process the methods of the subclass (defined in "overrides") to add the
      // this._super() method for methods that can call their associated superclass method. This should happen before
      // defining the new subclass, so that the constructor function can be wrapped as well.
      ClassBuilder.wrapSuperclassCallingMethods( superclass, overrides );

      // --------------------------


      // Now that preprocessing is complete, define the new subclass's constructor *implementation* function.
      // This is going to be wrapped in the actual subclass's constructor
      // This will be the actual implementation of the subclass's constructor (which the user defines), or a default
      // constructor which simply calls the superclass's constructor.
      var subclassCtorImplFn = ClassBuilder.createConstructor( superclass, overrides );


      // Create the actual subclass's constructor, which tests to see if the class being instantiated is abstract,
      // and if not, calls the subclassCtorFn implementation function
      var subclass = function() {
        var proto = this.constructor.prototype;
        if( proto.hasOwnProperty( 'abstractClass' ) && proto['abstractClass'] === true ) {
          var displayName = this.constructor.displayName;
          throw new Error( "Error: Cannot instantiate abstract class" + ( displayName ? " '" + displayName + "'" : "" ) );
        }

        // Call the actual constructor's implementation
        return subclassCtorImplFn.apply( this, arguments );
      };

      // Name provided, populate the special `displayName` property. Only supported by FF at the time of writing,
      // but may gain more support as time goes on.
      if( name ) {
        subclass.displayName = subclass.fullName = name;
      }

      ClassBuilder.createPrototypeChain( superclass, subclass );
      ClassBuilder.attachCommonSubclassStatics( subclass );
      ClassBuilder.attachCommonSubclassInstanceMethods( superclass, subclass );


      // Finally, add the properties/methods defined in the "overrides" config (which is basically the subclass's
      // properties/methods) onto the subclass prototype now.
      Class.override( subclass, overrides );


      // -----------------------------------

      // Check that if it is a concrete (i.e. non-abstract) class, that all abstract methods have been implemented
      // (i.e. that the concrete class overrides any `Class.abstractMethod` functions from its superclass)
      if( !abstractClass ) {
        ClassBuilder.checkAbstractMethodsImplemented( subclass );
      }

      // -----------------------------------

      // Now apply inherited statics to the class. Inherited statics from the superclass are first applied,
      // and then all overrides (so that subclasses's inheritableStatics take precedence)
      if( inheritedStatics || superclass.__Class_inheritedStatics ) {
        inheritedStatics = Util.assign( {}, angular.copy(superclass.__Class_inheritedStatics, {}), inheritedStatics );  // inheritedStatics takes precedence of the superclass's inherited statics
        Util.assign( subclass,  inheritedStatics);
        subclass.__Class_inheritedStatics = inheritedStatics;  // store the inheritedStatics for the next subclass
      }

      // Now apply statics to the class. These statics should override any inheritableStatics for the current subclass.
      // However, the inheritableStatics will still affect subclasses of this subclass.
      if( statics ) {
        Util.assign( subclass, statics );
      }


      // Handle mixins by applying their methods/properties to the subclass prototype. Methods defined by
      // the class itself will not be overwritten, and the later defined mixins take precedence over earlier
      // defined mixins. (Moving backwards through the mixins array to have the later mixin's methods/properties
      // take priority)
      if( mixins ) {
        ClassBuilder.applyMixins( subclass, mixins );
      }


      // If there is a static `onClassCreate` method, call it now with the new subclass as the argument
      var onClassCreate = subclass.onClassCreate || subclass.onClassCreated || subclass.onClassExtended;  // `onClassCreated` and `onClassExtended` are accepted for backward compatibility
      if( onClassCreate ) {
        onClassCreate.call( subclass, subclass );  // call in the scope of `subclass`, and with the `subclass` as the first argument
      }

      return subclass;
    },



    /**
     * @private
     * @param {Function} superclass
     * @param {Object} overrides
     */
    createConstructor : function( superclass, overrides ) {
      var subclassCtorImplFn;

      if( overrides.constructor !== Object ) {
        subclassCtorImplFn = overrides.constructor;
        delete overrides.constructor;  // Remove 'constructor' property from overrides here, so we don't accidentally re-apply it to the subclass prototype when we copy all properties over
      } else {
        subclassCtorImplFn = ( superclass === Object ) ? function(){} : function() { return superclass.apply( this, arguments ); };   // create a "default constructor" that automatically calls the superclass's constructor, unless the superclass is Object (in which case we don't need to, as we already have a new object)
      }

      return subclassCtorImplFn;
    },


    /**
     * @private
     * @param {Function} superclass
     * @param {Function} subclass
     */
    createPrototypeChain : function( superclass, subclass ) {
      var superclassPrototype = superclass.prototype,
          subclassPrototype = subclass.prototype;

      var F = function() {};
      F.prototype = superclassPrototype;
      subclassPrototype = subclass.prototype = new F();  // set up prototype chain
      subclassPrototype.constructor = subclass;          // fix constructor property
      subclass.superclass = subclass.__super__ = superclassPrototype;
      subclass.__Class = true;  // a flag for testing if a given function is a class or not
    },


    /**
     * @private
     * @param {Function} subclass
     */
    checkAbstractMethodsImplemented : function( subclass ) {
      var subclassPrototype = subclass.prototype;

      for( var methodName in subclassPrototype ) {
        if( subclassPrototype[ methodName ] === Class.abstractMethod ) {  // NOTE: Do *not* filter out prototype properties; we want to test them
          var displayName = subclassPrototype.constructor.displayName;
          if( subclassPrototype.hasOwnProperty( methodName ) ) {
            throw new Error( "The class " + ( displayName ? "'" + displayName + "'" : "being created" ) + " has abstract method '" + methodName + "', but is not declared with 'abstractClass: true'" );
          } else {
            throw new Error( "The concrete subclass " + ( displayName ? "'" + displayName + "'" : "being created" ) + " must implement abstract method: '" + methodName + "', or be declared abstract as well (using 'abstractClass: true')" );
          }
        }
      }
    },


    /**
     * @private
     * @param {Function} superclass
     * @param {Object} overrides
     */
    wrapSuperclassCallingMethods : function( superclass, overrides ) {
      var superclassPrototype = superclass.prototype;

      // Wrap all methods that use this._super() in the function that will allow this behavior (defined above), except
      // for the special 'constructor' property, which needs to be handled differently for IE (done below).
      for( var prop in overrides ) {
        if(
            prop !== 'constructor' &&                               // We process the constructor separately, below (which is needed for IE, because IE8 and probably all versions below it won't enumerate it in a for-in loop, for whatever reason...)
            overrides.hasOwnProperty( prop ) &&                     // Make sure the property is on the overrides object itself (not a prototype object)
            typeof overrides[ prop ] === 'function' &&              // Make sure the override property is a function (method)
            typeof superclassPrototype[ prop ] === 'function' &&    // Make sure the superclass has the same named function (method)
            !overrides[ prop ].hasOwnProperty( '__Class' ) &&       // We don't want to wrap a constructor function of another class being provided as a prototype property to the class being created
            superclassMethodCallRegex.test( overrides[ prop ] )     // And check to see if the string "_super" exists within the override function
        ) {
          overrides[ prop ] = ClassBuilder.createSuperclassCallingMethod( superclass, prop, overrides[ prop ] );
        }
      }

      // Process the constructor on its own, here, because IE8 (and probably all versions below it) will not enumerate it
      // in the for-in loop above (for whatever reason...)
      if(
          overrides.hasOwnProperty( 'constructor' ) &&  // make sure we don't get the constructor property from Object
          typeof overrides.constructor === 'function' &&
          typeof superclassPrototype.constructor === 'function' &&
          superclassMethodCallRegex.test( overrides.constructor )
      ) {
        overrides.constructor = ClassBuilder.createSuperclassCallingMethod( superclass, 'constructor', overrides.constructor );
      }
    },


    /**
     * @private
     * @param {Function} subclass
     */
    attachCommonSubclassStatics : function( subclass ) {
      // Attach new static methods to the subclass
      subclass.override = function( overrides ) { Class.override( subclass, overrides ); };
      subclass.extend = function( name, overrides ) {
        if( arguments.length === 1 ) {
          overrides = name;
          name = "";
        }
        return Class.extend( name, subclass, overrides );
      };
      subclass.hasMixin = function( mixin ) { return Class.hasMixin( subclass, mixin ); };
    },


    /**
     * Attach new instance methods to the subclass
     *
     * @private
     * @param {Function} superclass
     * @param {Function} subclass
     */
    attachCommonSubclassInstanceMethods : function( superclass, subclass ) {
      var subclassPrototype = subclass.prototype;
      subclassPrototype.superclass = subclassPrototype.supr = function() { return superclass.prototype; };
      subclassPrototype.override = inlineOverride;   // inlineOverride function defined above
      subclassPrototype.hasMixin = function( mixin ) { return Class.hasMixin( this.constructor, mixin ); };
    },


    /**
     * Creates a function that wraps methods of the new subclass that can call their superclass method.
     *
     * @private
     * @param {Function} superclass
     * @param {String} fnName
     * @param {Function} fn
     */
    createSuperclassCallingMethod : function( superclass, fnName, fn ) {
      var superclassPrototype = superclass.prototype;

      return function() {
        var tmpSuper = this._super,  // store any current _super reference, so we can "pop it off the stack" when the method returns
            scope = this;

        // Add the new _super() method that points to the superclass's method
        this._super = function( args ) {  // args is an array (or arguments object) of arguments
          return superclassPrototype[ fnName ].apply( scope, args || [] );
        };

        // Now call the target method
        var returnVal = fn.apply( this, arguments );

        // And finally, restore the old _super reference, as we leave the stack context
        this._super = tmpSuper;

        return returnVal;
      };
    },


    /**
     * @private
     * @param {Function} subclass
     * @param {Function[]} mixins
     */
    applyMixins : function( subclass, mixins ) {
      var subclassPrototype = subclass.prototype;

      for( var i = mixins.length-1; i >= 0; i-- ) {
        var mixinPrototype = mixins[ i ].prototype;
        for( var prop in mixinPrototype ) {
          // Do not overwrite properties that already exist on the prototype
          if( typeof subclassPrototype[ prop ] === 'undefined' ) {
            subclassPrototype[ prop ] = mixinPrototype[ prop ];
          }
        }
      }

      // Store which mixin classes the subclass has. This is used in the hasMixin() method
      subclass.mixins = mixins;
    }

  };
  /*global window, Class */

  // NOTE: This source file is wrapped in a UMD block / factory function when built,
  //       so these are not global.

  /**
   * @private
   * @class Class.Util
   * @singleton
   *
   * A few utility functions for Class.js
   */
  var Util = Class.Util = {

    /**
     * Assigns (shallow copies) the properties of `src` onto `dest`.
     *
     * @param {Object} dest The destination object.
     * @param {...Object} src The source object(s). They are processed in order from left to right.
     * @return {Object} The destination object.
     */
    assign : function( dest ) {
      var srcObjects = Array.prototype.slice.call( arguments, 1 );

      for( var i = 0, len = srcObjects.length; i < len; i++ ) {
        var srcObj = srcObjects[ i ];

        for( var prop in srcObj ) {
          if( srcObj.hasOwnProperty( prop ) ) {
            dest[ prop ] = srcObj[ prop ];
          }
        }
      }
      return dest;
    },


    /**
     * Determines if a value is an object.
     *
     * @param {Mixed} value
     * @return {Boolean} `true` if the value is an object, `false` otherwise.
     */
    isObject : function( value ) {
      return !!value && Object.prototype.toString.call( value ) === '[object Object]';
    },



    /**
     * Determines if on Internet Explorer, for dealing with IE's toString() problem.
     *
     * @return {Boolean} `true` if the browser is IE, `false` otherwise.
     */
    isIe : (function() {
      var isIe = false;
      if( typeof window !== 'undefined' && window.navigator && window.navigator.userAgent ) {
        var uA = window.navigator.userAgent.toLowerCase();
        isIe = /msie/.test( uA ) && !( /opera/.test( uA ) );
      }

      return function() { return isIe; };
    })()

  };

  return Class;
});


define('app/core/AngularClass', ['lib/Class'], function(Class) {
  return Class.create({
    inheritedStatics: {
      '$inject': [],
      inject: function() {
        for (var i = 0; i < arguments.length; i++) this['$inject'].push(arguments[i]);
        return this;
      }
    }
  });
});


define('app/constants/Events', [],function() {
  return {
    NEEDAUTH: 'app: need-auth'
  }
});


define('app/modules/api/HttpService', ['app/core/AngularClass', 'app/constants/Events'], function(AC, Events) {

  var tokens = [],
      $http,
      $q,
      $rootScope,
      $spinService,
      gSettings = window['gSettings'],
      host = (gSettings !== undefined && gSettings['backendURL'] !== undefined) ? gSettings['backendURL'] : document.location.protocol +
          '//' + document.location.host,
      ajaxURL = host + '/ajax.php',
      formURL = host + '/forms.php';

  return AC.extend('HttpService', {

    _$http: '',

    _$q: '',

    showSpinner: true,

    startStopSpinner: function(start) {
      var method = start ? 'spin' : 'stop';
      if (this.showSpinner) $spinService[method]('http-spinner');
    },

    constructor: function(_$rootScope, _$http, _$q, _spinService) {
      $http = _$http;
      $q = _$q;
      $spinService = _spinService;
      $rootScope = _$rootScope;
    },

    getToken: function() {
      var token = tokens.pop(),
          deferred = $q.defer();
      if (token) {
        deferred.resolve(token);
        return deferred.promise;
      } else {
        return this._doAJAX('authGetToken', {});
      }
    },

    request: function(classMethod, parameters, withToken) {
      var self = this;
      self.startStopSpinner(true);
      parameters = parameters || {};

      if (withToken) {
        return self.getToken().then(
            function(token) {
              return self._doAJAX(classMethod, angular.extend(parameters, {'token': token}));
            }
        );
      } else {
        return this._doAJAX(classMethod, parameters);
      }
    },

    requestWithToken: function(classMethod, parameters) {
      return this.request(classMethod, parameters, true);
    },

    requestFile: function(path, progressCallback, type) {
      this.startStopSpinner(true);
      return this._doAJAX('_getFile', {path: path, type: type || 'blob', onProgress: progressCallback});
    },

    submit: function(formName, parameters) {
      parameters['formName'] = formName;
      return this.request(null, parameters);
    },

    submitWithToken: function(formName, parameters) {
      parameters['formName'] = formName;
      return this.requestWithToken(null, parameters, true);
    },

    _doAJAX: function(classMethod, parameters) {

      var self = this,
          lang = gSettings['idLanguage'],
          postUrl = classMethod ? ajaxURL : formURL,
          deferred = $q.defer();

      function successFileCallback(result) {
        deferred.resolve(result['data']);
      }

      function successCallBack(result) {
        var error = null,
            data = result['data'];
        if (
            !data || data['data'] === undefined ||
            data['result'] === undefined ||
            data['errorCode'] === undefined ||
            data['errorMessage'] === undefined ||
            data['token'] === undefined) {    //Check packet format
          error = {errorCode: -1, errorMessage: 'Bad server answer: ' + data};
        }
        if (data['token']) {
          tokens.push(data['token']);
        }
        if (!data['result']) {
          error = {errorCode: data['errorCode'], errorMessage: data['errorMessage']};
        }
        if (error) {
          if (error.errorCode == 401) $rootScope.$broadcast(Events.NEEDAUTH);
          deferred.reject(error);

        } else {
          deferred.resolve(data['data']);
        }
      }

      function errorCallBack(response) {
        var error = {};
        /*if (errorThrown !== undefined) {
          error = {errorCode: -1, errorMessage: 'Ajax error: ' + errorThrown};
        } else {*/
        error = {errorCode: -1, errorMessage: 'Ajax error: ' + response['status'] + ' ' + response['statusText']};
        //}

        //$(document).trigger("gAjaxError.g", error);
        deferred.reject(error);
      }

      function finallyCallback() {
        self.startStopSpinner();
      }


      if (classMethod != '_getFile') {
        if (lang !== undefined) {
          parameters['idLanguage'] = lang;
        }

        if (classMethod) {
          parameters['ajaxCall'] = classMethod;
        }

        $http.post(postUrl, parameters)
          .then(successCallBack, errorCallBack)
          .finally (finallyCallback);
      } else {
        $http({
          'method': 'GET',
          'url': parameters.path,
          'data': {onprogress: parameters.onProgress},
          'responseType': parameters.type,
          'transformResponse': function(result) {
            return result;
          },
          'transformRequest': function(data) {
            return data;
          }
        })
          .then(successFileCallback, errorCallBack)
          .finally (finallyCallback);
      }

      return deferred.promise;

    }

  }).inject('$rootScope', '$http', '$q', 'SpinnerService');

});


define('app/modules/api/ApiService',
    ['app/core/AngularClass', 'app/modules/api/HttpService'],
    function(AC, HttpService) {

      var $httpService;

      function createLoginRequestData(email, password) {
        return {
          'email': email,
          'password': password
        }}

      function createRegisterRequestData(email, password, passwordConfirm, country) {
        return {
          'email': email,
          'password': password,
          'password1': passwordConfirm,
          'country': country
        }}

      function createBookListRequestData(start, end, list_id) {
        return {
          'list_id': list_id,
          'start': start,
          'end': end
        }}

      function createBookDetailsRequest(book_id) {
        return {
          'book_id': book_id
        }}

      function createSearchRequest(searchString) {
        return {
          'searchString': searchString
        }}

      return AC.extend('ApiService', {

        constructor: function(_$httpService) {
          $httpService = _$httpService;
        },

        getSession: function() {
          return $httpService.requestWithToken('getSession');
        },

        login: function(email, password) {
          return $httpService.submit('g-themes-webapp-login', createLoginRequestData(email, password));
        },

        register: function(email, password, passwordConfirm, country) {
          return $httpService.submit('g-themes-webapp-register', createRegisterRequestData(email, password, passwordConfirm, country));
        },

        getUserBooks: function(start, end) {
          return $httpService.requestWithToken('getUserBooks', createBookListRequestData(start, end));
        },

        getNewBooks: function(start, end) {
          return $httpService.requestWithToken('getNewBooks', createBookListRequestData(start, end));
        },

        getGenresList: function() {
          return $httpService.requestWithToken('getGenresList');
        },

        getBooksByGenre: function(start, end, genre_id) {
          return $httpService.requestWithToken('getBooksByGenre', createBookListRequestData(start, end, genre_id));
        },

        getBookDetails: function(book_id) {
          return $httpService.requestWithToken('getBookDetails', createBookDetailsRequest(book_id));
        },

        getBookLanguages: function() {
          return $httpService.requestWithToken('getAvailableLanguagesForBooks');
        },

        getBooksByLanguage: function(start, end, language_id) {
          return $httpService.requestWithToken('getBooksByLanguage', createBookListRequestData(start, end, language_id));
        },

        getSearchResults: function(searchString) {
          return $httpService.requestWithToken('getSearchResults', createSearchRequest(searchString));
        },

        getFile: function(path, progressCallback, type) {
          return $httpService.requestFile(path, progressCallback, type);
        },

        getEpub: function(book_id, progressCallback) {
          return $httpService.requestFile('moby-dick.epub', progressCallback, 'arraybuffer');
        }

      }).inject(HttpService.fullName);
    });


define('app/RouteResolver', ['app/modules/api/ApiService'], function(ApiService) {

  var CACHEKEY = 'booqla',
      instantiated = false,
      $q,
      $apiService,
      $cache,
      $route;

  function getDataDeferred(caller, apiCall, args) {
    var key = String(caller),
        deferred = $q.defer(),
        data = $cache.get(key);

    if (!data) {
      apiCall.apply(null, args).then(
          function(data) {
            $cache.put(key, data);
            deferred.resolve(data);
            return data;
          },
          function(error) {
            deferred.reject(error);
          });
    } else {
      deferred.resolve(data);
    }

    return deferred.promise;
  }

  function getId() {return parseInt($route.current.params['id'], 10)}

  function getGenreList() {return getDataDeferred(getGenreList, $apiService.getGenresList)}

  function getLanguageList() {return getDataDeferred(getLanguageList, $apiService.getBookLanguages)}

  function getGenreBooksList() {
    var deferred = $q.defer(),
        title_id = getId();

    getGenreList().then(
        function(genres) {deferred.resolve(genres[title_id]['title'])},
        function(error) {deferred.reject(error)}
    );

    return deferred.promise;
  }

  function getLanguageBooksList() {
    var deferred = $q.defer(),
        title_id = getId();

    getLanguageList().then(
        function(languages) {deferred.resolve(languages[title_id]['title'])},
        function(error) {deferred.reject(error)}
    );

    return deferred.promise;
  }

  function createResolver(func) {

    function instantiate(_$q, _$route, _$cacheFactory, _$apiService) {
      $q = _$q;
      $route = _$route;
      $cache = _$cacheFactory(CACHEKEY);
      $apiService = _$apiService;
      instantiated = true;
    }

    function resolverFunc() {
      if (!instantiated) {
        instantiate.apply(null, arguments);
      }
      return func.call();
    }
    resolverFunc['$inject'] = ['$q', '$route', '$cacheFactory', ApiService.fullName];
    return resolverFunc;
  }

  return {
    GENRES: {'data': createResolver(getGenreList)},
    LANGUAGES: {'data': createResolver(getLanguageList)},
    GENREBOOKS: {'title': createResolver(getGenreBooksList)},
    LANGUAGEBOOKS: {'title': createResolver(getLanguageBooksList)}
  };
});


define('app/core/BaseController', ['app/core/AngularClass'], function(AC) {
  return AC.extend({

    $scope: '',

    constructor: function(_$scope) {
      this.$scope = _$scope;
      this.init();
      this.defineListeners(_$scope);
      this.defineScope(_$scope);
    },

    init: function() {
      //OVERRIDE
    },

    defineListeners: function() {
      //OVERRIDE
    },

    defineScope: function() {
      //OVERRIDE
    }
  }).inject('$scope');
});


define('app/controllers/AppController',
    ['app/core/BaseController'],
    function(BaseController) {

      function setMenuState(state) {
        menuState['opened'] = !!state;
      }

      function toggleMenuState() {
        setMenuState(!menuState['opened']);
      }

      function doNavigate(path, params) {
        setMenuState();
        if (params) {
          $location.search(params);
        } else {
          $location['$$search'] = {};
        }
        $location.path(path);
      }

      var menuState = {'opened': false},
          genreTitle = '',
          navigate = {
            'login': function() {doNavigate('/login')},
            'home': function() {doNavigate('/nav/shelf')},
            'genres': function() {doNavigate('/nav/genres')},
            'new': function() {doNavigate('/nav/new')},
            'registration': function() {doNavigate('/nav/registration')},
            'showGenreBooks': function(genre) {doNavigate('/nav/genres/' + genre['id'])},
            'showBook': function(book) {doNavigate('/nav/book/' + book['id'])},
            'buyBook': function(book, $event) {if ($event) $event.stopPropagation(); console.log('buy book')},
            'readBook': function(book) {doNavigate('/nav/book/' + book['id'] + '/read')},
            'langs': function() {doNavigate('/nav/languages')},
            'showLangBooks': function(lang) {doNavigate('/nav/languages/' + lang['id'])},
            'search': function(value) {var params = value ? {'str': value} : ''; doNavigate('/nav/search', params)},
            'go': function(path) {
              doNavigate(path);
            }
          },
          $location,
          AppController = BaseController.extend('AppController', {
            constructor: function(_$scope, _$location) {
              $location = _$location;
              this._super([_$scope]);
            },

            defineScope: function(_$scope) {
              var scopeExtension = {
                'log': function(arg) {
                  window.console.log(arg);
                },
                'navigate': navigate,
                'menuState': menuState,
                'toggleMenu': toggleMenuState,
                'closeMenu': function() {
                  setMenuState();
                }
              };

              angular.extend(_$scope, scopeExtension);
            }
          });

      AppController.inject('$location'/*, SessionService.fullName, HttpService.fullName*/);
      return AppController;
    });


define('app/services/SessionService', ['app/core/AngularClass', 'app/modules/api/ApiService'], function(AC, ApiService) {

  var $apiService;

  return AC.extend('SessionService', {

    userID: '',

    email: '',

    constructor: function(_$apiService) {
      $apiService = _$apiService;
    },

    load: function() {
      var self = this,
          request = $apiService.getSession();

      request.then(function(data) {
        self.create(data);
        return data;
      }, function(error) {
        return error;
      });

      return request;
    },

    create: function(data) {
      this.userID = data['userID'];
      this.email = data['email'];
    },

    reset: function() {
      this.userID = this.email = '';
    }
  }).inject(ApiService.fullName);
});


define('app/services/AuthService',
    ['app/core/AngularClass',
      'app/modules/api/ApiService',
      'app/services/SessionService'],
    function(AC, ApiService, SessionService) {

      var $apiService, $sessionService, $q;

      return AC.extend('AuthService', {
        constructor: function(_$ApiService, _$SessionService, _$q) {
          $apiService = _$ApiService;
          $sessionService = _$SessionService;
          $q = _$q;
        },

        login: function(credentials) {
          return $apiService.login(credentials);
        },

        register: function(regInfo) {
          return $apiService.register(regInfo);
        },

        isLoggedIn: function() {
          if ($sessionService.userID) {
            return true;
          } else {
            return $sessionService.load();
          }
        }
      }).inject(ApiService.fullName, SessionService.fullName, '$q');
    });

/*
 * JavaScript MD5 1.0.1
 * https://github.com/blueimp/JavaScript-MD5
 *
 * Copyright 2011, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 *
 * Based on
 * A JavaScript implementation of the RSA Data Security, Inc. MD5 Message
 * Digest Algorithm, as defined in RFC 1321.
 * Version 2.2 Copyright (C) Paul Johnston 1999 - 2009
 * Other contributors: Greg Holt, Andrew Kepert, Ydnar, Lostinet
 * Distributed under the BSD License
 * See http://pajhome.org.uk/crypt/md5 for more info.
 */

/*jslint bitwise: true */
/*global unescape, define */

define('lib/md5', [],function() {

  /*
   * Add integers, wrapping at 2^32. This uses 16-bit operations internally
   * to work around bugs in some JS interpreters.
   */
  function safe_add(x, y) {
    var lsw = (x & 0xFFFF) + (y & 0xFFFF),
        msw = (x >> 16) + (y >> 16) + (lsw >> 16);
    return (msw << 16) | (lsw & 0xFFFF);
  }

  /*
   * Bitwise rotate a 32-bit number to the left.
   */
  function bit_rol(num, cnt) {
    return (num << cnt) | (num >>> (32 - cnt));
  }

  /*
   * These functions implement the four basic operations the algorithm uses.
   */
  function md5_cmn(q, a, b, x, s, t) {
    return safe_add(bit_rol(safe_add(safe_add(a, q), safe_add(x, t)), s), b);
  }
  function md5_ff(a, b, c, d, x, s, t) {
    return md5_cmn((b & c) | ((~b) & d), a, b, x, s, t);
  }
  function md5_gg(a, b, c, d, x, s, t) {
    return md5_cmn((b & d) | (c & (~d)), a, b, x, s, t);
  }
  function md5_hh(a, b, c, d, x, s, t) {
    return md5_cmn(b ^ c ^ d, a, b, x, s, t);
  }
  function md5_ii(a, b, c, d, x, s, t) {
    return md5_cmn(c ^ (b | (~d)), a, b, x, s, t);
  }

  /*
   * Calculate the MD5 of an array of little-endian words, and a bit length.
   */
  function binl_md5(x, len) {
    /* append padding */
    x[len >> 5] |= 0x80 << (len % 32);
    x[(((len + 64) >>> 9) << 4) + 14] = len;

    var i, olda, oldb, oldc, oldd,
        a = 1732584193,
        b = -271733879,
        c = -1732584194,
        d = 271733878;

    for (i = 0; i < x.length; i += 16) {
      olda = a;
      oldb = b;
      oldc = c;
      oldd = d;

      a = md5_ff(a, b, c, d, x[i], 7, -680876936);
      d = md5_ff(d, a, b, c, x[i + 1], 12, -389564586);
      c = md5_ff(c, d, a, b, x[i + 2], 17, 606105819);
      b = md5_ff(b, c, d, a, x[i + 3], 22, -1044525330);
      a = md5_ff(a, b, c, d, x[i + 4], 7, -176418897);
      d = md5_ff(d, a, b, c, x[i + 5], 12, 1200080426);
      c = md5_ff(c, d, a, b, x[i + 6], 17, -1473231341);
      b = md5_ff(b, c, d, a, x[i + 7], 22, -45705983);
      a = md5_ff(a, b, c, d, x[i + 8], 7, 1770035416);
      d = md5_ff(d, a, b, c, x[i + 9], 12, -1958414417);
      c = md5_ff(c, d, a, b, x[i + 10], 17, -42063);
      b = md5_ff(b, c, d, a, x[i + 11], 22, -1990404162);
      a = md5_ff(a, b, c, d, x[i + 12], 7, 1804603682);
      d = md5_ff(d, a, b, c, x[i + 13], 12, -40341101);
      c = md5_ff(c, d, a, b, x[i + 14], 17, -1502002290);
      b = md5_ff(b, c, d, a, x[i + 15], 22, 1236535329);

      a = md5_gg(a, b, c, d, x[i + 1], 5, -165796510);
      d = md5_gg(d, a, b, c, x[i + 6], 9, -1069501632);
      c = md5_gg(c, d, a, b, x[i + 11], 14, 643717713);
      b = md5_gg(b, c, d, a, x[i], 20, -373897302);
      a = md5_gg(a, b, c, d, x[i + 5], 5, -701558691);
      d = md5_gg(d, a, b, c, x[i + 10], 9, 38016083);
      c = md5_gg(c, d, a, b, x[i + 15], 14, -660478335);
      b = md5_gg(b, c, d, a, x[i + 4], 20, -405537848);
      a = md5_gg(a, b, c, d, x[i + 9], 5, 568446438);
      d = md5_gg(d, a, b, c, x[i + 14], 9, -1019803690);
      c = md5_gg(c, d, a, b, x[i + 3], 14, -187363961);
      b = md5_gg(b, c, d, a, x[i + 8], 20, 1163531501);
      a = md5_gg(a, b, c, d, x[i + 13], 5, -1444681467);
      d = md5_gg(d, a, b, c, x[i + 2], 9, -51403784);
      c = md5_gg(c, d, a, b, x[i + 7], 14, 1735328473);
      b = md5_gg(b, c, d, a, x[i + 12], 20, -1926607734);

      a = md5_hh(a, b, c, d, x[i + 5], 4, -378558);
      d = md5_hh(d, a, b, c, x[i + 8], 11, -2022574463);
      c = md5_hh(c, d, a, b, x[i + 11], 16, 1839030562);
      b = md5_hh(b, c, d, a, x[i + 14], 23, -35309556);
      a = md5_hh(a, b, c, d, x[i + 1], 4, -1530992060);
      d = md5_hh(d, a, b, c, x[i + 4], 11, 1272893353);
      c = md5_hh(c, d, a, b, x[i + 7], 16, -155497632);
      b = md5_hh(b, c, d, a, x[i + 10], 23, -1094730640);
      a = md5_hh(a, b, c, d, x[i + 13], 4, 681279174);
      d = md5_hh(d, a, b, c, x[i], 11, -358537222);
      c = md5_hh(c, d, a, b, x[i + 3], 16, -722521979);
      b = md5_hh(b, c, d, a, x[i + 6], 23, 76029189);
      a = md5_hh(a, b, c, d, x[i + 9], 4, -640364487);
      d = md5_hh(d, a, b, c, x[i + 12], 11, -421815835);
      c = md5_hh(c, d, a, b, x[i + 15], 16, 530742520);
      b = md5_hh(b, c, d, a, x[i + 2], 23, -995338651);

      a = md5_ii(a, b, c, d, x[i], 6, -198630844);
      d = md5_ii(d, a, b, c, x[i + 7], 10, 1126891415);
      c = md5_ii(c, d, a, b, x[i + 14], 15, -1416354905);
      b = md5_ii(b, c, d, a, x[i + 5], 21, -57434055);
      a = md5_ii(a, b, c, d, x[i + 12], 6, 1700485571);
      d = md5_ii(d, a, b, c, x[i + 3], 10, -1894986606);
      c = md5_ii(c, d, a, b, x[i + 10], 15, -1051523);
      b = md5_ii(b, c, d, a, x[i + 1], 21, -2054922799);
      a = md5_ii(a, b, c, d, x[i + 8], 6, 1873313359);
      d = md5_ii(d, a, b, c, x[i + 15], 10, -30611744);
      c = md5_ii(c, d, a, b, x[i + 6], 15, -1560198380);
      b = md5_ii(b, c, d, a, x[i + 13], 21, 1309151649);
      a = md5_ii(a, b, c, d, x[i + 4], 6, -145523070);
      d = md5_ii(d, a, b, c, x[i + 11], 10, -1120210379);
      c = md5_ii(c, d, a, b, x[i + 2], 15, 718787259);
      b = md5_ii(b, c, d, a, x[i + 9], 21, -343485551);

      a = safe_add(a, olda);
      b = safe_add(b, oldb);
      c = safe_add(c, oldc);
      d = safe_add(d, oldd);
    }
    return [a, b, c, d];
  }

  /*
   * Convert an array of little-endian words to a string
   */
  function binl2rstr(input) {
    var i,
        output = '';
    for (i = 0; i < input.length * 32; i += 8) {
      output += String.fromCharCode((input[i >> 5] >>> (i % 32)) & 0xFF);
    }
    return output;
  }

  /*
   * Convert a raw string to an array of little-endian words
   * Characters >255 have their high-byte silently ignored.
   */
  function rstr2binl(input) {
    var i,
        output = [];
    output[(input.length >> 2) - 1] = undefined;
    for (i = 0; i < output.length; i += 1) {
      output[i] = 0;
    }
    for (i = 0; i < input.length * 8; i += 8) {
      output[i >> 5] |= (input.charCodeAt(i / 8) & 0xFF) << (i % 32);
    }
    return output;
  }

  /*
   * Calculate the MD5 of a raw string
   */
  function rstr_md5(s) {
    return binl2rstr(binl_md5(rstr2binl(s), s.length * 8));
  }

  /*
   * Calculate the HMAC-MD5, of a key and some data (raw strings)
   */
  function rstr_hmac_md5(key, data) {
    var i,
        bkey = rstr2binl(key),
        ipad = [],
        opad = [],
        hash;
    ipad[15] = opad[15] = undefined;
    if (bkey.length > 16) {
      bkey = binl_md5(bkey, key.length * 8);
    }
    for (i = 0; i < 16; i += 1) {
      ipad[i] = bkey[i] ^ 0x36363636;
      opad[i] = bkey[i] ^ 0x5C5C5C5C;
    }
    hash = binl_md5(ipad.concat(rstr2binl(data)), 512 + data.length * 8);
    return binl2rstr(binl_md5(opad.concat(hash), 512 + 128));
  }

  /*
   * Convert a raw string to a hex string
   */
  function rstr2hex(input) {
    var hex_tab = '0123456789abcdef',
        output = '',
        x,
        i;
    for (i = 0; i < input.length; i += 1) {
      x = input.charCodeAt(i);
      output += hex_tab.charAt((x >>> 4) & 0x0F) + hex_tab.charAt(x & 0x0F);
    }
    return output;
  }

  /*
   * Encode a string as utf-8
   */
  function str2rstr_utf8(input) {
    return unescape(encodeURIComponent(input));
  }

  /*
   * Take string arguments and return either raw or hex encoded strings
   */
  function raw_md5(s) {
    return rstr_md5(str2rstr_utf8(s));
  }
  function hex_md5(s) {
    return rstr2hex(raw_md5(s));
  }
  function raw_hmac_md5(k, d) {
    return rstr_hmac_md5(str2rstr_utf8(k), str2rstr_utf8(d));
  }
  function hex_hmac_md5(k, d) {
    return rstr2hex(raw_hmac_md5(k, d));
  }

  function md5(string, key, raw) {
    if (!key) {
      if (!raw) {
        return hex_md5(string);
      }
      return raw_md5(string);
    }
    if (!raw) {
      return hex_hmac_md5(key, string);
    }
    return raw_hmac_md5(key, string);
  }

  return md5;

});


define('app/controllers/LoginController',
    ['app/core/BaseController',
      'app/services/AuthService',
      'app/services/SessionService',
      'app/modules/api/ApiService',
      'lib/md5'
    ],
    function(BaseController, AuthService, SessionService, ApiService, MD5) {

      var $authService,
          $sessionService,
          $apiService;

      return BaseController.extend('LoginController', {

        constructor: function(_$scope, _$authService, _$sessionService, _$apiService) {
          $authService = _$authService;
          $sessionService = _$sessionService;
          $apiService = _$apiService;
          this._super([_$scope]);
          console.log(MD5('123456789'));
        },

        defineScope: function(_$scope) {
          _$scope['credentials'] = {'email': '', 'password': ''};
          _$scope['login'] = function(credentials) {

            var form = _$scope['loginForm'];
            angular.forEach(credentials, function(value, key) {
              var field = form[key];
              field.$setTouched();
              field.$setDirty();
            });
            /*if (form.$invalid) {
              alert('Invalid data');
            } else {*/
            $authService.login(credentials).then(function() {
              $sessionService.create({'userID': 1}); //TODO: remove this in production
              _$scope['navigate']['home']();
            });
          };
          //};
        }
      }).inject(AuthService.fullName, SessionService.fullName, ApiService.fullName);

    });


define('app/controllers/RegisterController',
    ['app/core/BaseController',
      'app/services/AuthService',
      'app/services/SessionService'
    ],
    function(BaseController, AuthService, SessionService) {
      var $authService,
          $sessionService,
          $location;

      return BaseController.extend('RegisterController', {
        constructor: function(_$scope, _$location, _$authService, _$SessionService) {
          $location = _$location;
          $authService = _$authService;
          $sessionService = _$SessionService;
          this._super([_$scope]);
        },

        defineScope: function(_$scope) {
          _$scope['regInfo'] = {
            'email': '',
            'password': '',
            'password1': '',
            'country': ''
          };
          _$scope['register'] = function(regInfo) {
            var form = _$scope['registerForm'];

            angular.forEach(regInfo, function(value, key) {
              var field = form[key];
              field.$setTouched();
              field.$setDirty();
            });

            if (form.$invalid) {
              alert('Invalid data');
            } else {
              $authService.register(regInfo).then(function() {
                $sessionService.create({'userID': 1}); //TODO: remove this in production
                $location.path('/nav/shelf');
              });
            }
          };
        }
      }).inject('$location', AuthService.fullName, SessionService.fullName);

    });


define('app/core/ListController', ['app/core/BaseController', 'app/modules/api/ApiService'], function(BaseController, ApiService) {
  var $route,
      ListController = BaseController.extend('ListController', {

        buffer: '',

        fetchAll: true,

        nextSubPath: '',

        addToScope: '',

        index: 'id',

        apiService: '',

        api: '', //OVERRIDE

        something: this,

        constructor: function(_$scope, _$route, _$apiService) {
          var originalPath = _$route.current.originalPath;
          $route = _$route;
          this.apiService = _$apiService;
          if (!angular.isArray(this.buffer)) this.buffer = [];
          if (!this.path) this.path = this.nextSubPath ? originalPath + '/' + this.nextSubPath : originalPath;
          this._super([_$scope]);
        },

        defineScope: function(_$scope) {
          var self = this;
          _$scope['provider'] = self.getDataProvider();
          angular.forEach(this.addToScope, function(value, key) {
            _$scope[key] = value;
          });
        },

        fetch: function(index, count) {
          var api = this.api;
          if (!api) throw ('No api specified');
          var promise = api(index, count, $route.current.params['id']);
          promise.then(this.parseResponse.bind(this));
          return promise;
        },

        parseResponse: function(response) {
          return response; //OVERRIDE
        },

        getDataProvider: function() {
          var self = this;

          return {
            'get': function(index, count, successCallBack) {
              var start = Math.max(0, index),
                  end = index + count - 1,
                  buffer = self.buffer;

              function returnEmpty() {
                successCallBack([]);
              }

              function returnFromFullDataCache() {
                return buffer.slice(start, end + 1);
              }

              if (start > end) {
                returnEmpty();
                return;
              }

              if (self.fetchAll) {
                // In the case of fullData response
                if (buffer.length) {
                  // If already fetched
                  successCallBack(returnFromFullDataCache());
                } else {
                  self.fetch().then(
                      // success
                      function(data) {
                        buffer = self.buffer = data;
                        successCallBack(returnFromFullDataCache());
                      },
                      // error
                      function(error) {
                        returnEmpty();
                      });
                }
              } else {
                //fetch by start/end
                /*if (buffer[start]) {
                  //if we have data in cache
                  successCallBack(buffer[start]);
                } else {*/
                //if not
                self.fetch(start, end + 1).then(
                    //success
                    function(data) {
                      //buffer[start] = data;
                      //successCallBack(buffer[start]);
                      successCallBack(data);
                    },
                    //error
                    function(error) {
                      returnEmpty();
                    }
                );
                //}
              }
            }
          };
        }
      });

  ListController.inject('$route', ApiService.fullName);
  return ListController;
});


define('app/models/Book', ['lib/Class'], function(Class) {

  return Class.create('Book', {
    'id': '',
    'thumbBlob': '',
    'thumb': '',
    'epubBlob': '',
    'title': '',
    'author': '',
    'price': '',
    'status': '',
    'stored': '',
    'opened': '',

    constructor: function(data) {
      if (angular.isObject(data)) {
        this['id'] = data['id'];
        this['title'] = data['title'];
        this['author'] = data['author'];
        this['price'] = data['price'];
        this['thumb'] = data['thumb'];
        this['status'] = data['status'];
        this['stored'] = data['stored'];
        this['opened'] = data['opened'];
      }
    }
  });

});


define('app/factories/BookFactory', ['app/models/Book'], function(BookModel) {

  function generateBookKey(book_id) {
    if (angular.isDefined(book_id) && book_id >= 0) return 'book_' + book_id;
  }

  function generateThumbKey(book_id) {
    return generateBookKey(book_id) + '_thumb';
  }

  function generateEpubKey(book_id) {
    return generateBookKey(book_id) + '_epub';
  }

  var urlCreator = window['URL'] || window['webkitURL'],
      $apiService,
      $storageService,
      $q,

      factory = function(_$apiService, _$storageService, _$q) {
        $apiService = _$apiService;
        $storageService = _$storageService;
        $q = _$q;
        return {

          bookFromData: function(data) {
            return new BookModel(data);
          },

          bookFromStored: function(book_id) {
            var deferred = $q.defer(), result = {};
            if (angular.isDefined(book_id) && book_id >= 0) {
              $storageService.getItem(generateBookKey(book_id))
                .then(
                  function(book) {
                    result = new BookModel(book);
                    return book;
                  },
                  function(error) {
                    deferred.reject(error);
                  })
                .then(
                  function() {
                    $storageService.getItem(generateThumbKey(book_id))
                      .then(
                        function(blob) {
                          result['thumbBlob'] = blob;
                          result['thumb'] = urlCreator['createObjectURL'](blob);
                          $storageService.getItem(generateEpubKey(book_id)).then(
                            function(epub) {
                              result['epubBlob'] = epub;
                              deferred.resolve(result);
                            },
                            function(error) {
                              deferred.reject(error);
                              console.log('error: ', error);
                            }
                          );
                        },
                        function(error) {
                          deferred.reject(error);
                        });
                  });
            }
            return deferred.promise;
          },

          storeEpub: function(book_id, blob) {
            var epub_id = generateEpubKey(book_id);
            return $storageService.setItem(epub_id, blob);
          },

          storeBook: function(book) {
            var deferred = $q.defer();
            if (book['stored']) return book;
            var storedBook = new BookModel(book);
            storedBook['stored'] = storedBook['opened'] = Date.now();
            $apiService.getFile(book['thumb'])
              .then(
                function(blob) {
                  storedBook['thumbBlob'] = blob;
                  return blob;
                },
                function(httpError) {
                  deferred.reject(httpError);
                })
              .then(function(blob) {
                  var blob_id = generateThumbKey(storedBook['id']);
                  $storageService.setItem(blob_id, blob)
                    .then(
                      function(ok) {
                        $storageService.setItem(generateBookKey(storedBook['id']), storedBook)
                          .then(
                          function(ok) {
                            storedBook['thumb'] = urlCreator['createObjectURL'](blob);
                            deferred.resolve(storedBook);
                          },
                          function(saveBookError) {
                            $storageService.removeItem(blob_id);
                            deferred.reject(saveBookError);
                          }
                          )},
                      function(saveBlobError) {
                        deferred.reject(saveBlobError);
                      });
                });

            return deferred.promise;
          },

          removeFromStore: function(book) {
            var deferred = $q.defer(),
                book_id = book['id'];
            if (!book.stored) {
              deferred.resolve();
              return;
            }

            $storageService.removeItem(generateBookKey(book_id)).then(
                function(ok) {
                  $storageService.removeItem(generateThumbKey(book_id))
                    .then(
                      function(ok) {
                        deferred.resolve();
                      },
                      function(error) {
                        deferred.resolve(error);
                      });
                },
                function(error) {
                  deferred.resolve(error);
                }
            );

            return deferred.promise;
          },

          revokeThumb: function(book) {
            if (book['thumbBlob']) urlCreator['revokeObjectURL'](book['thumb']);
          }
        }};

  factory.fullName = 'BookFactory';
  return factory;
});


define('app/controllers/MyBooksController', ['app/core/ListController', 'app/factories/BookFactory'], function(ListController, BookFactory) {
  var SHELFCAPACITY = 3,
      $localForage,
      $apiService,
      $bookFactory;

  return ListController.extend('MyBooksController', {

    path: 'book',

    fetchAll: true,

    bookShelf: '',

    bookList: '',

    constructor: function(_$scope, _$route, _$apiService, _$localForage, _$bookFactory) {
      $localForage = _$localForage;
      $apiService = _$apiService;
      $bookFactory = _$bookFactory;
      this.bookShelf = [];
      this.bookList = new Array(SHELFCAPACITY);
      var self = this, i = 0;

      for (; i < SHELFCAPACITY; i++) {
        this.bookShelf.push({});
      }

      $localForage.getItem('bookshelf').then(
          function(data) {
            if (!data) {
              $localForage.setItem('bookshelf', self.bookList);
            } else {
              data.forEach(function(book_id, index) {
                $bookFactory.bookFromStored(book_id).then(
                    function(book) {
                      self.bookShelf[index] = book;
                    }
                );
              });

            }
          },
          function(error) {console.log('error', error)}
      );
      this._super(arguments);
      this.api = $apiService.getUserBooks;
    },

    parseResponse: function(data) {
      data.forEach(function(item, index) {
        data[index] = $bookFactory.bookFromData(item);
      });
      return data;
    },

    defineScope: function(_$scope) {
      this._super([_$scope]);
      _$scope['addToShelf'] = this.addToShelf.bind(this);
      _$scope['bookShelf'] = this.bookShelf;
    },

    defineListeners: function(_$scope) {
      var self = this;
      _$scope.$on('$destroy', function() {
        self.bookShelf.forEach(function(book) {
          $bookFactory.revokeThumb(book);
        });
      });
    },

    addToShelf: function(book) {
      var self = this,
          place = -1,
          min = Date.now(),
          i = 0;

      //if we have this book already
      for (i = 0; i < SHELFCAPACITY; i++) {
        if (book['id'] == self.bookList[i]) return;
      }

      //seek free space
      for (i = 0; i < SHELFCAPACITY; i++) {
        if (!self.bookList[i]) {
          place = i;
          break;
        }
      }

      //seek first opened
      if (place < 0) {
        self.bookShelf.forEach(function(book, index) {
          var opened = book['opened'];
          if (angular.isDefined(opened) && opened < min) {
            min = opened;
            place = index;
          }
        });
      }

      $bookFactory.storeBook(book).then(
          function(storedBook) {
            var oldBook = self.bookShelf[place];
            if (oldBook['stored']) {
              $bookFactory.removeFromStore(oldBook);
            }
            self.bookShelf[place] = storedBook;
            self.bookList[place] = storedBook['id'];
            $localForage.setItem('bookshelf', self.bookList);
          },
          function(error) {
            console.log(error);
          }
      );
    }

  }).inject('$localForage', BookFactory.fullName);

});


define('app/controllers/GenresController', ['app/core/ListController'], function(ListController) {
  return ListController.extend('GenresController', {
    constructor: function() {
      this.buffer = arguments[arguments.length - 1]; //data
      this._super(arguments);
      this.api = this.apiService.getGenresList;
    }
  }).inject('data');

});


define('app/controllers/GenreBooksController', ['app/core/ListController'], function(ListController) {
  return ListController.extend('GenreBooksController', {

    fetchAll: false,

    path: '/nav/book',

    constructor: function() {
      this.addToScope = {
        'title' : arguments[arguments.length - 1]
      };
      this._super(arguments);
      this.api = this.apiService.getBooksByGenre;
    }
  }).inject('title');
});


define('app/controllers/LanguagesController', ['app/core/ListController'], function(ListController) {
  return ListController.extend({
    constructor: function() {
      this.buffer = arguments[arguments.length - 1]; //data
      this._super(arguments);
      this.api = this.apiService.getBookLanguages;
    }
  });
});


define('app/controllers/LanguageBooksController', ['app/core/ListController'], function(ListController) {
  return ListController.extend({

    fetchAll: false,

    path: '/nav/book',

    constructor: function() {
      this.addToScope = {
        'title' : arguments[arguments.length - 1]
      };
      this._super(arguments);
      this.api = this.apiService.getBooksByGenre;
    }
  }).inject('title');
});

define('app/controllers/BookDetailsController',
    ['app/core/BaseController', 'app/modules/api/ApiService'],
    function(BaseController, ApiService) {
      var $apiService, $route;
      return BaseController.extend('BookDetailsController', {

        constructor: function(_$scope, _$apiService, _$route) {
          $apiService = _$apiService;
          $route = _$route;
          this._super([_$scope]);
        },

        getBookData: function(_scopeObj) {
          var promise = $apiService.getBookDetails({'id': $route.current.params['id']});
          promise.then(function(data) {
            angular.forEach(data, function(value, key) {
              _scopeObj[key] = value;
            });
            var summary = data['summary'],
                shortSummary = summary.substring(0, 180);

            if (shortSummary.length == summary.length) _scopeObj['fullSummary'] = summary; else _scopeObj['shortSummary'] = shortSummary + '...';

          });
          return promise;
        },

        defineScope: function(_$scope) {
          var scopeExtension = {
            'book': {},
            'showSummary': function() {
              var info = _$scope['book'];
              info['shortSummary'] = '';
              info['fullSummary'] = info['summary'];
            }
          };

          angular.extend(_$scope, scopeExtension);
          this.getBookData(_$scope['book']);
        }
      }).inject(ApiService.fullName, '$route');
    });


define('app/controllers/NewBooksController', ['app/core/ListController'], function(ListController) {

  return ListController.extend('MyBooksController', {

    path: '/nav/book',

    fetchAll: false,

    constructor: function() {
      this._super(arguments);
      this.api = this.apiService.getNewBooks;
    }

  });

});

define('app/controllers/SearchController', ['app/core/ListController'], function(ListController) {
  var $location, $q;
  return ListController.extend('SearchController', {

    searchString: '',

    similarSearches: '',

    constructor: function(_$scope, _$route, _$apiService, _$q, _$location) {
      $q = _$q;
      $location = _$location;
      this.searchString = $location.search()['str'];
      this.similarSearches = [];
      this._super(arguments);
      this.api = this.apiService.getSearchResults;
      this.search(this.searchString);
    },

    init: function() {
      this.addToScope = {
        'searchString': this.searchString,
        'search': this.search.bind(this),
        'similar': this.similarSearches
      }},

    search: function(str) {
      str = str ? str.trim() : '';
      if (!str.length) return;
      var self = this;
      self.$scope['searchString'] = str;
      $location.search({'str': str});
      this.apiService.getSearchResults(str).then(
          function(result) {
            self.searchResult = result;
            self.buffer = result['result'];
            self.similarSearches = self.$scope['similar'] = result['similar'];
            self.$scope['provider'].adapter.reload();
          },
          function(error) {
            console.log(error);
          });
    },

    //stub
    fetch: function() {
      var result = $q.defer();
      result.reject();
      return result.promise;
    }

  }).inject('$q', '$location');
});


define('app/controllers/ReaderController',
    ['app/core/BaseController', 'app/factories/BookFactory', 'app/modules/api/ApiService'],
    function(BaseController, BookFactory, ApiService) {

      var $bookFactory,
          $apiService;

      return BaseController.extend('ReaderController', {

        bookId: '',

        bookData: '',

        loadStatus: '',

        constructor: function(_$scope, _$route, _$bookFactory, _$apiService) {
          var self = this;
          $bookFactory = _$bookFactory;
          $apiService = _$apiService;
          self.bookData = {'book': {}};
          self.loadStatus = {'loading': false, 'progress': 0};
          self.bookId = _$route.current.params['id'];
          self._super([_$scope]);

          function bookLoadProgress(arg) {
            this.loadStatus['progress'] = Math.floor(arg['loaded'] * 100 / arg['total']);
          }

          $bookFactory.bookFromStored(_$route.current.params['id']).then(
              function(book) {
                self.bookData['book'] = book;
                if (!book['epubBlob']) {
                 self.loadStatus['loading'] = true;
                  $apiService.getEpub(self.bookId, bookLoadProgress.bind(self)).then(
                      function(blob) {
                        book['epubBlob'] = blob;
                        $bookFactory.storeEpub(self.bookId, blob);
                        self.loadStatus['loading'] = false;
                      },
                      function(error) {
                        self.loadStatus['loading'] = false;
                        console.log('error: ', error);
                      }
                  );
                }
              },
              function(error) {}
          );
        },

        defineScope: function(_$scope) {
          _$scope['bookData'] = this.bookData;
          _$scope['loadStatus'] = this.loadStatus;
        }
      }).inject('$route', BookFactory.fullName, ApiService.fullName);
    });


define('i18n/translations', [],function() {
  return {
    'sw': {
      'Enter email' : 'Ange email',
      'Password': 'Lösenord',
      'Forgot your password?': 'Har du glömt lösenordet?',
      'Sign in': 'Logga in',
      'Sign up': 'Registrera',
      'Create an account': 'Skapa ett konto',
      'Continue': 'Fortsätt',
      'Register at Booqlas webapp and take advantage of all the books that are available. Your active books are available free online.':
          'Registrera dig på Booqlas webapp och ta del av alla de böcker som finns tillgängliga. Dina aktiva böcker finns tillgängliga utan internet.',
      'Repeat password': 'Uprepa lösenord',
      'Your country': 'Ditt land',
      'Reset password': 'Återställ lösenord',
      'Your books': 'Dina böcker'
    }
  };
});


define('app/directives/compareTo', [],function() {
  return function() {
    return {
      'require': 'ngModel',
      'scope': {
        'otherModelValue': '=compareTo'
      },
      link: function(scope, element, attributes, ngModel) {
        ngModel['$validators']['compareTo'] = function(modelValue) {
          return modelValue == scope['otherModelValue'];
        };

        scope.$watch('otherModelValue', function() {
          ngModel.$validate();
        });
      }
    };
  };
});

define('epub/Utils', [],function() {
  var MAX_ARRAY_INDEX = Math.pow(2, 53) - 1,
      URL = window['URL'] || window['webkitURL'] || window['mozURL'],
      currentUUID = 0;

  /* */
  function _folder(url) {
    var lastSlash = url.lastIndexOf('/');
    return (lastSlash == -1) ? '' : url.slice(0, lastSlash + 1);
  }

  /* */
  function _uri(url) {
    var uri = {
                protocol: '',
                host: '',
                path: '',
                origin: '',
                directory: '',
                base: '',
                filename: '',
                extension: '',
                fragment: '',
                href: url
              },
        blob = url.indexOf('blob:'),
        doubleSlash = url.indexOf('://'),
        search = url.indexOf('?'),
        fragment = url.indexOf('#'),
        withoutProtocol,
        dot,
        firstSlash,
        href;

    if (blob === 0) {
      uri.protocol = 'blob';
      uri.base = url.indexOf(0, fragment);
      return uri;
    }

    if (fragment != -1) {
      uri.fragment = url.slice(fragment + 1);
      url = url.slice(0, fragment);
    }

    if (search != -1) {
      uri.search = url.slice(search + 1);
      url = url.slice(0, search);
      href = url;
    }

    if (doubleSlash != -1) {
      uri.protocol = url.slice(0, doubleSlash);
      withoutProtocol = url.slice(doubleSlash + 3);
      firstSlash = withoutProtocol.indexOf('/');

      if (firstSlash === -1) {
        uri.host = uri.path;
        uri.path = '';
      } else {
        uri.host = withoutProtocol.slice(0, firstSlash);
        uri.path = withoutProtocol.slice(firstSlash);
      }
      uri.origin = uri.protocol + '://' + uri.host;
      uri.directory = _folder(uri.path);
      uri.base = uri.origin + uri.directory;
      // return origin;
    } else {
      uri.path = url;
      uri.directory = _folder(url);
      uri.base = uri.directory;
    }
    //-- Filename
    uri.filename = url.replace(uri.base, '');
    dot = uri.filename.lastIndexOf('.');
    if (dot != -1) {
      uri.extension = uri.filename.slice(dot + 1);
    }
    return uri;
  }

  /* */
  function _resolveURL(base, path) {
    var url,
        segments = [],
        uri = _uri(path),
        folders = base.split('/'),
        paths;

    if (uri.host) {
      return path;
    }

    folders.pop();

    paths = path.split('/');
    paths.forEach(function(p) {
      if (p === '..') {
        folders.pop();
      } else {
        segments.push(p);
      }
    });

    url = folders.concat(segments);

    return url.join('/');
  }

  /* */
  function _indexOfTextNode(textNode) {
    var parent = textNode.parentNode,
        children = parent.childNodes,
        sib,
        index = -1,
        i = 0;

    for (; i < children.length; i++) {
      sib = children[i];
      if (sib.nodeType === Node['TEXT_NODE']) {
        index++;
      }
      if (sib == textNode) break;
    }

    return index;
  }

  /* */
  function _getElementTreeXPath(element) {
    var paths = [],
        isXhtml = (element.ownerDocument.documentElement.getAttribute('xmlns') === 'http://www.w3.org/1999/xhtml'),
        index,
        nodeName,
        tagName,
        pathIndex;

    if (element.nodeType === Node['TEXT_NODE']) {
      // index = Array.prototype.indexOf.call(element.parentNode.childNodes, element) + 1;
      index = _indexOfTextNode(element) + 1;

      paths.push('text()[' + index + ']');
      element = element.parentNode;
    }

    // Use nodeName (instead of localName) so namespace prefix is included (if any).
    for (; element && element.nodeType == 1; element = element.parentNode)
    {
      index = 0;
      for (var sibling = element.previousSibling; sibling; sibling = sibling.previousSibling)
      {
        // Ignore document type declaration.
        if (sibling.nodeType == Node.DOCUMENT_TYPE_NODE) {
          continue;
        }
        if (sibling.nodeName == element.nodeName) {
          ++index;
        }
      }
      nodeName = element.nodeName.toLowerCase();
      tagName = (isXhtml ? 'xhtml:' + nodeName : nodeName);
      pathIndex = (index ? '[' + (index + 1) + ']' : '');
      paths.splice(0, 0, tagName + pathIndex);
    }

    return paths.length ? './' + paths.join('/') : null;
  }

  /* */
  function _nsResolver(prefix) {
    var ns = {
      'xhtml' : 'http://www.w3.org/1999/xhtml',
      'epub': 'http://www.idpf.org/2007/ops'
    };
    return ns[prefix] || null;
  }

  function _cleanStringForXpath(str)  {
    var parts = str.match(/[^'"]+|['"]/g);
    parts = parts.map(function(part) {
      if (part === "'") {
        return '\"\'\"'; // output "'"
      }

      if (part === '"') {
        return "\'\"\'"; // output '"'
      }
      return "\'" + part + "\'";
    });
    return "concat(\'\'," + parts.join(',') + ')';
  }

  function _queue(_scope) {
    var _q = [],
        scope = _scope;
    // Add an item to the queue
    function enqueue(func, args, context) {
      _q.push({
        func: func,
        args: args,
        context: context
      });
      return _q;
    }

    // Run one item
    function dequeue() {
      var t;
      if (_q.length) {
        t = _q.shift();
        // Defer to any current tasks
        // setTimeout(function(){
        t.func.apply(t.context || scope, t.args);
        // }, 0);
      }
    }

    // Run All
    function flush() {
      while (_q.length) {
        dequeue();
      }
    }

    // Clear all items in wait
    function clear() {
      _q = [];
    }

    function length() {
      return _q.length;
    }

    return {
      enqueue: enqueue,
      dequeue: dequeue,
      flush: flush,
      clear: clear,
      length: length
    };
  }

  //
  function _isArrayLike(collection) {
    var length = collection && collection['length'];
    return typeof length == 'number' && length >= 0 && length <= MAX_ARRAY_INDEX;
  }

  //function each(obj, iteratee, context) {

    /**
     * @param {function(...*)} func
     * @param {*} context
     * @param {*=} opt_argCount
     * @return {function(...?):?}
     */
    /*function optimize(func, context, opt_argCount) {
      if (context === void 0) return func;
      switch (opt_argCount == null ? 3 : opt_argCount) {
        case 1: return function(value) {
          return func.call(context, value);
        };
        case 2: return function(value, other) {
          return func.call(context, value, other);
        };
        case 3: return function(value, index, collection) {
          return func.call(context, value, index, collection);
        };
        case 4: return function(accumulator, value, index, collection) {
          return func.call(context, accumulator, value, index, collection);
        };
      }
      return function() {
        return func.apply(context, arguments);
      };
    }

    iteratee = optimize(iteratee, context);

    var i, length, key;
    if (_isArrayLike(obj)) {
      for (i = 0, length = obj.length; i < length; i++) {
        iteratee(obj[i], i, obj);
      }
    } else {
      for (key in obj) {
        if (obj.hasOwnProperty(key)) iteratee(obj[key], key, obj);
      }
    }
    return obj;
  }*/

  function _compareFunc(a, b) {
    if (a > b) return 1;
    if (a < b) return -1;
    if (a = b) return 0;
  }

  function _indexOfSorted(item, array, compareFunction, _start, _end) {
    var start = _start || 0,
        end = _end || array.length,
        pivot = parseInt(start + (end - start) / 2, 10),
        compareFunc = compareFunction || _compareFunc,
        compared;

    if (end - start <= 0) {
      return -1; // Not found
    }

    compared = compareFunc(array[pivot], item);
    if (end - start === 1) {
      return compared === 0 ? pivot : -1;
    }
    if (compared === 0) {
      return pivot; // Found
    }
    if (compared === -1) {
      return _indexOfSorted(item, array, compareFunc, pivot, end);
    } else {
      return _indexOfSorted(item, array, compareFunc, start, pivot);
    }
  }

  function _locationOf(item, array, compareFunction, _start, _end) {
    var start = _start || 0,
        end = _end || array.length,
        pivot = parseInt(start + (end - start) / 2, 10),
        compareFunc = compareFunction || _compareFunc,
        compared;

    if (end - start <= 0) {
      return pivot;
    }

    compared = compareFunc(array[pivot], item);
    if (end - start === 1) {
      return compared > 0 ? pivot : pivot + 1;
    }

    if (compared === 0) {
      return pivot;
    }

    if (compared === -1) {
      return _locationOf(item, array, compareFunc, pivot, end);
    } else {
      return _locationOf(item, array, compareFunc, start, pivot);
    }
  }

  function _prefixed(unprefixed) {
    var vendors = ['Webkit', 'Moz', 'O', 'ms'],
        prefixes = ['-Webkit-', '-moz-', '-o-', '-ms-'],
        upper = unprefixed[0].toUpperCase() + unprefixed.slice(1),
        length = vendors.length,
        i = 0;
    if (typeof(document.body.style[unprefixed]) != 'undefined') {
      return unprefixed;
    }
    for (; i < length; i++) {
      if (typeof(document.body.style[vendors[i] + upper]) != 'undefined') {
        return vendors[i] + upper;
      }
    }
    return unprefixed;
  }

  function _getElementXPath(element) {
    if (element && element['id']) {
      return '//*[@id="' + element['id'] + '"]';
    } else {
      return _getElementTreeXPath(element);
    }
  }

  function _createObjectUrl(obj) {
    return URL['createObjectURL'](obj);
  }

  function _revokeObjectUrl(url) {
    return URL['revokeObjectURL'](url);
  }

  function _uuid(obj) {
    currentUUID++;
    if (obj) obj.uuid = currentUUID;
    return currentUUID;
  }

  return {
    prefixed: _prefixed,
    getElementXPath: _getElementXPath,
    getElementTreeXPath: _getElementTreeXPath,
    indexOfTextNode: _indexOfTextNode,
    nsResolver: _nsResolver,
    cleanStringForXpath: _cleanStringForXpath,
    queue: _queue,
    resolveURL: _resolveURL,
    uri: _uri,
    folder: _folder,
    isArrayLike: _isArrayLike,
    //each: each,
    indexOfSorted: _indexOfSorted,
    locationOf: _locationOf,
    createObjectUrl: _createObjectUrl,
    revokeObjectUrl: _revokeObjectUrl,
    uuid: _uuid
  };

});


define('epub/Parser', ['epub/Utils'], function(U) {
  'use strict';
  function getListItems(parent) {
    var items = [];
    [].slice.call(parent.childNodes).forEach(function(node) {
      if ('ol' == node.tagName) {
        [].slice.call(node.childNodes).forEach(function(item) {
          if ('li' == item.tagName) {
            items.push(item);
          }
        });
      }
    });
    return items;
  }

  function getAnchorOrSpan(parent) {
    var item = null;

    [].slice.call(parent.childNodes).forEach(function(node) {
      if ('a' == node.tagName || 'span' == node.tagName) {
        item = node;
      }
    });

    return item;
  }

  function getContainer(containerXml) {
    if (!containerXml) {
      console.error('Container File Not Found');
      return;
    }
    var rootFile = containerXml.querySelector('rootfile'),
        fullPath, folder, encoding;

    if (!rootFile) {
      console.error('No RootFile Found');
      return;
    }

    fullPath = rootFile.getAttribute('full-path');
    folder = U.uri(fullPath).directory;
    encoding = containerXml.xmlEncoding;

    //-- Now that we have the path we can parse the contents
    return {
      packagePath: fullPath,
      basePath: folder,
      encoding: encoding
    };
  }

  function getIdentifier(packageXml) {
    if (!packageXml) {
      console.error('Package File Not Found');
      return;
    }
    var metadataNode = packageXml.querySelector('metadata');

    if (!metadataNode) {
      console.error('No Metadata Found');
      return;
    }
    return getElementText(metadataNode, 'identifier');
  }

  function getElementText(xml, tag) {
    var found = xml.getElementsByTagNameNS('http://purl.org/dc/elements/1.1/', tag),
        el;

    if (!found || found.length === 0) return '';
    el = found[0];

    return (el.childNodes.length) ? el.childNodes[0].nodeValue : '';
  }

  function querySelectorText(xml, q) {
    var el = xml.querySelector(q);
    return (el && el.childNodes.length) ? el.childNodes[0].nodeValue : '';
  }

  //-- Find TOC NAV
  function getNavPath(manifestNode) {
    var node = manifestNode.querySelector('item[properties$="nav"], ' + 'item[properties^="nav "], ' + 'item[properties*=" nav "]');
    return node ? node.getAttribute('href') : false;
  }

  function getCoverPath(manifestNode) {
    var node = manifestNode.querySelector('item[properties="cover-image"]');
    return node ? node.getAttribute('href') : false;
  }

  function getSpine(spineXml, manifest) {
    var spine = [],
        nonLinear = [],
        selected = spineXml.getElementsByTagName('itemref'),
        items = [].slice.call(selected);

    items.forEach(function(item, index) {
      var id = item.getAttribute('idref'),
          props = item.getAttribute('properties') || '',
          propArray = props.length ? props.split(' ') : [],
          manifestProps = manifest[id].properties,
          manifestPropArray = manifestProps.length ? manifestProps.split(' ') : [];

      spine.push({
        id: id,
        linear: item.getAttribute('linear') || '',
        properties: propArray,
        manifestProperties: manifestPropArray,
        href: manifest[id].href,
        url: manifest[id].url,
        index: index
      });
    });
    return spine;
  }

  function getPackageContents(packageXml, basePath) {
    if (!packageXml) {
      console.error('Package File Not Found');
      return;
    }
    var metadataNode = packageXml.querySelector('metadata'),
        manifestNode = packageXml.querySelector('manifest'),
        spineNode = packageXml.querySelector('spine'),
        manifest, navPath, tocPath, coverPath,
        spineNodeIndex, spine, metadata,
        spineIndexByURL = {};

    if (!metadataNode) {
      console.error('No Metadata Found');
      return;
    }

    if (!manifestNode) {
      console.error('No Manifest Found');
      return;
    }

    if (!spineNode) {
      console.error('No Spine Found');
      return;
    }

    manifest = getManifest(manifestNode, basePath);
    navPath = getNavPath(manifestNode);
    tocPath = getTocPath(manifestNode, spineNode);
    coverPath = getCoverPath(manifestNode);

    spineNodeIndex = Array.prototype.indexOf.call(spineNode.parentNode.childNodes, spineNode);

    spine = getSpine(spineNode, manifest);

    spine.forEach(function(item) {
      spineIndexByURL[item.href] = item.index;
    });

    metadata = getMetadata(metadataNode);

    metadata.direction = spineNode.getAttribute('page-progression-direction');

    return {
      metadata: metadata,
      spine: spine,
      manifest: manifest,
      navPath: navPath,
      tocPath: tocPath,
      coverPath: coverPath,
      spineNodeIndex: spineNodeIndex,
      spineIndexByURL: spineIndexByURL
    };
  }

  //-- Get TOC NCX
  function getTocPath(manifestNode, spineNode) {
    var node = manifestNode.querySelector('item[media-type="application/x-dtbncx+xml"]'),
        tocId;

    // If we can't find the toc by media-type then try to look
    // for id of the item in the spine attributes as according to
    // http://www.idpf.org/epub/20/spec/OPF_2.0.1_draft.htm#Section2.4.1.2,
    // 'The item that describes the NCX must be referenced by the spine
    // toc attribute.'
    if (!node) {
      tocId = spineNode.getAttribute('toc');
      if (tocId) {
        node = manifestNode.querySelector('item[id="' + tocId + ']');
      }
    }

    return node ? node.getAttribute('href') : false;
  }

  function getToc(tocXml, spineIndexByURL, bookSpine) {
    var navMap = tocXml.querySelector('navMap');
    if (!navMap) return [];

    function _getTOC(parent) {
      var list = [],
          snapshot = tocXml.evaluate('*[local-name()="navPoint"]', parent, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null),
          length = snapshot.snapshotLength;

      if (length === 0) return [];

      for (var i = length - 1; i >= 0; i--) {
        var item = snapshot.snapshotItem(i);

        var id = item.getAttribute('id') || false,
            content = item.querySelector('content'),
            src = content.getAttribute('src'),
            navLabel = item.querySelector('navLabel'),
            text = navLabel.textContent ? navLabel.textContent : '',
            split = src.split('#'),
            baseUrl = split[0],
            spinePos = spineIndexByURL[baseUrl],
            spineItem = bookSpine[spinePos],
            subitems = _getTOC(item),
            idCounter = 0,
            cfi = spineItem ? spineItem.cfi : '';

        if (!id) {
          if (spinePos) {
            spineItem = bookSpine[spinePos];
            id = spineItem.id;
            cfi = spineItem.cfi;
          } else {
            id = 'epubjs-autogen-toc-id-' + (idCounter++);
          }
        }

        list.unshift({
          id: id,
          href: src,
          label: text,
          spinePos: spinePos,
          subitems: subitems,
          parent: parent ? parent.getAttribute('id') : null,
          cfi: cfi
        });

      }

      return list;
    }

    return _getTOC(navMap);
  }

  //-- Expanded to match Readium web components
  function getMetadata(xml) {
    return {
      direction: '',
      bookTitle: getElementText(xml, 'title'),
      creator: getElementText(xml, 'creator'),
      description: getElementText(xml, 'description'),
      pubdate: getElementText(xml, 'date'),
      publisher: getElementText(xml, 'publisher'),
      identifier: getElementText(xml, 'identifier'),
      language: getElementText(xml, 'language'),
      rights: getElementText(xml, 'rights'),
      modified_date: querySelectorText(xml, 'meta[property="dcterms:modified"]'),
      layout: querySelectorText(xml, 'meta[property="rendition:layout"]'),
      orientation: querySelectorText(xml, 'meta[property="rendition:orientation"]'),
      spread: querySelectorText(xml, 'meta[property="rendition:spread"]')
    };
  }

  function getManifest(manifestXml, baseUrl) {
    var manifest = {},
        selected = manifestXml.querySelectorAll('item'),
        items = [].slice.call(selected);

    //-- Create an object with the id as key
    items.forEach(function(item) {
      var id = item.getAttribute('id'),
          href = item.getAttribute('href') || '',
          type = item.getAttribute('media-type') || '',
          properties = item.getAttribute('properties') || '';

      manifest[id] = {
        href: href,
        //-- Absolute URL for loading with a web worker
        url: baseUrl + href,
        type: type,
        properties: properties
      };

    });

    return manifest;
  }

  function getNav(navHtml, spineIndexByURL, bookSpine) {
    var navEl = navHtml.querySelector('nav[*|type="toc"]'),
        idCounter = 0;

    if (!navEl) return [];

    function _getNAV(parent) {
      var list = [],
          nodes = getListItems(parent),
          items = [].slice.call(nodes),
          length = items.length,
          node;

      if (length === 0) return false;

      items.forEach(function(item) {
        var id = item.getAttribute('id') || false,
            content = getAnchorOrSpan(item),
            href = content.getAttribute('href') || '',
            text = content.textContent || '',
            split = href.split('#'),
            baseUrl = split[0],
            subitems = _getNAV(item),
            spinePos = spineIndexByURL[baseUrl],
            spineItem = bookSpine[spinePos],
            cfi = spineItem ? spineItem.cfi : '';

        if (!id) {
          if (spinePos) {
            spineItem = bookSpine[spinePos];
            id = spineItem.id;
            cfi = spineItem.cfi;
          } else {
            id = 'epubjs-autogen-toc-id-' + (idCounter++);
          }
        }

        item.setAttribute('id', id); // Ensure all elements have an id
        list.push({
          id: id,
          href: href,
          label: text,
          subitems: subitems,
          parent: parent ? parent.getAttribute('id') : null,
          cfi: cfi
        });

      });

      return list;
    }
    return _getNAV(navEl);
  }

  return {
    getContainer: getContainer,
    getPackageContents: getPackageContents,
    getNav: getNav,
    getToc: getToc
  };
});


define('epub/Mime', [],function() {
  var table = {
    'application' : {
      'andrew-inset' : 'ez',
      'annodex' : 'anx',
      'atom+xml' : 'atom',
      'atomcat+xml' : 'atomcat',
      'atomserv+xml' : 'atomsrv',
      'bbolin' : 'lin',
      'cap' : ['cap', 'pcap'],
      'cu-seeme' : 'cu',
      'davmount+xml' : 'davmount',
      'dsptype' : 'tsp',
      'ecmascript' : ['es', 'ecma'],
      'futuresplash' : 'spl',
      'hta' : 'hta',
      'java-archive' : 'jar',
      'java-serialized-object' : 'ser',
      'java-vm' : 'class',
      'javascript' : 'js',
      'm3g' : 'm3g',
      'mac-binhex40' : 'hqx',
      'mathematica' : ['nb', 'ma', 'mb'],
      'msaccess' : 'mdb',
      'msword' : ['doc', 'dot'],
      'mxf' : 'mxf',
      'oda' : 'oda',
      'ogg' : 'ogx',
      'pdf' : 'pdf',
      'pgp-keys' : 'key',
      'pgp-signature' : ['asc', 'sig'],
      'pics-rules' : 'prf',
      'postscript' : ['ps', 'ai', 'eps', 'epsi', 'epsf', 'eps2', 'eps3'],
      'rar' : 'rar',
      'rdf+xml' : 'rdf',
      'rss+xml' : 'rss',
      'rtf' : 'rtf',
      'smil' : ['smi', 'smil'],
      'xhtml+xml' : ['xhtml', 'xht'],
      'xml' : ['xml', 'xsl', 'xsd'],
      'xspf+xml' : 'xspf',
      'zip' : 'zip',
      'vnd.android.package-archive' : 'apk',
      'vnd.cinderella' : 'cdy',
      'vnd.google-earth.kml+xml' : 'kml',
      'vnd.google-earth.kmz' : 'kmz',
      'vnd.mozilla.xul+xml' : 'xul',
      'vnd.ms-excel' : ['xls', 'xlb', 'xlt', 'xlm', 'xla', 'xlc', 'xlw'],
      'vnd.ms-pki.seccat' : 'cat',
      'vnd.ms-pki.stl' : 'stl',
      'vnd.ms-powerpoint' : ['ppt', 'pps', 'pot'],
      'vnd.oasis.opendocument.chart' : 'odc',
      'vnd.oasis.opendocument.database' : 'odb',
      'vnd.oasis.opendocument.formula' : 'odf',
      'vnd.oasis.opendocument.graphics' : 'odg',
      'vnd.oasis.opendocument.graphics-template' : 'otg',
      'vnd.oasis.opendocument.image' : 'odi',
      'vnd.oasis.opendocument.presentation' : 'odp',
      'vnd.oasis.opendocument.presentation-template' : 'otp',
      'vnd.oasis.opendocument.spreadsheet' : 'ods',
      'vnd.oasis.opendocument.spreadsheet-template' : 'ots',
      'vnd.oasis.opendocument.text' : 'odt',
      'vnd.oasis.opendocument.text-master' : 'odm',
      'vnd.oasis.opendocument.text-template' : 'ott',
      'vnd.oasis.opendocument.text-web' : 'oth',
      'vnd.openxmlformats-officedocument.spreadsheetml.sheet' : 'xlsx',
      'vnd.openxmlformats-officedocument.spreadsheetml.template' : 'xltx',
      'vnd.openxmlformats-officedocument.presentationml.presentation' : 'pptx',
      'vnd.openxmlformats-officedocument.presentationml.slideshow' : 'ppsx',
      'vnd.openxmlformats-officedocument.presentationml.template' : 'potx',
      'vnd.openxmlformats-officedocument.wordprocessingml.document' : 'docx',
      'vnd.openxmlformats-officedocument.wordprocessingml.template' : 'dotx',
      'vnd.smaf' : 'mmf',
      'vnd.stardivision.calc' : 'sdc',
      'vnd.stardivision.chart' : 'sds',
      'vnd.stardivision.draw' : 'sda',
      'vnd.stardivision.impress' : 'sdd',
      'vnd.stardivision.math' : ['sdf', 'smf'],
      'vnd.stardivision.writer' : ['sdw', 'vor'],
      'vnd.stardivision.writer-global' : 'sgl',
      'vnd.sun.xml.calc' : 'sxc',
      'vnd.sun.xml.calc.template' : 'stc',
      'vnd.sun.xml.draw' : 'sxd',
      'vnd.sun.xml.draw.template' : 'std',
      'vnd.sun.xml.impress' : 'sxi',
      'vnd.sun.xml.impress.template' : 'sti',
      'vnd.sun.xml.math' : 'sxm',
      'vnd.sun.xml.writer' : 'sxw',
      'vnd.sun.xml.writer.global' : 'sxg',
      'vnd.sun.xml.writer.template' : 'stw',
      'vnd.symbian.install' : ['sis', 'sisx'],
      'vnd.visio' : ['vsd', 'vst', 'vss', 'vsw'],
      'vnd.wap.wbxml' : 'wbxml',
      'vnd.wap.wmlc' : 'wmlc',
      'vnd.wap.wmlscriptc' : 'wmlsc',
      'vnd.wordperfect' : 'wpd',
      'vnd.wordperfect5.1' : 'wp5',
      'x-123' : 'wk',
      'x-7z-compressed' : '7z',
      'x-abiword' : 'abw',
      'x-apple-diskimage' : 'dmg',
      'x-bcpio' : 'bcpio',
      'x-bittorrent' : 'torrent',
      'x-cbr' : ['cbr', 'cba', 'cbt', 'cb7'],
      'x-cbz' : 'cbz',
      'x-cdf' : ['cdf', 'cda'],
      'x-cdlink' : 'vcd',
      'x-chess-pgn' : 'pgn',
      'x-cpio' : 'cpio',
      'x-csh' : 'csh',
      'x-debian-package' : ['deb', 'udeb'],
      'x-director' : ['dcr', 'dir', 'dxr', 'cst', 'cct', 'cxt', 'w3d', 'fgd', 'swa'],
      'x-dms' : 'dms',
      'x-doom' : 'wad',
      'x-dvi' : 'dvi',
      'x-httpd-eruby' : 'rhtml',
      'x-font' : 'pcf.Z',
      'x-freemind' : 'mm',
      'x-gnumeric' : 'gnumeric',
      'x-go-sgf' : 'sgf',
      'x-graphing-calculator' : 'gcf',
      'x-gtar' : ['gtar', 'taz'],
      'x-hdf' : 'hdf',
      'x-httpd-php' : ['phtml', 'pht', 'php'],
      'x-httpd-php-source' : 'phps',
      'x-httpd-php3' : 'php3',
      'x-httpd-php3-preprocessed' : 'php3p',
      'x-httpd-php4' : 'php4',
      'x-httpd-php5' : 'php5',
      'x-ica' : 'ica',
      'x-info' : 'info',
      'x-internet-signup' : ['ins', 'isp'],
      'x-iphone' : 'iii',
      'x-iso9660-image' : 'iso',
      'x-java-jnlp-file' : 'jnlp',
      'x-jmol' : 'jmz',
      'x-killustrator' : 'kil',
      'x-koan' : ['skp', 'skd', 'skt', 'skm'],
      'x-kpresenter' : ['kpr', 'kpt'],
      'x-kword' : ['kwd', 'kwt'],
      'x-latex' : 'latex',
      'x-lha' : 'lha',
      'x-lyx' : 'lyx',
      'x-lzh' : 'lzh',
      'x-lzx' : 'lzx',
      'x-maker' : ['frm', 'maker', 'frame', 'fm', 'fb', 'book', 'fbdoc'],
      'x-ms-wmd' : 'wmd',
      'x-ms-wmz' : 'wmz',
      'x-msdos-program' : ['com', 'exe', 'bat', 'dll'],
      'x-msi' : 'msi',
      'x-netcdf' : ['nc', 'cdf'],
      'x-ns-proxy-autoconfig' : ['pac', 'dat'],
      'x-nwc' : 'nwc',
      'x-object' : 'o',
      'x-oz-application' : 'oza',
      'x-pkcs7-certreqresp' : 'p7r',
      'x-python-code' : ['pyc', 'pyo'],
      'x-qgis' : ['qgs', 'shp', 'shx'],
      'x-quicktimeplayer' : 'qtl',
      'x-redhat-package-manager' : 'rpm',
      'x-ruby' : 'rb',
      'x-sh' : 'sh',
      'x-shar' : 'shar',
      'x-shockwave-flash' : ['swf', 'swfl'],
      'x-silverlight' : 'scr',
      'x-stuffit' : 'sit',
      'x-sv4cpio' : 'sv4cpio',
      'x-sv4crc' : 'sv4crc',
      'x-tar' : 'tar',
      'x-tcl' : 'tcl',
      'x-tex-gf' : 'gf',
      'x-tex-pk' : 'pk',
      'x-texinfo' : ['texinfo', 'texi'],
      'x-trash' : ['~', '%', 'bak', 'old', 'sik'],
      'x-troff' : ['t', 'tr', 'roff'],
      'x-troff-man' : 'man',
      'x-troff-me' : 'me',
      'x-troff-ms' : 'ms',
      'x-ustar' : 'ustar',
      'x-wais-source' : 'src',
      'x-wingz' : 'wz',
      'x-x509-ca-cert' : ['crt', 'der', 'cer'],
      'x-xcf' : 'xcf',
      'x-xfig' : 'fig',
      'x-xpinstall' : 'xpi',
      'applixware' : 'aw',
      'atomsvc+xml' : 'atomsvc',
      'ccxml+xml' : 'ccxml',
      'cdmi-capability' : 'cdmia',
      'cdmi-container' : 'cdmic',
      'cdmi-domain' : 'cdmid',
      'cdmi-object' : 'cdmio',
      'cdmi-queue' : 'cdmiq',
      'docbook+xml' : 'dbk',
      'dssc+der' : 'dssc',
      'dssc+xml' : 'xdssc',
      'emma+xml' : 'emma',
      'epub+zip' : 'epub',
      'exi' : 'exi',
      'font-tdpfr' : 'pfr',
      'gml+xml' : 'gml',
      'gpx+xml' : 'gpx',
      'gxf' : 'gxf',
      'hyperstudio' : 'stk',
      'inkml+xml' : ['ink', 'inkml'],
      'ipfix' : 'ipfix',
      'json' : 'json',
      'jsonml+json' : 'jsonml',
      'lost+xml' : 'lostxml',
      'mads+xml' : 'mads',
      'marc' : 'mrc',
      'marcxml+xml' : 'mrcx',
      'mathml+xml' : 'mathml',
      'mbox' : 'mbox',
      'mediaservercontrol+xml' : 'mscml',
      'metalink+xml' : 'metalink',
      'metalink4+xml' : 'meta4',
      'mets+xml' : 'mets',
      'mods+xml' : 'mods',
      'mp21' : ['m21', 'mp21'],
      'mp4' : 'mp4s',
      'oebps-package+xml' : 'opf',
      'omdoc+xml' : 'omdoc',
      'onenote' : ['onetoc', 'onetoc2', 'onetmp', 'onepkg'],
      'oxps' : 'oxps',
      'patch-ops-error+xml' : 'xer',
      'pgp-encrypted' : 'pgp',
      'pkcs10' : 'p10',
      'pkcs7-mime' : ['p7m', 'p7c'],
      'pkcs7-signature' : 'p7s',
      'pkcs8' : 'p8',
      'pkix-attr-cert' : 'ac',
      'pkix-crl' : 'crl',
      'pkix-pkipath' : 'pkipath',
      'pkixcmp' : 'pki',
      'pls+xml' : 'pls',
      'prs.cww' : 'cww',
      'pskc+xml' : 'pskcxml',
      'reginfo+xml' : 'rif',
      'relax-ng-compact-syntax' : 'rnc',
      'resource-lists+xml' : 'rl',
      'resource-lists-diff+xml' : 'rld',
      'rls-services+xml' : 'rs',
      'rpki-ghostbusters' : 'gbr',
      'rpki-manifest' : 'mft',
      'rpki-roa' : 'roa',
      'rsd+xml' : 'rsd',
      'sbml+xml' : 'sbml',
      'scvp-cv-request' : 'scq',
      'scvp-cv-response' : 'scs',
      'scvp-vp-request' : 'spq',
      'scvp-vp-response' : 'spp',
      'sdp' : 'sdp',
      'set-payment-initiation' : 'setpay',
      'set-registration-initiation' : 'setreg',
      'shf+xml' : 'shf',
      'sparql-query' : 'rq',
      'sparql-results+xml' : 'srx',
      'srgs' : 'gram',
      'srgs+xml' : 'grxml',
      'sru+xml' : 'sru',
      'ssdl+xml' : 'ssdl',
      'ssml+xml' : 'ssml',
      'tei+xml' : ['tei', 'teicorpus'],
      'thraud+xml' : 'tfi',
      'timestamped-data' : 'tsd',
      'vnd.3gpp.pic-bw-large' : 'plb',
      'vnd.3gpp.pic-bw-small' : 'psb',
      'vnd.3gpp.pic-bw-var' : 'pvb',
      'vnd.3gpp2.tcap' : 'tcap',
      'vnd.3m.post-it-notes' : 'pwn',
      'vnd.accpac.simply.aso' : 'aso',
      'vnd.accpac.simply.imp' : 'imp',
      'vnd.acucobol' : 'acu',
      'vnd.acucorp' : ['atc', 'acutc'],
      'vnd.adobe.air-application-installer-package+zip' : 'air',
      'vnd.adobe.formscentral.fcdt' : 'fcdt',
      'vnd.adobe.fxp' : ['fxp', 'fxpl'],
      'vnd.adobe.xdp+xml' : 'xdp',
      'vnd.adobe.xfdf' : 'xfdf',
      'vnd.ahead.space' : 'ahead',
      'vnd.airzip.filesecure.azf' : 'azf',
      'vnd.airzip.filesecure.azs' : 'azs',
      'vnd.amazon.ebook' : 'azw',
      'vnd.americandynamics.acc' : 'acc',
      'vnd.amiga.ami' : 'ami',
      'vnd.anser-web-certificate-issue-initiation' : 'cii',
      'vnd.anser-web-funds-transfer-initiation' : 'fti',
      'vnd.antix.game-component' : 'atx',
      'vnd.apple.installer+xml' : 'mpkg',
      'vnd.apple.mpegurl' : 'm3u8',
      'vnd.aristanetworks.swi' : 'swi',
      'vnd.astraea-software.iota' : 'iota',
      'vnd.audiograph' : 'aep',
      'vnd.blueice.multipass' : 'mpm',
      'vnd.bmi' : 'bmi',
      'vnd.businessobjects' : 'rep',
      'vnd.chemdraw+xml' : 'cdxml',
      'vnd.chipnuts.karaoke-mmd' : 'mmd',
      'vnd.claymore' : 'cla',
      'vnd.cloanto.rp9' : 'rp9',
      'vnd.clonk.c4group' : ['c4g', 'c4d', 'c4f', 'c4p', 'c4u'],
      'vnd.cluetrust.cartomobile-config' : 'c11amc',
      'vnd.cluetrust.cartomobile-config-pkg' : 'c11amz',
      'vnd.commonspace' : 'csp',
      'vnd.contact.cmsg' : 'cdbcmsg',
      'vnd.cosmocaller' : 'cmc',
      'vnd.crick.clicker' : 'clkx',
      'vnd.crick.clicker.keyboard' : 'clkk',
      'vnd.crick.clicker.palette' : 'clkp',
      'vnd.crick.clicker.template' : 'clkt',
      'vnd.crick.clicker.wordbank' : 'clkw',
      'vnd.criticaltools.wbs+xml' : 'wbs',
      'vnd.ctc-posml' : 'pml',
      'vnd.cups-ppd' : 'ppd',
      'vnd.curl.car' : 'car',
      'vnd.curl.pcurl' : 'pcurl',
      'vnd.dart' : 'dart',
      'vnd.data-vision.rdz' : 'rdz',
      'vnd.dece.data' : ['uvf', 'uvvf', 'uvd', 'uvvd'],
      'vnd.dece.ttml+xml' : ['uvt', 'uvvt'],
      'vnd.dece.unspecified' : ['uvx', 'uvvx'],
      'vnd.dece.zip' : ['uvz', 'uvvz'],
      'vnd.denovo.fcselayout-link' : 'fe_launch',
      'vnd.dna' : 'dna',
      'vnd.dolby.mlp' : 'mlp',
      'vnd.dpgraph' : 'dpg',
      'vnd.dreamfactory' : 'dfac',
      'vnd.ds-keypoint' : 'kpxx',
      'vnd.dvb.ait' : 'ait',
      'vnd.dvb.service' : 'svc',
      'vnd.dynageo' : 'geo',
      'vnd.ecowin.chart' : 'mag',
      'vnd.enliven' : 'nml',
      'vnd.epson.esf' : 'esf',
      'vnd.epson.msf' : 'msf',
      'vnd.epson.quickanime' : 'qam',
      'vnd.epson.salt' : 'slt',
      'vnd.epson.ssf' : 'ssf',
      'vnd.eszigno3+xml' : ['es3', 'et3'],
      'vnd.ezpix-album' : 'ez2',
      'vnd.ezpix-package' : 'ez3',
      'vnd.fdf' : 'fdf',
      'vnd.fdsn.mseed' : 'mseed',
      'vnd.fdsn.seed' : ['seed', 'dataless'],
      'vnd.flographit' : 'gph',
      'vnd.fluxtime.clip' : 'ftc',
      'vnd.framemaker' : ['fm', 'frame', 'maker', 'book'],
      'vnd.frogans.fnc' : 'fnc',
      'vnd.frogans.ltf' : 'ltf',
      'vnd.fsc.weblaunch' : 'fsc',
      'vnd.fujitsu.oasys' : 'oas',
      'vnd.fujitsu.oasys2' : 'oa2',
      'vnd.fujitsu.oasys3' : 'oa3',
      'vnd.fujitsu.oasysgp' : 'fg5',
      'vnd.fujitsu.oasysprs' : 'bh2',
      'vnd.fujixerox.ddd' : 'ddd',
      'vnd.fujixerox.docuworks' : 'xdw',
      'vnd.fujixerox.docuworks.binder' : 'xbd',
      'vnd.fuzzysheet' : 'fzs',
      'vnd.genomatix.tuxedo' : 'txd',
      'vnd.geogebra.file' : 'ggb',
      'vnd.geogebra.tool' : 'ggt',
      'vnd.geometry-explorer' : ['gex', 'gre'],
      'vnd.geonext' : 'gxt',
      'vnd.geoplan' : 'g2w',
      'vnd.geospace' : 'g3w',
      'vnd.gmx' : 'gmx',
      'vnd.grafeq' : ['gqf', 'gqs'],
      'vnd.groove-account' : 'gac',
      'vnd.groove-help' : 'ghf',
      'vnd.groove-identity-message' : 'gim',
      'vnd.groove-injector' : 'grv',
      'vnd.groove-tool-message' : 'gtm',
      'vnd.groove-tool-template' : 'tpl',
      'vnd.groove-vcard' : 'vcg',
      'vnd.hal+xml' : 'hal',
      'vnd.handheld-entertainment+xml' : 'zmm',
      'vnd.hbci' : 'hbci',
      'vnd.hhe.lesson-player' : 'les',
      'vnd.hp-hpgl' : 'hpgl',
      'vnd.hp-hpid' : 'hpid',
      'vnd.hp-hps' : 'hps',
      'vnd.hp-jlyt' : 'jlt',
      'vnd.hp-pcl' : 'pcl',
      'vnd.hp-pclxl' : 'pclxl',
      'vnd.hydrostatix.sof-data' : 'sfd-hdstx',
      'vnd.ibm.minipay' : 'mpy',
      'vnd.ibm.modcap' : ['afp', 'listafp', 'list3820'],
      'vnd.ibm.rights-management' : 'irm',
      'vnd.ibm.secure-container' : 'sc',
      'vnd.iccprofile' : ['icc', 'icm'],
      'vnd.igloader' : 'igl',
      'vnd.immervision-ivp' : 'ivp',
      'vnd.immervision-ivu' : 'ivu',
      'vnd.insors.igm' : 'igm',
      'vnd.intercon.formnet' : ['xpw', 'xpx'],
      'vnd.intergeo' : 'i2g',
      'vnd.intu.qbo' : 'qbo',
      'vnd.intu.qfx' : 'qfx',
      'vnd.ipunplugged.rcprofile' : 'rcprofile',
      'vnd.irepository.package+xml' : 'irp',
      'vnd.is-xpr' : 'xpr',
      'vnd.isac.fcs' : 'fcs',
      'vnd.jam' : 'jam',
      'vnd.jcp.javame.midlet-rms' : 'rms',
      'vnd.jisp' : 'jisp',
      'vnd.joost.joda-archive' : 'joda',
      'vnd.kahootz' : ['ktz', 'ktr'],
      'vnd.kde.karbon' : 'karbon',
      'vnd.kde.kchart' : 'chrt',
      'vnd.kde.kformula' : 'kfo',
      'vnd.kde.kivio' : 'flw',
      'vnd.kde.kontour' : 'kon',
      'vnd.kde.kpresenter' : ['kpr', 'kpt'],
      'vnd.kde.kspread' : 'ksp',
      'vnd.kde.kword' : ['kwd', 'kwt'],
      'vnd.kenameaapp' : 'htke',
      'vnd.kidspiration' : 'kia',
      'vnd.kinar' : ['kne', 'knp'],
      'vnd.koan' : ['skp', 'skd', 'skt', 'skm'],
      'vnd.kodak-descriptor' : 'sse',
      'vnd.las.las+xml' : 'lasxml',
      'vnd.llamagraphics.life-balance.desktop' : 'lbd',
      'vnd.llamagraphics.life-balance.exchange+xml' : 'lbe',
      'vnd.lotus-1-2-3' : '123',
      'vnd.lotus-approach' : 'apr',
      'vnd.lotus-freelance' : 'pre',
      'vnd.lotus-notes' : 'nsf',
      'vnd.lotus-organizer' : 'org',
      'vnd.lotus-screencam' : 'scm',
      'vnd.lotus-wordpro' : 'lwp',
      'vnd.macports.portpkg' : 'portpkg',
      'vnd.mcd' : 'mcd',
      'vnd.medcalcdata' : 'mc1',
      'vnd.mediastation.cdkey' : 'cdkey',
      'vnd.mfer' : 'mwf',
      'vnd.mfmp' : 'mfm',
      'vnd.micrografx.flo' : 'flo',
      'vnd.micrografx.igx' : 'igx',
      'vnd.mif' : 'mif',
      'vnd.mobius.daf' : 'daf',
      'vnd.mobius.dis' : 'dis',
      'vnd.mobius.mbk' : 'mbk',
      'vnd.mobius.mqy' : 'mqy',
      'vnd.mobius.msl' : 'msl',
      'vnd.mobius.plc' : 'plc',
      'vnd.mobius.txf' : 'txf',
      'vnd.mophun.application' : 'mpn',
      'vnd.mophun.certificate' : 'mpc',
      'vnd.ms-artgalry' : 'cil',
      'vnd.ms-cab-compressed' : 'cab',
      'vnd.ms-excel.addin.macroenabled.12' : 'xlam',
      'vnd.ms-excel.sheet.binary.macroenabled.12' : 'xlsb',
      'vnd.ms-excel.sheet.macroenabled.12' : 'xlsm',
      'vnd.ms-excel.template.macroenabled.12' : 'xltm',
      'vnd.ms-fontobject' : 'eot',
      'vnd.ms-htmlhelp' : 'chm',
      'vnd.ms-ims' : 'ims',
      'vnd.ms-lrm' : 'lrm',
      'vnd.ms-officetheme' : 'thmx',
      'vnd.ms-powerpoint.addin.macroenabled.12' : 'ppam',
      'vnd.ms-powerpoint.presentation.macroenabled.12' : 'pptm',
      'vnd.ms-powerpoint.slide.macroenabled.12' : 'sldm',
      'vnd.ms-powerpoint.slideshow.macroenabled.12' : 'ppsm',
      'vnd.ms-powerpoint.template.macroenabled.12' : 'potm',
      'vnd.ms-project' : ['mpp', 'mpt'],
      'vnd.ms-word.document.macroenabled.12' : 'docm',
      'vnd.ms-word.template.macroenabled.12' : 'dotm',
      'vnd.ms-works' : ['wps', 'wks', 'wcm', 'wdb'],
      'vnd.ms-wpl' : 'wpl',
      'vnd.ms-xpsdocument' : 'xps',
      'vnd.mseq' : 'mseq',
      'vnd.musician' : 'mus',
      'vnd.muvee.style' : 'msty',
      'vnd.mynfc' : 'taglet',
      'vnd.neurolanguage.nlu' : 'nlu',
      'vnd.nitf' : ['ntf', 'nitf'],
      'vnd.noblenet-directory' : 'nnd',
      'vnd.noblenet-sealer' : 'nns',
      'vnd.noblenet-web' : 'nnw',
      'vnd.nokia.n-gage.data' : 'ngdat',
      'vnd.nokia.n-gage.symbian.install' : 'n-gage',
      'vnd.nokia.radio-preset' : 'rpst',
      'vnd.nokia.radio-presets' : 'rpss',
      'vnd.novadigm.edm' : 'edm',
      'vnd.novadigm.edx' : 'edx',
      'vnd.novadigm.ext' : 'ext',
      'vnd.oasis.opendocument.chart-template' : 'otc',
      'vnd.oasis.opendocument.formula-template' : 'odft',
      'vnd.oasis.opendocument.image-template' : 'oti',
      'vnd.olpc-sugar' : 'xo',
      'vnd.oma.dd2+xml' : 'dd2',
      'vnd.openofficeorg.extension' : 'oxt',
      'vnd.openxmlformats-officedocument.presentationml.slide' : 'sldx',
      'vnd.osgeo.mapguide.package' : 'mgp',
      'vnd.osgi.dp' : 'dp',
      'vnd.osgi.subsystem' : 'esa',
      'vnd.palm' : ['pdb', 'pqa', 'oprc'],
      'vnd.pawaafile' : 'paw',
      'vnd.pg.format' : 'str',
      'vnd.pg.osasli' : 'ei6',
      'vnd.picsel' : 'efif',
      'vnd.pmi.widget' : 'wg',
      'vnd.pocketlearn' : 'plf',
      'vnd.powerbuilder6' : 'pbd',
      'vnd.previewsystems.box' : 'box',
      'vnd.proteus.magazine' : 'mgz',
      'vnd.publishare-delta-tree' : 'qps',
      'vnd.pvi.ptid1' : 'ptid',
      'vnd.quark.quarkxpress' : ['qxd', 'qxt', 'qwd', 'qwt', 'qxl', 'qxb'],
      'vnd.realvnc.bed' : 'bed',
      'vnd.recordare.musicxml' : 'mxl',
      'vnd.recordare.musicxml+xml' : 'musicxml',
      'vnd.rig.cryptonote' : 'cryptonote',
      'vnd.rn-realmedia' : 'rm',
      'vnd.rn-realmedia-vbr' : 'rmvb',
      'vnd.route66.link66+xml' : 'link66',
      'vnd.sailingtracker.track' : 'st',
      'vnd.seemail' : 'see',
      'vnd.sema' : 'sema',
      'vnd.semd' : 'semd',
      'vnd.semf' : 'semf',
      'vnd.shana.informed.formdata' : 'ifm',
      'vnd.shana.informed.formtemplate' : 'itp',
      'vnd.shana.informed.interchange' : 'iif',
      'vnd.shana.informed.package' : 'ipk',
      'vnd.simtech-mindmapper' : ['twd', 'twds'],
      'vnd.smart.teacher' : 'teacher',
      'vnd.solent.sdkm+xml' : ['sdkm', 'sdkd'],
      'vnd.spotfire.dxp' : 'dxp',
      'vnd.spotfire.sfs' : 'sfs',
      'vnd.stepmania.package' : 'smzip',
      'vnd.stepmania.stepchart' : 'sm',
      'vnd.sus-calendar' : ['sus', 'susp'],
      'vnd.svd' : 'svd',
      'vnd.syncml+xml' : 'xsm',
      'vnd.syncml.dm+wbxml' : 'bdm',
      'vnd.syncml.dm+xml' : 'xdm',
      'vnd.tao.intent-module-archive' : 'tao',
      'vnd.tcpdump.pcap' : ['pcap', 'cap', 'dmp'],
      'vnd.tmobile-livetv' : 'tmo',
      'vnd.trid.tpt' : 'tpt',
      'vnd.triscape.mxs' : 'mxs',
      'vnd.trueapp' : 'tra',
      'vnd.ufdl' : ['ufd', 'ufdl'],
      'vnd.uiq.theme' : 'utz',
      'vnd.umajin' : 'umj',
      'vnd.unity' : 'unityweb',
      'vnd.uoml+xml' : 'uoml',
      'vnd.vcx' : 'vcx',
      'vnd.visionary' : 'vis',
      'vnd.vsf' : 'vsf',
      'vnd.webturbo' : 'wtb',
      'vnd.wolfram.player' : 'nbp',
      'vnd.wqd' : 'wqd',
      'vnd.wt.stf' : 'stf',
      'vnd.xara' : 'xar',
      'vnd.xfdl' : 'xfdl',
      'vnd.yamaha.hv-dic' : 'hvd',
      'vnd.yamaha.hv-script' : 'hvs',
      'vnd.yamaha.hv-voice' : 'hvp',
      'vnd.yamaha.openscoreformat' : 'osf',
      'vnd.yamaha.openscoreformat.osfpvg+xml' : 'osfpvg',
      'vnd.yamaha.smaf-audio' : 'saf',
      'vnd.yamaha.smaf-phrase' : 'spf',
      'vnd.yellowriver-custom-menu' : 'cmp',
      'vnd.zul' : ['zir', 'zirz'],
      'vnd.zzazz.deck+xml' : 'zaz',
      'voicexml+xml' : 'vxml',
      'widget' : 'wgt',
      'winhlp' : 'hlp',
      'wsdl+xml' : 'wsdl',
      'wspolicy+xml' : 'wspolicy',
      'x-ace-compressed' : 'ace',
      'x-authorware-bin' : ['aab', 'x32', 'u32', 'vox'],
      'x-authorware-map' : 'aam',
      'x-authorware-seg' : 'aas',
      'x-blorb' : ['blb', 'blorb'],
      'x-bzip' : 'bz',
      'x-bzip2' : ['bz2', 'boz'],
      'x-cfs-compressed' : 'cfs',
      'x-chat' : 'chat',
      'x-conference' : 'nsc',
      'x-dgc-compressed' : 'dgc',
      'x-dtbncx+xml' : 'ncx',
      'x-dtbook+xml' : 'dtb',
      'x-dtbresource+xml' : 'res',
      'x-eva' : 'eva',
      'x-font-bdf' : 'bdf',
      'x-font-ghostscript' : 'gsf',
      'x-font-linux-psf' : 'psf',
      'x-font-otf' : 'otf',
      'x-font-pcf' : 'pcf',
      'x-font-snf' : 'snf',
      'x-font-ttf' : ['ttf', 'ttc'],
      'x-font-type1' : ['pfa', 'pfb', 'pfm', 'afm'],
      'x-font-woff' : 'woff',
      'x-freearc' : 'arc',
      'x-gca-compressed' : 'gca',
      'x-glulx' : 'ulx',
      'x-gramps-xml' : 'gramps',
      'x-install-instructions' : 'install',
      'x-lzh-compressed' : ['lzh', 'lha'],
      'x-mie' : 'mie',
      'x-mobipocket-ebook' : ['prc', 'mobi'],
      'x-ms-application' : 'application',
      'x-ms-shortcut' : 'lnk',
      'x-ms-xbap' : 'xbap',
      'x-msbinder' : 'obd',
      'x-mscardfile' : 'crd',
      'x-msclip' : 'clp',
      'x-msdownload' : ['exe', 'dll', 'com', 'bat', 'msi'],
      'x-msmediaview' : ['mvb', 'm13', 'm14'],
      'x-msmetafile' : ['wmf', 'wmz', 'emf', 'emz'],
      'x-msmoney' : 'mny',
      'x-mspublisher' : 'pub',
      'x-msschedule' : 'scd',
      'x-msterminal' : 'trm',
      'x-mswrite' : 'wri',
      'x-nzb' : 'nzb',
      'x-pkcs12' : ['p12', 'pfx'],
      'x-pkcs7-certificates' : ['p7b', 'spc'],
      'x-research-info-systems' : 'ris',
      'x-silverlight-app' : 'xap',
      'x-sql' : 'sql',
      'x-stuffitx' : 'sitx',
      'x-subrip' : 'srt',
      'x-t3vm-image' : 't3',
      'x-tads' : 'gam',
      'x-tex' : 'tex',
      'x-tex-tfm' : 'tfm',
      'x-tgif' : 'obj',
      'x-xliff+xml' : 'xlf',
      'x-xz' : 'xz',
      'x-zmachine' : ['z1', 'z2', 'z3', 'z4', 'z5', 'z6', 'z7', 'z8'],
      'xaml+xml' : 'xaml',
      'xcap-diff+xml' : 'xdf',
      'xenc+xml' : 'xenc',
      'xml-dtd' : 'dtd',
      'xop+xml' : 'xop',
      'xproc+xml' : 'xpl',
      'xslt+xml' : 'xslt',
      'xv+xml' : ['mxml', 'xhvml', 'xvml', 'xvm'],
      'yang' : 'yang',
      'yin+xml' : 'yin',
      'envoy' : 'evy',
      'fractals' : 'fif',
      'internet-property-stream' : 'acx',
      'olescript' : 'axs',
      'vnd.ms-outlook' : 'msg',
      'vnd.ms-pkicertstore' : 'sst',
      'x-compress' : 'z',
      'x-compressed' : 'tgz',
      'x-gzip' : 'gz',
      'x-perfmon' : ['pma', 'pmc', 'pml', 'pmr', 'pmw'],
      'x-pkcs7-mime' : ['p7c', 'p7m'],
      'ynd.ms-pkipko' : 'pko'
    },
    'audio' : {
      'amr' : 'amr',
      'amr-wb' : 'awb',
      'annodex' : 'axa',
      'basic' : ['au', 'snd'],
      'flac' : 'flac',
      'midi' : ['mid', 'midi', 'kar', 'rmi'],
      'mpeg' : ['mpga', 'mpega', 'mp2', 'mp3', 'm4a', 'mp2a', 'm2a', 'm3a'],
      'mpegurl' : 'm3u',
      'ogg' : ['oga', 'ogg', 'spx'],
      'prs.sid' : 'sid',
      'x-aiff' : ['aif', 'aiff', 'aifc'],
      'x-gsm' : 'gsm',
      'x-ms-wma' : 'wma',
      'x-ms-wax' : 'wax',
      'x-pn-realaudio' : 'ram',
      'x-realaudio' : 'ra',
      'x-sd2' : 'sd2',
      'x-wav' : 'wav',
      'adpcm' : 'adp',
      'mp4' : 'mp4a',
      's3m' : 's3m',
      'silk' : 'sil',
      'vnd.dece.audio' : ['uva', 'uvva'],
      'vnd.digital-winds' : 'eol',
      'vnd.dra' : 'dra',
      'vnd.dts' : 'dts',
      'vnd.dts.hd' : 'dtshd',
      'vnd.lucent.voice' : 'lvp',
      'vnd.ms-playready.media.pya' : 'pya',
      'vnd.nuera.ecelp4800' : 'ecelp4800',
      'vnd.nuera.ecelp7470' : 'ecelp7470',
      'vnd.nuera.ecelp9600' : 'ecelp9600',
      'vnd.rip' : 'rip',
      'webm' : 'weba',
      'x-aac' : 'aac',
      'x-caf' : 'caf',
      'x-matroska' : 'mka',
      'x-pn-realaudio-plugin' : 'rmp',
      'xm' : 'xm',
      'mid' : ['mid', 'rmi']
    },
    'chemical' : {
      'x-alchemy' : 'alc',
      'x-cache' : ['cac', 'cache'],
      'x-cache-csf' : 'csf',
      'x-cactvs-binary' : ['cbin', 'cascii', 'ctab'],
      'x-cdx' : 'cdx',
      'x-chem3d' : 'c3d',
      'x-cif' : 'cif',
      'x-cmdf' : 'cmdf',
      'x-cml' : 'cml',
      'x-compass' : 'cpa',
      'x-crossfire' : 'bsd',
      'x-csml' : ['csml', 'csm'],
      'x-ctx' : 'ctx',
      'x-cxf' : ['cxf', 'cef'],
      'x-embl-dl-nucleotide' : ['emb', 'embl'],
      'x-gamess-input' : ['inp', 'gam', 'gamin'],
      'x-gaussian-checkpoint' : ['fch', 'fchk'],
      'x-gaussian-cube' : 'cub',
      'x-gaussian-input' : ['gau', 'gjc', 'gjf'],
      'x-gaussian-log' : 'gal',
      'x-gcg8-sequence' : 'gcg',
      'x-genbank' : 'gen',
      'x-hin' : 'hin',
      'x-isostar' : ['istr', 'ist'],
      'x-jcamp-dx' : ['jdx', 'dx'],
      'x-kinemage' : 'kin',
      'x-macmolecule' : 'mcm',
      'x-macromodel-input' : ['mmd', 'mmod'],
      'x-mdl-molfile' : 'mol',
      'x-mdl-rdfile' : 'rd',
      'x-mdl-rxnfile' : 'rxn',
      'x-mdl-sdfile' : ['sd', 'sdf'],
      'x-mdl-tgf' : 'tgf',
      'x-mmcif' : 'mcif',
      'x-mol2' : 'mol2',
      'x-molconn-Z' : 'b',
      'x-mopac-graph' : 'gpt',
      'x-mopac-input' : ['mop', 'mopcrt', 'mpc', 'zmt'],
      'x-mopac-out' : 'moo',
      'x-ncbi-asn1' : 'asn',
      'x-ncbi-asn1-ascii' : ['prt', 'ent'],
      'x-ncbi-asn1-binary' : ['val', 'aso'],
      'x-pdb' : ['pdb', 'ent'],
      'x-rosdal' : 'ros',
      'x-swissprot' : 'sw',
      'x-vamas-iso14976' : 'vms',
      'x-vmd' : 'vmd',
      'x-xtel' : 'xtel',
      'x-xyz' : 'xyz'
    },
    'image' : {
      'gif' : 'gif',
      'ief' : 'ief',
      'jpeg' : ['jpeg', 'jpg', 'jpe'],
      'pcx' : 'pcx',
      'png' : 'png',
      'svg+xml' : ['svg', 'svgz'],
      'tiff' : ['tiff', 'tif'],
      'vnd.djvu' : ['djvu', 'djv'],
      'vnd.wap.wbmp' : 'wbmp',
      'x-canon-cr2' : 'cr2',
      'x-canon-crw' : 'crw',
      'x-cmu-raster' : 'ras',
      'x-coreldraw' : 'cdr',
      'x-coreldrawpattern' : 'pat',
      'x-coreldrawtemplate' : 'cdt',
      'x-corelphotopaint' : 'cpt',
      'x-epson-erf' : 'erf',
      'x-icon' : 'ico',
      'x-jg' : 'art',
      'x-jng' : 'jng',
      'x-nikon-nef' : 'nef',
      'x-olympus-orf' : 'orf',
      'x-photoshop' : 'psd',
      'x-portable-anymap' : 'pnm',
      'x-portable-bitmap' : 'pbm',
      'x-portable-graymap' : 'pgm',
      'x-portable-pixmap' : 'ppm',
      'x-rgb' : 'rgb',
      'x-xbitmap' : 'xbm',
      'x-xpixmap' : 'xpm',
      'x-xwindowdump' : 'xwd',
      'bmp' : 'bmp',
      'cgm' : 'cgm',
      'g3fax' : 'g3',
      'ktx' : 'ktx',
      'prs.btif' : 'btif',
      'sgi' : 'sgi',
      'vnd.dece.graphic' : ['uvi', 'uvvi', 'uvg', 'uvvg'],
      'vnd.dwg' : 'dwg',
      'vnd.dxf' : 'dxf',
      'vnd.fastbidsheet' : 'fbs',
      'vnd.fpx' : 'fpx',
      'vnd.fst' : 'fst',
      'vnd.fujixerox.edmics-mmr' : 'mmr',
      'vnd.fujixerox.edmics-rlc' : 'rlc',
      'vnd.ms-modi' : 'mdi',
      'vnd.ms-photo' : 'wdp',
      'vnd.net-fpx' : 'npx',
      'vnd.xiff' : 'xif',
      'webp' : 'webp',
      'x-3ds' : '3ds',
      'x-cmx' : 'cmx',
      'x-freehand' : ['fh', 'fhc', 'fh4', 'fh5', 'fh7'],
      'x-pict' : ['pic', 'pct'],
      'x-tga' : 'tga',
      'cis-cod' : 'cod',
      'pipeg' : 'jfif'
    },
    'message' : {
      'rfc822' : ['eml', 'mime', 'mht', 'mhtml', 'nws']
    },
    'model' : {
      'iges' : ['igs', 'iges'],
      'mesh' : ['msh', 'mesh', 'silo'],
      'vrml' : ['wrl', 'vrml'],
      'x3d+vrml' : ['x3dv', 'x3dvz'],
      'x3d+xml' : ['x3d', 'x3dz'],
      'x3d+binary' : ['x3db', 'x3dbz'],
      'vnd.collada+xml' : 'dae',
      'vnd.dwf' : 'dwf',
      'vnd.gdl' : 'gdl',
      'vnd.gtw' : 'gtw',
      'vnd.mts' : 'mts',
      'vnd.vtu' : 'vtu'
    },
    'text' : {
      'cache-manifest' : ['manifest', 'appcache'],
      'calendar' : ['ics', 'icz', 'ifb'],
      'css' : 'css',
      'csv' : 'csv',
      'h323' : '323',
      'html' : ['html', 'htm', 'shtml', 'stm'],
      'iuls' : 'uls',
      'mathml' : 'mml',
      'plain' : ['txt', 'text', 'brf', 'conf', 'def', 'list', 'log', 'in', 'bas'],
      'richtext' : 'rtx',
      'scriptlet' : ['sct', 'wsc'],
      'texmacs' : ['tm', 'ts'],
      'tab-separated-values' : 'tsv',
      'vnd.sun.j2me.app-descriptor' : 'jad',
      'vnd.wap.wml' : 'wml',
      'vnd.wap.wmlscript' : 'wmls',
      'x-bibtex' : 'bib',
      'x-boo' : 'boo',
      'x-c++hdr' : ['h++', 'hpp', 'hxx', 'hh'],
      'x-c++src' : ['c++', 'cpp', 'cxx', 'cc'],
      'x-component' : 'htc',
      'x-dsrc' : 'd',
      'x-diff' : ['diff', 'patch'],
      'x-haskell' : 'hs',
      'x-java' : 'java',
      'x-literate-haskell' : 'lhs',
      'x-moc' : 'moc',
      'x-pascal' : ['p', 'pas'],
      'x-pcs-gcd' : 'gcd',
      'x-perl' : ['pl', 'pm'],
      'x-python' : 'py',
      'x-scala' : 'scala',
      'x-setext' : 'etx',
      'x-tcl' : ['tcl', 'tk'],
      'x-tex' : ['tex', 'ltx', 'sty', 'cls'],
      'x-vcalendar' : 'vcs',
      'x-vcard' : 'vcf',
      'n3' : 'n3',
      'prs.lines.tag' : 'dsc',
      'sgml' : ['sgml', 'sgm'],
      'troff' : ['t', 'tr', 'roff', 'man', 'me', 'ms'],
      'turtle' : 'ttl',
      'uri-list' : ['uri', 'uris', 'urls'],
      'vcard' : 'vcard',
      'vnd.curl' : 'curl',
      'vnd.curl.dcurl' : 'dcurl',
      'vnd.curl.scurl' : 'scurl',
      'vnd.curl.mcurl' : 'mcurl',
      'vnd.dvb.subtitle' : 'sub',
      'vnd.fly' : 'fly',
      'vnd.fmi.flexstor' : 'flx',
      'vnd.graphviz' : 'gv',
      'vnd.in3d.3dml' : '3dml',
      'vnd.in3d.spot' : 'spot',
      'x-asm' : ['s', 'asm'],
      'x-c' : ['c', 'cc', 'cxx', 'cpp', 'h', 'hh', 'dic'],
      'x-fortran' : ['f', 'for', 'f77', 'f90'],
      'x-opml' : 'opml',
      'x-nfo' : 'nfo',
      'x-sfv' : 'sfv',
      'x-uuencode' : 'uu',
      'webviewhtml' : 'htt'
    },
    'video' : {
      '3gpp' : '3gp',
      'annodex' : 'axv',
      'dl' : 'dl',
      'dv' : ['dif', 'dv'],
      'fli' : 'fli',
      'gl' : 'gl',
      'mpeg' : ['mpeg', 'mpg', 'mpe', 'm1v', 'm2v', 'mp2', 'mpa', 'mpv2'],
      'mp4' : ['mp4', 'mp4v', 'mpg4'],
      'quicktime' : ['qt', 'mov'],
      'ogg' : 'ogv',
      'vnd.mpegurl' : ['mxu', 'm4u'],
      'x-flv' : 'flv',
      'x-la-asf' : ['lsf', 'lsx'],
      'x-mng' : 'mng',
      'x-ms-asf' : ['asf', 'asx', 'asr'],
      'x-ms-wm' : 'wm',
      'x-ms-wmv' : 'wmv',
      'x-ms-wmx' : 'wmx',
      'x-ms-wvx' : 'wvx',
      'x-msvideo' : 'avi',
      'x-sgi-movie' : 'movie',
      'x-matroska' : ['mpv', 'mkv', 'mk3d', 'mks'],
      '3gpp2' : '3g2',
      'h261' : 'h261',
      'h263' : 'h263',
      'h264' : 'h264',
      'jpeg' : 'jpgv',
      'jpm' : ['jpm', 'jpgm'],
      'mj2' : ['mj2', 'mjp2'],
      'vnd.dece.hd' : ['uvh', 'uvvh'],
      'vnd.dece.mobile' : ['uvm', 'uvvm'],
      'vnd.dece.pd' : ['uvp', 'uvvp'],
      'vnd.dece.sd' : ['uvs', 'uvvs'],
      'vnd.dece.video' : ['uvv', 'uvvv'],
      'vnd.dvb.file' : 'dvb',
      'vnd.fvt' : 'fvt',
      'vnd.ms-playready.media.pyv' : 'pyv',
      'vnd.uvvu.mp4' : ['uvu', 'uvvu'],
      'vnd.vivo' : 'viv',
      'webm' : 'webm',
      'x-f4v' : 'f4v',
      'x-m4v' : 'm4v',
      'x-ms-vob' : 'vob',
      'x-smv' : 'smv'
    },
    'x-conference' : {
      'x-cooltalk' : 'ice'
    },
    'x-world' : {
      'x-vrml' : ['vrm', 'vrml', 'wrl', 'flr', 'wrz', 'xaf', 'xof']
    }
  };

  var mimeTypes = (function() {
    var type, subtype, val, index, mimeTypes = {};
    for (type in table) {
      if (table.hasOwnProperty(type)) {
        for (subtype in table[type]) {
          if (table[type].hasOwnProperty(subtype)) {
            val = table[type][subtype];
            if (typeof val == 'string') {
              mimeTypes[val] = type + '/' + subtype;
            } else {
              for (index = 0; index < val.length; index++) {
                mimeTypes[val[index]] = type + '/' + subtype;
              }
            }
          }
        }
      }
    }
    return mimeTypes;
  })();

  window['JSZip'].prototype['getMimeType'] = function(filename) {
    var defaultValue = 'application/octet-stream';
    return filename && mimeTypes[filename.split('.').pop().toLowerCase()] || defaultValue;
  };

  return true;
});


define('epub/Model', ['lib/Class', 'epub/Utils', 'epub/Parser', 'epub/Mime'], function(Class, U, Parser) {

  function generateBookKey(id) {
    return 'epub:' + window.location.host + ':' + id;
  }

  function decodeUrl(url) {
    return window['decodeURIComponent'](url);
  }

  function getEntry(url, model) {
    return model.store.file(decodeUrl(url));
  }

  function error(deferred, url) {
    deferred.reject({
      'message': 'File not found in the epub: ' + url,
      'stack': new Error().stack
    });
    return deferred.promise;
  }

  function loadContainer(model, _containerPath) {
    var containerPath = _containerPath || 'META-INF/container.xml',
        containerPromise = model.getXML(containerPath)
          .then(function(containerXML) {
              return Parser.getContainer(containerXML);
            })
          .then(function(container) {
              model.contentsPath = container.basePath;
              model.packageUrl = container.packagePath;
              model.encoding = container.encoding;
              return model.getXML(model.packageUrl);
            });

    containerPromise.catch(function(error) {
      console.error('Could not load book at: ' + containerPath, error);
      model.trigger('book:loadFailed', containerPath);
    });
    return containerPromise;
  }

  function parse(model, packageXML) {
    var contents = Parser.getPackageContents(packageXML, model.contentsPath);

    model.manifest = contents.manifest;
    model.spine = contents.spine;
    model.spineIndexByURL = contents.spineIndexByURL;
    model.spineNodeIndex = contents.spineNodeIndex;
    model.metadata = contents.metadata;
    model.navPath = contents.navPath;
    model.tocPath = contents.tocPath;

    if (!model.bookKey) {
      model.bookKey = generateBookKey(model.metadata.identifier);
    }

    if (contents.coverPath) {
      model.coverPath = model.contentsPath + contents.coverPath;
    }

    //-- Load the TOC, optional; either the EPUB3 XHTML
    // Navigation file or the EPUB2 NCX file
    if (model.navPath) {
      model.navUrl = model.contentsPath + contents.navPath;

      model.getXML(model.navUrl)
        .then(function(navHtml) {
            // Grab Table of Contents
            return Parser.getNav(navHtml, model.spineIndexByURL, model.spine);
          })
        .then(function(toc) {
            model.toc = toc;
            //model.ready.toc.resolve(model.contents.toc);
          }, function(error) {
            console.error(CANNOTRESOLVETOC, error);
            model.ready.resolve(false);
          });
    } else if (model.tocPath) {
      model.tocUrl = model.contentsPath + model.tocPath;
      model.getXML(model.tocUrl)
        .then(function(tocXml) {
            // Grab Table of Contents
            return Parser.getToc(tocXml, model.spineIndexByURL, model.spine);
          })
        .then(function(toc) {
            model.toc = toc;
          }, function(error) {
            console.error('Can not resolve TOC', error);
            model.ready.resolve(false);
          });
    } else {
      console.error('Can not find toc path');
      model.ready.resolve(false);
    }
  }

  return Class.extend('Model', {

    manifest: '',

    spine: '',

    toc: '',

    spineIndexByURL: '',

    spineNodeIndex: '',

    metadata: '',

    contentsPath: '',

    packageUrl: '',

    encoding: '',

    coverPath: '',

    navUrl: '',

    tocUrl: '',

    urlCache: {},

    ready: '',

    store: null,

    $q: '',

    $scope: '',

    constructor: function(_blob, _$q, _$scope) {
      var self = this;
      self.$q = _$q;
      self.ready = _$q.defer();
      self.$scope = _$scope;
      self.store = new window['JSZip'](_blob);
      loadContainer(self, '').then(function(pkg) {
        parse(self, pkg);
        self.ready.resolve(self);
        return self;
      });
    },

    getXML: function(url, encoding) {
      return this.getText(url, encoding)
        .then(function(text) {
            return new DOMParser().parseFromString(text, 'text/xml');
          });
    },

    getText: function(url, encoding) {
      var deferred = this.$q.defer(),
          entry = getEntry(url, this);

      if (!entry) {
        return error(deferred, url);
      }

      deferred.resolve(entry['asText']());
      return deferred.promise;
    },

    getUrl: function(url, mime) {
      var self = this,
          deferred = self.$q.defer(),
          entry = getEntry(url, self),
          tempUrl,
          blob;

      if (!entry) {
        return error(deferred, url);
      }

      if (url in self.urlCache) {
        deferred.resolve(self.urlCache[url]);
        return deferred.promise;
      }

      blob = new Blob([entry['asUint8Array']()], {
        type: mime || self.store['getMimeType'](entry.name)
      });

      tempUrl = U.createObjectUrl(blob);
      deferred.resolve(tempUrl);
      self.urlCache[url] = tempUrl;
      return deferred.promise;
    },

    revokeUrl: function(url) {
      var fromCache = this.urlCache[url];
      if (fromCache) U.revokeObjectUrl(fromCache);
    },

    destroy: function() {
      angular.forEach(this.urlCache, function(value, key) {
        U.revokeObjectUrl(value);
      });
    }
  });
});


define('epub/Chapter', ['lib/Class'], function(Class) {
  return Class.create('Chapter', {
    href: '',
    absolute: '',
    id: '',
    spinePos: '',
    properties: '',
    isRendered: false,
    manifestProperties: '',
    linear: '',
    pages: -1,
    model: '',
    url: '',

    constructor: function(spineObject, model) {
      this.$q = model.$q;
      this.href = spineObject.href;
      this.absolute = spineObject.url;
      this.id = spineObject.id;
      this.spinePos = spineObject.index;
      this.properties = spineObject.properties;
      this.manifestProperties = spineObject.manifestProperties;
      this.linear = spineObject.linear;
      this.model = model;
    },

    getUrl: function() {
      var deferred = this.$q.defer(),
          chapter = this,
          url;

      if (!chapter.url) {
        chapter.model.getUrl(chapter.absolute).then(function(url) {
          chapter.url = url;
          deferred.resolve(url);
        });
      } else {
        url = chapter.url;
        deferred.resolve(url);
      }

      return deferred.promise;
    },

    unload: function() {
      if (this.url) {
        this.model.revokeUrl(this.url);
        this.url = '';
      }
    }
  });
});


define('epub/BookEvent', [],function() {
  return {
    IFRAMELOADED: 'iframe:loaded',
    VISIBLERANGECHANGED: 'view:visibleRangeChanged',
    PAGECHANGED: 'book:pageChanged',
    VIEWCREATED: 'book:viewcreated',
    CHAPTERUNLOADED: 'view:chapterunloaded',
    PAGELIST: 'book:pagelist',
    LAYOUT: 'view:layout'
  };
});


define('epub/replace', ['epub/Utils'], function(U) {

  var $q,
      cache = {};

  /* */
  function _srcs(_store, full, done) {
    _store.getUrl(full).then(done);
  }

  /* */
  function _hrefs(view) {
    var book = view.controller;
    function replacements(link, done) {
      var href = link.getAttribute('href'),
          isRelative = href.search('://'),
          directory,
          relative,
          location;

      if (isRelative != -1) {

        link.setAttribute('target', '_blank');

      } else {
        // Links may need to be resolved, such as ../chp1.xhtml
        directory = U.uri(view.window.location.href).directory;
        if (directory) {
          relative = U.resolveURL(directory, href);
        } else {
          relative = href;
        }

        link.onclick = function() {
          book.gotoHref(relative);
          return false;
        };

      }
      done();
    }
    return view.replace('a[href]', replacements);
  }

  /* */
  function _head(view) {
    return view.replaceWithStored('link[href]', 'href', _links);
  }

  /* */
  function _resources(view) {
    //srcs = this.doc.querySelectorAll('[src]');
    return view.replaceWithStored('[src]', 'src', _srcs);
  }

  /* */
  function _svg(view) {
    return view.replaceWithStored('image', 'xlink:href', function(_store, full, done) { _store.getUrl(full).then(done);});
  }

  /* */
  function _links(_store, full, done, link) {
    //-- Handle replacing urls in CSS
    if (link.getAttribute('rel') === 'stylesheet') {
      _stylesheets(_store, full)
        .then(function(url, full) {
            // done
            setTimeout(function() {
              done(url, full);
            }, 3); //-- Allow for css to apply before displaying chapter
          }, function(reason) {
            // we were unable to replace the style sheets
            done(null);
          });
    } else {
      _store.getUrl(full).then(done, function(reason) {
        // we were unable to get the url, signal to upper layer
        done(null);
      });
    }
  }

  /* */
  function _stylesheets(_store, _url) {
    if (!_store) return;
    var deferred = $q.defer();

    if (_url in cache) {
      deferred.resolve(cache[_url]);
    } else {
      _store.getText(_url)
        .then(function(text) {
            _cssUrls(_store, _url, text)
              .then(function(newText) {
                  var blob = new Blob([newText], {'type': 'text\/css'}),
                      url = U.createObjectUrl(blob);
                  cache[_url] = url;
                  deferred.resolve(url);

                }, function(reason) {
                  deferred.reject(reason);
                });

          }, function(reason) {
            deferred.reject(reason);
          });
    }

    return deferred.promise;
  }

  /* */
  function _cssUrls(_store, base, text) {
    if (!_store) return;
    var deferred = $q.defer(),
        promises = [],
        matches = text.match(/url\(\'?\"?([^\'|^\"^\)]*)\'?\"?\)/g);

    if (!matches) {
      deferred.resolve(text);
      return deferred.promise;
    }

    matches.forEach(function(str) {
      var full = U.resolveURL(base, str.replace(/url\(|[|\)|\'|\"]/g, '')),
          replaced = _store.getUrl(full)
            .then(function(url) {
                text = text.replace(str, 'url("' + url + '")');
              }, function(reason) {
                deferred.reject(reason);
              });
      promises.push(replaced);
    });

    $q.all(promises).then(function() {
      deferred.resolve(text);
    });

    return deferred.promise;
  }

  /* */
  function _images(view) {
    var d = $q.defer();
    if (view.currentChapter.isRendered) {
      d.resolve();
      return d.promise;
    }
    var images = view.documentEl.querySelectorAll('img'),
        items = Array.prototype.slice.call(images);
    //iheight = renderer.height,//chapter.bodyEl.clientHeight,//chapter.doc.body.getBoundingClientRect().height,
    //oheight;

    function size() {
      var itemRect = this.getBoundingClientRect(),
          rectHeight = itemRect.height,
          top = itemRect.top,
          oHeight = this.getAttribute('data-height'),
          height = oHeight || rectHeight,
          newHeight,
          fontSize = Number(window.getComputedStyle(this, '').fontSize.match(/(\d*(\.\d*)?)px/)[1]),
          fontAdjust = fontSize ? fontSize / 2 : 0,
          style = this.style,
          iHeight = view.documentEl.clientHeight;

      if (top < 0) top = 0;

      if (height + top >= iHeight) {

        if (top < iHeight / 2) {
          // Remove top and half font-size from height to keep container from overflowing
          newHeight = iHeight - top - fontAdjust;
          style.maxHeight = newHeight + 'px';
          style.width = 'auto';
        } else {
          if (height > iHeight) {
            style.maxHeight = iHeight + 'px';
            style.width = 'auto';
            itemRect = this.getBoundingClientRect();
            height = itemRect.height;
          }
          style.display = 'block';
          style['WebkitColumnBreakBefore'] = 'always';
          style['breakBefore'] = 'column';

        }

        this.setAttribute('data-height', newHeight);

      } else {
        style.removeProperty('max-height');
        style.removeProperty('margin-top');
      }
    }

    //-- Only adjust images for reflowable text
    if (view.layoutSettings.layout != 'reflowable') {
      return;
    }

    items.forEach(function(item) {
      item.addEventListener('load', size, false);
      view.on('renderer:resized', size);
      view.on('renderer:chapterUnloaded', function() {
        item.removeEventListener('load', size);
        view.off('renderer:resized', size);
      });
      size.call(item);
    });
    d.resolve();
    return d.promise;
  }

  function _destroy() {
    angular.forEach(cache, function(value, key) {
      U.revokeObjectUrl(value);
      delete cache[key];
    });
  }

  return function(_$q) {
    if (!$q && !_$q) throw ('No deferred in arguments');
    if (!$q) $q = _$q;
    return {
      hrefs: _hrefs,
      head: _head,
      resources: _resources,
      svg: _svg,
      srcs: _srcs,
      links: _links,
      stylesheets: _stylesheets,
      cssUrls: _cssUrls,
      images: _images,
      destroy: _destroy
    };
  }
});


define('epub/BookView', ['lib/Class', 'epub/BookEvent', 'epub/Utils', 'epub/replace'], function(Class, BookEvent, U, R) {

  var HIDDEN = 'hidden',
      HORIZONTAL = 'horizontal',
      AUTO = 'auto',
      REFLOWABLESPREADS = 'ReflowableSpreads',
      REFLOWABLE = 'Reflowable',
      FIXED = 'Fixed',
      columnAxis = U.prefixed('columnAxis'),
      columnGap = U.prefixed('columnGap'),
      columnWidth = U.prefixed('columnWidth'),
      columnFill = U.prefixed('columnFill');

  function loadedHandler() {
    var url = this.element.contentWindow.location.href;
    this.left = 0;
    this.document = this.element.contentDocument;
    this.documentEl = this.document.documentElement;
    this.headEl = this.document.head;
    this.bodyEl = this.document.body || this.document.querySelector('body');
    this.window = this.element.contentWindow;

    //-- Clear Margins
    if (this.bodyEl) {
      this.bodyEl.style.margin = '0';
      this.bodyEl.style.padding = '0';
    }
    if (url != 'about:blank') {
      this.trigger(BookEvent.IFRAMELOADED, url);
      if (this.iFrameReady) this.iFrameReady.resolve(this);
    }
  }

  function errorLoadedHandler(e) {
    if (this.iFrameReady) this.iFrameReady.reject(
        {
          message: 'Error Loading Contents: ' + e,
          stack: new Error().stack
        }
      );
  }

  function getChapterLayout(globalSettings, chapterProperties) {
    var settings = angular.extend({}, globalSettings);

    chapterProperties.forEach(function(prop) {
      var rendition = prop.replace('rendition:', '');
      var split = rendition.indexOf('-');
      var property, value;

      if (split != -1) {
        property = rendition.slice(0, split);
        value = rendition.slice(split + 1);
        settings[property] = value;
      }
    });
    return settings;
  }

  function determineLayout(view) {
    var spreads = !(view.isForcedSingle || !view.minSpreadWidth || this.width < view.minSpreadWidth),
        layoutMethod = spreads ? REFLOWABLESPREADS : REFLOWABLE,
        scroll = false;

    switch (view.settings.layout) {
      case 'pre-paginated':
        layoutMethod = FIXED;
        scroll = true;
        spreads = false;
        break;
      case 'reflowable':
        if (view.settings.spread === 'none') {
          layoutMethod = REFLOWABLE;
          spreads = false;
        }
        if (view.settings.spread === 'both') {
          layoutMethod = REFLOWABLESPREADS;
          spreads = true;
        }
        break;
    }

    view.spreads = spreads;
    view.scroll = scroll;
    view.trigger(BookEvent.LAYOUT, layoutMethod);
    return layoutMethod;
  }

  function applyStyles(view) {
    var styles = view.settings.styles,
        style;
    for (style in styles) {
      if (styles.hasOwnProperty(style)) view.setStyle(style, styles[style]);
    }
  }

  function applyHeadTags(view) {
    var headTags = view.settings.headTags,
        tag;
    for (tag in headTags) {
      if (headTags.hasOwnProperty(tag)) view.addHeadTag(tag, headTags[tag]);
    }
  }

  function formatLayout(view) {

    function setStyle(width, height, colwidth, colgap) {
      var style = view.documentEl.style;
      style.overflow = HIDDEN;
      style.width = width + 'px';
      style.height = height + 'px';
      style[columnAxis] = HORIZONTAL;
      style[columnFill] = AUTO;
      style[columnWidth] = colwidth + 'px';
      style[columnGap] = colgap + 'px';
    }

    switch (view.layoutMethod) {
      case FIXED: break;
      case REFLOWABLE:
        //-- Single Page
        //-- Check the width and create even width columns
        var width = Math.floor(view.width),
            section = Math.floor(width / 8),
            gap = (view.gap >= 0) ? view.gap : ((section % 2 === 0) ? section : section - 1);
        setStyle(width, view.height, width, gap);
        view.spreadWidth = (width + gap);
        view.colWidth = width;
        view.gap = gap;
        view.pageWidth = view.spreadWidth;
        view.pageHeight = view.height;
        break;
      case REFLOWABLESPREADS: break;
    }
  }

  return Class.create('BookView', {

    settings: {
      width: '',
      height: '',
      container: '',
      styles: {},
      headTags: {},
      visible: true
    },

    controller: '',

    element: '',

    document: '',

    headEl: '',

    bodyEl: '',

    window: '',

    documentEl: '',

    width: 0,

    height: 0,

    gap: 0,

    spreadWidth: 0,

    colWidth: 0,

    pageWidth: 0,

    pageHeight: 0,

    left: 0,

    direction: '',

    renderQ: [],

    layoutSettings: {},

    layoutMethod: '',

    spreads: true,

    scroll: false,

    isForcedSingle: false,

    rendering: false,

    iFrameReady: '',

    currentChapter: '',

    chapterPos: 0,

    caches: {},

    $q: '',

    $scope: '',

    replaces: '',

    constructor: function(_settings) {
      var s = {}, el = this.element = document.createElement('iframe');
      angular.extend(s, this.settings, _settings);
      this.controller = _settings.controller;
      this.$q = this.controller.$q;
      this.$scope = this.controller.$scope;
      if (!s.container) {
        console.error('No container specified');
        return false;
      }
      this.settings = s;
      //initialize replaces
      R(this.$q);
      this.renderQ = U.queue(this);
      el['id'] = s.id || this.settings.id;
      el['scrolling'] = 'no';
      el['seamless'] = 'seamless';
      el['style']['border'] = 'none';
      el.addEventListener('load', loadedHandler.bind(this), false);

      this.width = s.width || s.container.clientWidth;
      this.height = s.height || s.container.clientHeight;

      //s.container.appendChild(el);
      angular.element(s.container).prepend(el);
      this.resize(this.width, this.height);
    },

    trigger: function(event, value) {
      this.$scope.$emit(event, {sender: this, value: value});
    },

    on: function(event, callback) {
      this.$scope.$on(event, callback);
    },

    resize: function(width, height) {
      if (!this.element) return;
      width = width || '100%';
      this.height = this.element['height'] = height || '100%';
      if (!isNaN(width) && width % 2 !== 0) {
        width += 1; //-- Prevent cutting off edges of text in columns
      }
      this.width = this.element['width'] = width;
    },

    setStyle: function(style, val, prefixed) {
      if (prefixed) {
        style = U.prefixed(style);
      }
      if (this.bodyEl) this.bodyEl['style'][style] = val;
    },

    setGap: function(gap) {
      this.gap = gap;
    },

    addHeadTag: function(tag, attrs, _doc) {
      var doc = _doc || this.document,
          tagEl = doc.createElement(tag),
          headEl = doc['head'],
          attr;

      for (attr in attrs) {
        if (attrs.hasOwnProperty(attr)) tagEl.setAttribute(attr, attrs[attr]);
      }

      if (headEl) headEl.insertBefore(tagEl, headEl.firstChild);
    },

    displayChapter: function(chapter, _layout) {
      var self = this,
          deferred = self.$q.defer(),
          layout = _layout || {layout: 'reflowable', spread: 'auto', orientation: 'auto'};

      if (self.rendering) {
        console.error('Rendering In Progress');
        deferred.reject();
        return deferred.promise;
      }

      function chapterUrlReady(url) {
        if (self.currentChapter) {
          self.currentChapter.unload();
          self.trigger(BookEvent.CHAPTERUNLOADED);
          //self.contents = null;
          //self.doc = null;
          //self.pageMap = null;
        }
      }

      function render() {
        self.iFrameReady = self.$q.defer();
        self.currentChapter = chapter;
        self.chapterPos = 1;
        self.layoutSettings = getChapterLayout(layout, chapter.properties);
        self.layoutMethod = determineLayout(self);
        self.visible(false);
        self.window.location.replace(chapter.url);
        return self.iFrameReady.promise.then(afterRender);
      }

      function afterRender() {
        applyStyles(self);
        applyHeadTags(self);
        formatLayout(self);
        R().hrefs(self).then(function() {
          R().head(self).then(function() {
            R().resources(self).then(function() {
              R().images(self).then(function() {
                self.visible(true);
                self.trigger(BookEvent.VISIBLERANGECHANGED, self.getVisibleRange());
                self.rendering = false;
                deferred.resolve(self);
              });
            });
          });
        });
        return self;
      }

      self.rendering = true;
      chapter.getUrl().then(chapterUrlReady).then(render);
      return deferred.promise;
    },

    calculatePages: function() {
      var totalWidth, displayedPages;

      switch (this.layoutMethod) {
        case FIXED: break;
        case REFLOWABLE:
          this.documentEl.style.width = AUTO; //-- reset width for calculations
          totalWidth = this.documentEl.scrollWidth;
          displayedPages = Math.ceil(totalWidth / this.pageWidth);
          return {
            displayedPages: displayedPages,
            pageCount: displayedPages
          };
          break;
        case REFLOWABLESPREADS: break;
      }
    },

    visible: function(bool) {
      if (typeof(bool) === 'undefined') {
        return this.element.style.visibility;
      }

      if (bool === true && this.settings.visible) {
        this.element.style.visibility = 'visible';
      } else if (bool === false) {
        this.element.style.visibility = 'hidden';
      }
    },

    page: function(number) {
      if (number >= 1 && number <= this.currentChapter.pages.displayedPages) {
        this.chapterPos = number;
        var left = this.left = this.pageWidth * (number - 1); //-- pages start at 1
        if (navigator.userAgent.match(/(iPad|iPhone|iPod|Mobile|Android)/g)) {
          //this.documentEl['style']['-webkit-transform'] = 'translate(' + (-left) + 'px, 0)';
          this.documentEl.style[U.prefixed('transform')] = 'translate(' + (-left) + 'px, 0)';
        } else {
          this.document.defaultView.scrollTo(left, 0);
        }
        this.trigger(BookEvent.VISIBLERANGECHANGED, this.getVisibleRange());
        return true;
      }
      return false;
    },

    nextPage: function() {
      return this.page(this.chapterPos + 1);
    },

    prevPage: function() {
      return this.page(this.chapterPos - 1);
    },

    section: function(fragment) {
      var el = this.document.getElementById(fragment);
      if (el) {
        this.page(this.pageByElement(el));
      }
    },

    pageByElement: function(el) {
      if (!el) return;
      var left = this.left + el.getBoundingClientRect()['left']; //-- Calculate left offset compared to scrolled position
      return Math.floor(left / this.pageWidth) + 1; //-- pages start at 1
    },

    replace: function(query, callback, finished, progress) {
      var items = this.documentEl.querySelectorAll(query),
          resources = Array.prototype.slice.call(items),
          count = resources.length,
          deferred = this.$q.defer();

      if (count === 0) {
        if (finished) finished(false);
        deferred.resolve(false);
        return deferred.promise;
      }
      resources.forEach(function(item) {
        var called = false;
        function after(result, full) {
          if (called === false) {
            count--;
            if (progress) progress(result, full, count);
            if (count <= 0) {
              if (finished) finished(true);
              deferred.resolve(true);
            }
            called = true;
          }
        }
        if (callback) callback(item, after);
      }.bind(this));
      return deferred.promise;
    },

    replaceWithStored: function(query, attr, func) {
      var deferred = this.$q.defer();
      if (this.currentChapter.isRendered) {
        deferred.resolve();
        return deferred.promise;
      }
      var _oldUrls,
          _newUrls = {},
          _store = this.currentChapter.model,
          _cache = this.caches[query],
          _uri = U.uri(this.currentChapter.absolute),
          _chapterBase = _uri.base,
          _attr = attr,
          _wait = 2000;

      if (!_store) return;

      function progress(url, full, count) {
        _newUrls[full] = url;
      }

      function finished(notempty) {
        angular.forEach(_oldUrls, function(url) {
          _store.revokeUrl(url);
        });
        _cache = _newUrls;
        deferred.resolve();
      }

      if (!_cache) _cache = {};
      _oldUrls = angular.extend({}, _cache);

      this.replace(query, function(link, done) {
        var src = link.getAttribute(_attr),
            full = U.resolveURL(_chapterBase, src);

        var replaceUrl = function(url) {
          var timeout;
          link.onload = function() {
            clearTimeout(timeout);
            done(url, full);
          };

          link.onerror = function(e) {
            clearTimeout(timeout);
            done(url, full);
            console.error(e);
          };

          if (query == 'image') {
            //-- SVG needs this to trigger a load event
            link.setAttribute('externalResourcesRequired', 'true');
          }

          if (query == 'link[href]' && link.getAttribute('rel') !== 'stylesheet') {
            //-- Only Stylesheet links seem to have a load events, just continue others
            done(url, full);
          }

          link.setAttribute(_attr, url);

          //-- If elements never fire Load Event, should continue anyways
          timeout = setTimeout(function() {
            done(url, full);
          }, _wait);

        };

        if (full in _oldUrls) {
          replaceUrl(_oldUrls[full]);
          _newUrls[full] = _oldUrls[full];
          delete _oldUrls[full];
        } else {
          func(_store, full, replaceUrl, link);
        }

      }, finished, progress);
      return deferred.promise;
    },

    getVisibleRange: function() {
      var chapters = this.controller.chapters,
          pagesBefore = 0,
          i = 0;
      while (i < this.currentChapter.spinePos) {
        pagesBefore += chapters[i].pages.pageCount;
        i++;
      }
      return pagesBefore + this.chapterPos;
    },

    destroy: function() {
      this.settings.container.removeChild(this.element);
      R().destroy();
    }

  });
});


define('epub/Book', ['lib/Class', 'epub/Model', 'epub/Chapter', 'epub/BookEvent', 'epub/BookView', 'epub/Utils'],
    function(Class, Model, Chapter, BookEvent, BookView, U) {

      function createBookView(book, _settings) {
        var settings = _settings || book.settings;
        return new BookView({
          controller: book,
          width: settings.width,
          height: settings.height,
          id: settings.id || 'iframe',
          container: settings.container,
          styles: settings.styles,
          headTags: settings.headTags,
          spread: settings.spread || 'none',
          layout: settings.layout || 'reflowable'
        });
      }

      function generatePageList(book) {
        var body = document.body,
            hiddenContainer = document.createElement('div'),
            hiddenEl = document.createElement('div'),
            s = angular.extend({}, book.settings),
            hiddenView,
            spineLength = book.spine.length,
            spinePos = -1,
            totalPages = 0,
            currentPage = 0,
            $q = book.$q,
            deferred = $q.defer();

        function nextChapter(opt_deferred) {
          var chapter,
              next = spinePos + 1,
              done = opt_deferred || $q.defer();

          book.trigger(BookEvent.PAGELIST, next / spineLength);
          if (next >= spineLength) {
            done.resolve();
          } else {
            spinePos = next;
            chapter = new Chapter(book.spine[spinePos], book);
            book.chapters[spinePos] = chapter;
            hiddenView.displayChapter(chapter).then(function() {
              chapter.pages = hiddenView.calculatePages();
              totalPages += chapter.pages.pageCount;
              // Load up the next chapter
              setTimeout(function() {
                nextChapter(done);
              }, 1);
            });
          }

          return done.promise;
        }

        hiddenContainer.style.visibility = hiddenContainer.style.overflow = hiddenEl.style.visibility = hiddenEl.style.overflow = 'hidden';
        hiddenContainer.style.width = hiddenContainer.style.height = '0';
        hiddenEl.style.width = book.settings.width + 'px';
        hiddenEl.style.height = book.settings.height + 'px';
        hiddenContainer.appendChild(hiddenEl);
        body.appendChild(hiddenContainer);
        s.container = hiddenEl;
        hiddenView = createBookView(book, s);

        nextChapter().then(function() {
          hiddenView.destroy();
          body.removeChild(hiddenContainer);
          book.totalPages = totalPages;
          deferred.resolve();
        });

        return deferred.promise;
      }

      var Book = Model.extend('Book', {
        settings: {
          width: 320,
          height: 521,
          minSpreadWidth: 320,
          gap: 'auto',
          bookKey: '',
          reload: false,
          forceSingle: false,
          goto: '',
          styles: {},
          headTags: {},
          container: '',
          spread: 'none'
        },

        globalLayoutProperties: '',

        spinePos: 0,

        view: '',

        //startQ: [],

        chapters: [],

        totalPages: 0,

        currentPage: 0,

        constructor: function(_settings, _blob, _$q, _$scope) {
          this.settings = angular.extend({}, this.settings, _settings);
          this._super([_blob, _$q, _$scope]);
          return this;
        },

        generatePages: function() {
          var book = this,
              deferred = book.$q.defer();
          book.ready.promise.then(function() {
            generatePageList(book).then(function() { deferred.resolve(book)});
          });
          return deferred.promise;
        },

        trigger: function(event, value) {
          this.$scope.$emit(event, {sender: this, value: value});
        },

        on: function(event, callback) {
          this.$scope.$on(event, callback);
        },

        show: function() {
          var self = this;
          function onReady() {
            self.view = createBookView(self);
            self.trigger(BookEvent.VIEWCREATED, self.view);
            self.view.on(BookEvent.VISIBLERANGECHANGED, function(event, data) {
              self.currentPage = data.value;
              self.trigger(BookEvent.PAGECHANGED, self.currentPage);
            });
            //self.startQ.flush();
            return self.settings.goto ? self.goto(self.settings.goto) : self.showChapter(self.spinePos);
          }
          return self.ready.promise.then(onReady);
        },

        goto: function(arg) {
          if (angular.isNumber(arg)) this.gotoPage(arg);
        },

        gotoPage: function(page) {
          var chapters = this.chapters,
              spinePos = 0,
              pages = 0,
              chapter;
          while (spinePos < chapters.length) {
            chapter = chapters[spinePos];
            pages += chapter.pages.pageCount;
            if (pages >= page) break;
            spinePos++;
          }

          if (chapter.spinePos === this.view.currentChapter.spinePos) {

          } else {
            this.showChapter(spinePos).then(
                function(view) {
                  view.page(pages - page);
                }
            );
          }
        },

        gotoHref: function(url) {
          var split = url.split('#'),
              chapter = split[0],
              section = split[1] || false,
              relativeURL = chapter.replace(this.settings.contentsPath, ''),
              spinePos = this.spineIndexByURL[relativeURL],
              currentChapter = this.view.currentChapter,
              deferred = this.$q.defer();

          //try to search by id
          if (!spinePos) {
            for (var i = 0; i < this.spine.length; i++) {
              if (this.spine[i].id === relativeURL) {
                spinePos = i;
                break;
              }
            }
          }

          //-- If link fragment only stay on current chapter
          if (!chapter) {
            spinePos = currentChapter ? currentChapter.spinePos : 0;
          }

          //-- Check that URL is present in the index, or stop
          if (typeof(spinePos) != 'number') return false;

          if (!currentChapter || spinePos != currentChapter.spinePos) {
            //-- Load new chapter if different than current
            return this.showChapter(spinePos).then(function() {
              if (section) {
                this.view.section(section);
              }
              deferred.resolve();
            }.bind(this));
          } else {
            //--  Goto section
            if (section) {
              this.view.section(section);
            } else {
              // Or jump to the start
              this.view.page(1);
            }
            deferred.resolve();
          }

        },

        showChapter: function(chapterIndex, atEnd, _deferred) {
          var self = this,//deferred = _deferred || new Deferred(),
              chapter = self.chapters[chapterIndex] || new Chapter(this.spine[chapterIndex], this);
          return this.view.displayChapter(chapter).then(function(view) {
            self.spinePos = chapterIndex;
            if (atEnd) view.page(chapter.pages.displayedPages);
            return view;
          });
        },

        nextPage: function() {
          var next = this.view.nextPage();
          if (!next) {
            return this.nextChapter();
          }
        },

        prevPage: function() {
          var prev = this.view.prevPage();
          if (!prev) {
            return this.prevChapter();
          }
        },

        firstPage: function() {},
        lastPage: function() {},

        nextChapter: function() {
          var next;
          if (this.spinePos < this.spine.length - 1) {
            next = this.spinePos + 1;
            // Skip non linear chapters
            while (this.spine[next] && this.spine[next]['linear'] && this.spine[next]['linear'] == 'no') {
              next++;
            }
            if (next < this.spine.length) {
              return this.showChapter(next);
            } else {
              this.trigger('book:atEnd');
            }

          } else {
            this.trigger('book:atEnd');
          }
        },

        prevChapter: function() {
          var prev;
          if (this.spinePos > 0) {
            prev = this.spinePos - 1;
            while (this.spine[prev] && this.spine[prev]['linear'] && this.spine[prev]['linear'] == 'no') {
              prev--;
            }
            if (prev >= 0) {
              return this.showChapter(prev, true);
            } else {
              this.trigger('book:atStart');
            }

          } else {
            this.trigger('book:atStart');
          }
        },

        destroy: function() {
          this.view.destroy();
          this._super();
        }
      });
      return Book;
    });


define('app/directives/epub-reader', ['epub/Book', 'epub/BookEvent'], function(Book, BookEvent) {

  var SOURCEATTRIBUTE = 'epubSource',
      epub;

  function Controller(_$scope, _$q, _$swipe) {
    this.$scope = _$scope;
    this.$q = _$q;
    this.$swipe = _$swipe;
    this.resetSlider();
    return this;
  }

  var proto = {

    blob: '',

    book: '',

    sliderData: {

    },

    createBook: function($el, blob) {
      if (this.book) this.destroyBook();
      this.resetSlider();

      var self = this,
          $scope = self.$scope,
          el = $el[0],
          width = el.offsetWidth,
          height = el.offsetHeight,
          start,
          bookData = $scope['bookData'],
          book = this.book = new Book(
              {
                container: el.querySelector('#ebook'),
                width: width,
                height: height - 75,
                minSpreadWidth: width,
                styles: {'padding': '0px 15px'}
              }, blob, this.$q, this.$scope),
          additionalData = {
            'totalPages': '',
            'currentPage': '',
            'ready': false
          };

      if (bookData) {
        angular.extend(bookData['book'], additionalData);
      } else {
        bookData = $scope['bookData'] = {'book': additionalData};
      }

      book.on(BookEvent.PAGECHANGED, function() {
        var percent = book.currentPage / book.totalPages;
        bookData['book']['currentPage'] = book.currentPage;
        //$scope.apply() goes in slider
        $scope['setSliderPosition'](percent);
      });

      book.on(BookEvent.VIEWCREATED, function() {
        bookData['book']['ready'] = true;
        book.view.on(BookEvent.IFRAMELOADED, function() {
          self.$swipe.bind(angular.element(book.view.window), {
            'start': function(coords) {
              start = coords;
            },
            'end': function(coords) {
              var endX = coords['x'],
                  startX = start['x'];
              if (Math.abs(endX - startX) < 10) return;
              if (endX < startX) book.nextPage(); else book.prevPage();
            }
          });
        });
      });

      book.on(BookEvent.PAGELIST, function(event, data) {
        $scope['slider']['width'] = data.value;
      });

      book.generatePages().then(
          function(book) {
            $scope['slider']['pagination'] = false;
            bookData['book']['totalPages'] = book.totalPages;
            book.show();
          }
      );

      $scope.$on('slider: position', function() {
        var pos = self.$scope['slider']['position'];
        //pos = (pos < 0) ? 0 : (pos > 1) ? 1 : pos;
        book.gotoPage(Math.floor(pos * book.totalPages));
      });

      $scope.$on('$destroy', function() {
        self.destroyBook();
      });
    },

    resetSlider: function() {
      this.sliderData = {
        'width' : 0,
        'position': 0,
        'pagination': true,
        'moving': false
      };
      this.$scope['slider'] = this.sliderData;
    },

    destroyBook: function() {
      this.book.destroy();
    }
  };

  angular.extend(Controller.prototype, proto);

  function directive() {
    return {
      templateUrl: 'epub',
      scope: true,
      link: function() {
        var a = arguments,
            $scope = a[0],
            $element = a[1],
            $attrs = a[2],
            controller = a[3];

        $scope.$watch($attrs[SOURCEATTRIBUTE], function(value) {
          if (!value) return;
          controller.createBook($element, value);
        });
      },
      controller: ['$scope', '$q', '$swipe', Controller]
    };
  }
  directive.fullName = 'epubReader';
  return directive;
});


define('app/directives/slider', [],function() {

  function Controller(_$scope, _$swipe, _$element) {
    this.$scope = _$scope;
    this.$swipe = _$swipe;
    this.$element = _$element;
    _$scope['setSliderPosition'] = this.setPosition.bind(this);
  }

  var proto = {

    _startSwipe: {},

    _startPos: 0,

    _totalWidth: 1,

    position: 0,

    init: function(_handler) {
      this.$handler = angular.element(_handler);
      this._totalWidth = this.$element[0].offsetWidth;
      this.bindListeners();
    },

    setPosition: function(pos) {
      pos = parseFloat(pos);
      var realPos = (!pos) ? 0 : (pos < 0) ? 0 : (pos > 1) ? 1 : pos,
          $scope = this.$scope;
      $scope['slider']['position'] = this.position = realPos;
      if (!$scope['$$phase']) $scope.$apply();
    },

    _sliderMove: function(coords) {
      var difference = (this._startSwipe['x'] - coords['x']) / this._totalWidth;
      this.setPosition(this._startPos - difference);
    },

    bindListeners: function() {
      var self = this,
          sliderFunc = self._sliderMove.bind(self),
          sliderEndFunc = function(coords) {
            self.$scope['slider']['moving'] = false;
            sliderFunc(coords);
            self.$scope.$emit('slider: position');
          };

      this.$swipe.bind(this.$handler, {
        'start': function(coords) {
          self._startSwipe = coords;
          self._startPos = self.position;
          self.$scope['slider']['moving'] = true;
        },
        'move': sliderFunc,
        'end': sliderEndFunc,
        'cancel': sliderEndFunc
      });
    }
  };

  angular.extend(Controller.prototype, proto);

  function directive() {
    return {
      templateUrl: 'slider',
      scope: false,
      link: function() {
        var a = arguments,
            $scope = a[0],
            $element = a[1],
            $attrs = a[2],
            controller = a[3],
            el = $element[0],
            elWidth = el.offsetWidth,
            handler = el.querySelector('.handler'),
            handlerWidth = handler.offsetWidth,
            orangeLine = el.querySelector('.viewed');

        $scope.$watch('slider.position', function() {
          var pos = controller.position * elWidth - handlerWidth / 2;
          handler.style.left = pos + 'px';
          orangeLine.style.width = pos + handlerWidth / 2 + 'px';
        });

        $scope.$watch('slider.width', function(value) {
          var width = Math.floor(parseFloat(value) * elWidth);
          $element.css({'width': width + 'px'});
        });

        $scope.$watch('slider.pagination', function(value) {
          handler.style.display = orangeLine.style.display = (value === true) ? 'none' : 'block';
        });

        controller.init(handler);

      },
      controller: ['$scope', '$swipe', '$element', Controller]
    };
  }
  directive.fullName = 'epubSlider';
  return directive;
});


define('lib/Spinner', ['lib/Class'], function(Class) {
  var prefixes = ['webkit', 'Moz', 'ms', 'O'], /* Vendor prefixes */
      animations = {}, /* Animation rules keyed by their name */
      useCssAnimations; /* Whether to use CSS animations or setTimeout */

  /**
   * Utility function to create elements. If no tag name is given,
   * a DIV is created. Optionally properties can be passed.
   */
  function createEl(tag, prop) {
    var el = document.createElement(tag || 'div'), n;
    for (n in prop) el[n] = prop[n];
    return el;
  }

  /**
   * Appends children and returns the parent.
   */
  function ins(parent /* child1, child2, ...*/) {
    for (var i = 1, n = arguments.length; i < n; i++)
      parent.appendChild(arguments[i]);
    return parent;
  }

  /**
    * Insert a new stylesheet to hold the keyframe or VML rules.
   */
  var sheet = (function() {
    var el = createEl('style', {type: 'text/css'});
    ins(document.getElementsByTagName('head')[0], el);
    return el.sheet || el.styleSheet;
  }());

  /**
   * Creates an opacity keyframe animation rule and returns its name.
   * Since most mobile Webkits have timing issues with animation-delay,
   * we create separate rules for each line/segment.
   */
  function addAnimation(alpha, trail, i, lines) {
    var name = ['opacity', trail, ~~(alpha * 100), i, lines].join('-'),
        start = 0.01 + i / lines * 100,
        z = Math.max(1 - (1 - alpha) / trail * (100 - start), alpha),
        prefix = useCssAnimations.substring(0, useCssAnimations.indexOf('Animation')).toLowerCase(),
        pre = prefix && '-' + prefix + '-' || '';

    if (!animations[name]) {
      sheet.insertRule(
          '@' + pre + 'keyframes ' + name + '{' +
          '0%{opacity:' + z + '}' +
          start + '%{opacity:' + alpha + '}' +
          (start + 0.01) + '%{opacity:1}' +
          (start + trail) % 100 + '%{opacity:' + alpha + '}' +
          '100%{opacity:' + z + '}' +
          '}', sheet.cssRules.length);

      animations[name] = 1;
    }
    return name;
  }

  /**
   * Tries various vendor prefixes and returns the first supported property.
   */
  function vendor(el, prop) {
    var s = el.style, pp, i;

    prop = prop.charAt(0).toUpperCase() + prop.slice(1);
    for (i = 0; i < prefixes.length; i++) {
      pp = prefixes[i] + prop;
      if (s[pp] !== undefined) return pp;
    }
    if (s[prop] !== undefined) return prop;
  }

  /**
   * Sets multiple style properties at once.
   */
  function css(el, prop) {
    for (var n in prop)
      el.style[vendor(el, n) || n] = prop[n];

    return el;
  }

  /**
   * Fills in default values.
   */
  function merge(obj) {
    for (var i = 1; i < arguments.length; i++) {
      var def = arguments[i];
      for (var n in def)
        if (obj[n] === undefined) obj[n] = def[n];
    }
    return obj;
  }

  /**
   * Returns the line color from the given string or array.
   */
  function getColor(color, idx) {
    return typeof color == 'string' ? color : color[idx % color.length];
  }

  // Built-in defaults

  var defaults = {
    lines: 12,            // The number of lines to draw
    length: 7,            // The length of each line
    width: 5,             // The line thickness
    radius: 10,           // The radius of the inner circle
    rotate: 0,            // Rotation offset
    corners: 1,           // Roundness (0..1)
    color: '#000',        // #rgb or #rrggbb
    direction: 1,         // 1: clockwise, -1: counterclockwise
    speed: 1,             // Rounds per second
    trail: 100,           // Afterglow percentage
    opacity: 1 / 4,         // Opacity of the lines
    fps: 20,              // Frames per second when using setTimeout()
    zIndex: 2e9,          // Use a high z-index by default
    className: 'spinner', // CSS class to assign to the element
    top: '50%',           // center vertically
    left: '50%',          // center horizontally
    position: 'absolute',  // element position
    hwacceleration: true  //hardware acceleration
  },

      Spinner = Class.create('Spinner', {
        statics: {
          defaults: {}
        },

        opts: '',

        el: '',

        constructor: function(o) {
          this.opts = angular.extend({}, o || {}, Spinner.defaults, defaults);
        },

        /**
         * Internal method that adjusts the opacity of a single line.
         * Will be overwritten in VML fallback mode below.
         */
        opacity: function(el, i, val) {
          if (i < el.childNodes.length) el.childNodes[i].style.opacity = val;
        },

        spin: function(target) {
          this.stop();

          var self = this,
              o = self.opts,
              el = self.el = css(createEl(0, {'className': o.className}), {'position': o.position, 'width': 0, 'zIndex': o.zIndex});

          css(el, {
            'left': o.left,
            'top': o.top
          });

          if (target) {
            target.insertBefore(el, target.firstChild || null);
          }

          el.setAttribute('role', 'progressbar');
          self.lines(el, self.opts);
          var parentNode = el.parentNode;
          if (parentNode) {
            parentNode.style['display'] = 'block';
          }
          return self;
        },

        stop: function() {
          var el = this.el, parentNode;
          if (el) {
            parentNode = el.parentNode;
            if (parentNode) {
              parentNode.style['display'] = 'none';
              parentNode.removeChild(el);
            }
            this.el = undefined;
          }
          return this;
        },

        lines: function(el, o) {
          var i = 0, start = (o.lines - 1) * (1 - o.direction) / 2, seg;

          function fill(color, shadow) {
            return css(createEl(), {
              position: 'absolute',
              width: (o.length + o.width) + 'px',
              height: o.width + 'px',
              background: color,
              boxShadow: shadow,
              transformOrigin: 'left',
              transform: 'rotate(' + ~~(360 / o.lines * i + o.rotate) + 'deg) translate(' + o.radius + 'px' + ',0)',
              borderRadius: (o.corners * o.width >> 1) + 'px'
            });
          }

          for (; i < o.lines; i++) {
            seg = css(createEl(), {
              position: 'absolute',
              top: 1 + ~(o.width / 2) + 'px',
              transform: o.hwacceleration ? 'translate3d(0,0,0)' : '',
              opacity: o.opacity,
              animation: useCssAnimations && addAnimation(o.opacity, o.trail, start + i * o.direction, o.lines) + ' ' + 1 / o.speed + 's linear infinite'
            });

            if (o.shadow) ins(seg, css(fill('#000', '0 0 4px ' + '#000'), {top: 2 + 'px'}));
            ins(el, ins(seg, fill(getColor(o.color, i), '0 0 1px rgba(0,0,0,.1)')));
          }
          return el;
        }
      });

  var probe = css(createEl('group'));
  useCssAnimations = vendor(probe, 'animation');

  return Spinner;
});


/**
 * angular-spinner version 0.6.2
 * License: MIT.
 * Copyright (C) 2013, 2014, 2015, Uri Shaked and contributors.
 */
define('app/modules/Spinner', ['lib/Spinner'], function(Spinner) {
  return angular
    .module('angularSpinner', [])

    .provider('SpinnerConfig', function() {
        var _config = {};
        return {
          'setDefaults': function(config) {
            _config = config || _config;
          },
          '$get': function() {
            return {
              config: _config
            };
          }
        };
      })

      .factory('SpinnerService', ['$rootScope', function($rootScope) {
        return {
          'spin': function(key) {$rootScope.$broadcast('us-spinner:spin', key);},
          'stop': function(key) {$rootScope.$broadcast('us-spinner:stop', key);}
        };
      }])

      .directive('usSpinner', ['$window', 'SpinnerConfig', function($window, usSpinnerConfig) {
        return {
          scope: true,
          link: function(scope, element, attr) {
            var SpinnerConstructor = Spinner;
            scope.spinner = null;
            scope.key = angular.isDefined(attr['spinnerKey']) ? attr['spinnerKey'] : false;
            scope.startActive = angular.isDefined(attr['spinnerStartActive']) ? scope.$eval(attr['spinnerStartActive']) : scope.key ? false : true;

            function stopSpinner() {
              if (scope.spinner) {
                scope.spinner.stop();
              }
            }

            scope.spin = function() {
              if (scope.spinner) {
                scope.spinner.spin(element[0]);
              }
            };

            scope.stop = function() {
              scope.startActive = false;
              stopSpinner();
            };

            scope.$watch(attr['usSpinner'], function(options) {
              stopSpinner();

              options = options || {};
              for (var property in usSpinnerConfig.config) {
                if (options[property] === undefined) {
                  options[property] = usSpinnerConfig.config[property];
                }
              }

              scope.spinner = new SpinnerConstructor(options);
              if (!scope.key || scope.startActive) {
                scope.spinner.spin(element[0]);
              }
            }, true);

            scope.$on('us-spinner:spin', function(event, key) {
              if (key === scope.key) {
                scope.spin();
              }
            });

            scope.$on('us-spinner:stop', function(event, key) {
              if (key === scope.key) {
                scope.stop();
              }
            });

            scope.$on('$destroy', function() {
              scope.stop();
              scope.spinner = null;
            });
          }
        };
      }]);
});


define('app/modules/api', ['app/modules/api/HttpService', 'app/modules/api/ApiService', 'app/modules/Spinner'], function(HttpService, ApiService) {
  return angular.module('api', ['angularSpinner'])
    .service(HttpService.fullName, HttpService)
    .service(ApiService.fullName, ApiService)
    .config(['$provide', function($provide) {
        $provide.decorator('$httpBackend', ['$delegate', function($delegate) {
          var proxy = function(method, url, post, callback, headers, timeout, withCredentials, responseType) {

            if (responseType !== 'blob' && responseType !== 'arraybuffer') {
              $delegate(method, url, post, callback, headers, timeout, withCredentials, responseType);
              return;
            }

            var xhr = new window.XMLHttpRequest();

            function requestError() {
              // The response is always empty
              // See https://xhr.spec.whatwg.org/#request-error-steps and https://fetch.spec.whatwg.org/#concept-network-error
              callback(-1, null, null, '');
            };

            xhr.open(method, url, true);
            angular.forEach(headers, function(value, key) {
              if (angular.isDefined(value)) {
                xhr.setRequestHeader(key, value);
              }
            });

            xhr.onload = function requestLoaded() {
              var statusText = xhr['statusText'] || '',
                  // responseText is the old-school way of retrieving response (supported by IE9)
                  // response/responseType properties were introduced in XHR Level2 spec (supported by IE10)
                  response = ('response' in xhr) ? xhr['response'] : xhr['responseText'],
                  status = xhr['status'];


              // fix status code when it is 0 (0 status is undocumented).
              // Occurs when accessing file resources or on Android 4.1 stock browser
              // while retrieving files from application cache.
              if (status === 0) {
                status = response ? 200 : 404;
              }

              callback(status, response, xhr.getAllResponseHeaders(), statusText);
            };

            xhr.onerror = requestError;
            xhr.onabort = requestError;

            if (post.onprogress) {
              xhr.onprogress = post.onprogress;
            }

            if (withCredentials) {
              xhr.withCredentials = true;
            }

            if (responseType) {
              try {
                xhr.responseType = responseType;
              } catch (e) {
                // WebKit added support for the json responseType value on 09/03/2013
                // https://bugs.webkit.org/show_bug.cgi?id=73648. Versions of Safari prior to 7 are
                // known to throw when setting the value "json" as the response type. Other older
                // browsers implementing the responseType
                //
                // The json response type can be ignored if not supported, because JSON payloads are
                // parsed on the client-side regardless.
                if (responseType !== 'json') {
                  throw e;
                }
              }
            }

            xhr.send(post);


          };
          for (var key in $delegate) {
            proxy[key] = $delegate[key];
          }
          return proxy;
        }]);
      }]);
});


define('lib/local-forage', [],function() {
  /**
   * angular-localforage - Angular service & directive for https://github.com/mozilla/localForage (Offline storage, improved.)
   * @version v1.2.3
   * @link https://github.com/ocombe/angular-localForage
   * @license MIT
   * @author olivier.combe@gmail.com (Olivier Combe)
   */
  var angularLocalForage = angular.module('LocalForageModule', ['ng']),
      localforage = window['localforage'];
  angularLocalForage.provider('$localForage', function() {
    var lfInstances = {},
        defaultConfig = {
          'name': 'lf'
        },
        // Send signals for each of the following actions ?
        notify = {
          setItem: false,
          removeItem: false
        },
        watchers = {};

    // Setter for notification config, itemSet & itemRemove should be booleans
    this.setNotify = function(itemSet, itemRemove) {
      notify = {
        setItem: itemSet,
        removeItem: itemRemove
      };
    };

    this.config = function(config) {
      if (!angular.isObject(config)) {
        throw new Error('The config parameter should be an object');
      }
      angular.extend(defaultConfig, config);
    };

    this['$get'] = ['$rootScope', '$q', '$parse', function($rootScope, $q, $parse) {

      if (!window['Promise']) window['Promise'] = $q;

      var LocalForageInstance = function LocalForageInstance(params) {
        if (angular.isDefined(params)) {
          this._localforage = localforage['createInstance'](params);
        } else {
          this._localforage = localforage;
          localforage['config'](defaultConfig);
        }
      };

      LocalForageInstance.prototype.createInstance = function createInstance(config) {
        if (angular.isObject(config)) { // create new instance
          config = angular.extend({}, defaultConfig, config);
          if (angular.isDefined(lfInstances[config['name']])) {
            throw new Error('A localForage instance with the name ' + config['name'] + ' is already defined.');
          }

          lfInstances[config['name']] = new LocalForageInstance(config);
          return lfInstances[config['name']];
        } else {throw new Error('The parameter should be a config object.')}
      };

      LocalForageInstance.prototype.instance = function instance(name) {
        if (angular.isUndefined(name)) {
          return lfInstances[defaultConfig['name']];
        } else if (angular.isString(name)) {
          if (angular.isDefined(lfInstances[name])) {
            return lfInstances[name];
          } else {
            throw new Error('No localForage instance of that name exists.')}
        } else {
          throw new Error('The parameter should be a string.')}
      };

      // Setter for the storage driver
      LocalForageInstance.prototype.setDriver = function setDriver(driver) {
        return this._localforage['setDriver'](driver);
      };

      // Getter for the storage driver
      LocalForageInstance.prototype.driver = function driver() {
        return this._localforage['driver']();
      };

      // Directly adds a value to storage
      LocalForageInstance.prototype.setItem = function setItem(key, value) {
        // throw error on undefined key, we allow undefined value because... why not ?
        if (angular.isUndefined(key)) {
          throw new Error('You must define a key to set');
        }

        var self = this;

        if (angular.isArray(key)) {
          if (!angular.isArray(value)) {
            throw new Error('If you set an array of keys, the values should be an array too');
          }

          var promises = [];
          angular.forEach(key, function(k, index) {
            promises.push(self.setItem(k, value[index]));
          });

          return $q.all(promises);
        } else {
          var deferred = $q.defer(),
              args = arguments,
              localCopy = (typeof Blob !== 'undefined' && value instanceof Blob) ||
                          (typeof ArrayBuffer !== 'undefined' && value instanceof ArrayBuffer) ? value : angular.copy(value);

          //avoid $promises attributes from value objects, if present.
          if (angular.isObject(localCopy) && angular.isDefined(localCopy['$promise'])) {
            delete localCopy['$promise']; //delete attribut from object structure.
          }

          self._localforage['setItem'](self.prefix() + key, localCopy).then(function success() {
            if (notify.setItem) {
              $rootScope.$broadcast('LocalForageModule.setItem', {
                key: key,
                newvalue: localCopy,
                driver: self.driver()
              });
            }
            deferred.resolve(localCopy);
          }, function error(data) {
            self.onError(data, args, self.setItem, deferred);
          });

          return deferred.promise;
        }
      };

      // Directly get a value from storage
      LocalForageInstance.prototype.getItem = function getItem(key) {
        // throw error on undefined key
        if (angular.isUndefined(key)) {
          throw new Error('You must define a key to get');
        }

        var deferred = $q.defer(),
            args = arguments,
            self = this,
            promise;

        if (angular.isArray(key)) {
          var res = [],
              found = 0;
          promise = self._localforage['iterate'](function(value, k) {
            var index = key.indexOf(self.prefix() + k);
            if (index > -1) {
              res[index] = value;
              found++;
            }
            if (found === key.length) {
              return res;
            }
          }).then(function() {
            deferred.resolve(res);
          });
        } else {
          promise = self._localforage['getItem'](self.prefix() + key).then(function(item) {
            deferred.resolve(item);
          });
        }

        promise.then(null, function error(data) {
          self.onError(data, args, self.getItem, deferred);
        });

        return deferred.promise;
      };

      // Iterate over all the values in storage
      LocalForageInstance.prototype.iterate = function iterate(callback) {
        // throw error on undefined key
        if (angular.isUndefined(callback)) {
          throw new Error('You must define a callback to iterate');
        }

        var deferred = $q.defer(),
            args = arguments,
            self = this;

        self._localforage['iterate'](callback).then(function success(item) {
          deferred.resolve(item);
        }, function error(data) {
          self.onError(data, args, self.iterate, deferred);
        });

        return deferred.promise;
      };

      // Remove an item from storage
      LocalForageInstance.prototype.removeItem = function removeItem(key) {
        // throw error on undefined key
        if (angular.isUndefined(key)) {
          throw new Error('You must define a key to remove');
        }

        var self = this;

        if (angular.isArray(key)) {
          var promises = [];
          angular.forEach(key, function(k, index) {
            promises.push(self.removeItem(k));
          });

          return $q.all(promises);
        } else {
          var deferred = $q.defer(),
              args = arguments;

          self._localforage['removeItem'](self.prefix() + key).then(function success() {
            if (notify.removeItem) {
              $rootScope.$broadcast('LocalForageModule.removeItem', {key: key, driver: self.driver()});
            }
            deferred.resolve();
          }, function error(data) {
            self.onError(data, args, self.removeItem, deferred);
          });

          return deferred.promise;
        }
      };

      // Get an item and removes it from storage
      LocalForageInstance.prototype.pull = function pull(key) {
        // throw error on undefined key
        if (angular.isUndefined(key)) {
          throw new Error('You must define a key to pull');
        }

        var self = this,
            deferred = $q.defer(),
            onError = function error(err) {
              deferred.reject(err);
            };

        self.getItem(key).then(function success(value) {
          self.removeItem(key).then(function success() {
            deferred.resolve(value);
          }, onError);
        }, onError);

        return deferred.promise;
      };

      // Remove all data for this app from storage
      LocalForageInstance.prototype.clear = function clear() {
        var deferred = $q.defer(),
            args = arguments,
            self = this;

        self._localforage['clear']().then(function success(keys) {
          deferred.resolve();
        }, function error(data) {
          self.onError(data, args, self.clear, deferred);
        });
        return deferred.promise;
      };

      // Return the key for item at position n
      LocalForageInstance.prototype.key = function key(n) {
        // throw error on undefined n
        if (angular.isUndefined(n)) {
          throw new Error('You must define a position to get for the key function');
        }

        var deferred = $q.defer(),
            args = arguments,
            self = this;

        self._localforage['key'](n).then(function success(key) {
          deferred.resolve(key);
        }, function error(data) {
          self.onError(data, args, self.key, deferred);
        });
        return deferred.promise;
      };

      var keys = function keys() {
        var deferred = $q.defer(),
            args = arguments,
            self = this;

        self._localforage['keys']().then(function success(keyList) {
          if (defaultConfig['oldPrefix'] && self.driver() === 'localStorageWrapper') {
            var tempKeyList = [];
            for (var i = 0, len = keyList.length; i < len; i++) {
              tempKeyList.push(keyList[i].substr(self.prefix().length, keyList[i].length));
            }
            keyList = tempKeyList;
          }
          deferred.resolve(keyList);
        }, function error(data) {
          self.onError(data, args, self.keys, deferred);
        });
        return deferred.promise;
      };

      // Return the list of keys stored for this application
      LocalForageInstance.prototype.keys = keys;

      // deprecated
      LocalForageInstance.prototype.getKeys = keys;

      // Returns the number of keys in this storage
      LocalForageInstance.prototype.length = function() {
        var deferred = $q.defer(),
            args = arguments,
            self = this;

        self._localforage['length']().then(function success(length) {
          deferred.resolve(length);
        }, function error(data) {
          self.onError(data, args, length, deferred);
        });
        return deferred.promise;
      };

      /**
       * Bind - let's you directly bind a LocalForage value to a $scope variable
       * @param {Angular $scope} $scope - the current scope you want the variable available in
       * @param {String/Object} opts - the key name of the variable you are binding OR an object with the key and custom options
       * like default value or instance name
       * Here are the available options you can set:
       * * key: the key used in storage and in the scope (if scopeKey isn't defined)
       * * defaultValue: the default value
       * * name: name of the instance that should store the data
       * * scopeKey: the key used in the scope
       * @return {*} - returns whatever the stored value is
       */
      /*LocalForageInstance.prototype.bind = function bind($scope, opts) {
        if (angular.isString(opts)) {
          opts = {
            key: opts
          };
        } else if (!angular.isObject(opts) || angular.isUndefined(opts.key)) {
          throw new Error('You must define a key to bind');
        }

        var defaultOpts = {
          defaultValue: '',
          name: defaultConfig.name
        };

        // If no defined options we use defaults otherwise extend defaults
        opts = angular.extend({}, defaultOpts, opts);

        var self = lfInstances[opts['name']];

        if (angular.isUndefined(self)) {
          throw new Error('You must use the name of an existing instance');
        }

        // Set the storeName key for the LocalForage entry
        // use user defined in specified
        var scopeKey = opts.scopeKey || opts.key,
            model = $parse(scopeKey);

        return self.getItem(opts.key).then(function(item) {
          if (item !== null) { // If it does exist assign it to the $scope value
            model.assign($scope, item);
          } else if (!angular.isUndefined(opts.defaultValue)) { // If a value doesn't already exist store it as is
            model.assign($scope, opts.defaultValue);
            self.setItem(opts.key, opts.defaultValue);
          }

          // Register a listener for changes on the $scope value
          // to update the localForage value
          if (angular.isDefined(watchers[opts.key])) {
            watchers[opts.key]();
          }

          watchers[opts.key] = $scope.$watch(scopeKey, function(val) {
            if (angular.isDefined(val)) {
              self.setItem(opts.key, val);
            }
          }, true);
          return item;
        });
      };*/

      /**
       * Unbind - let's you unbind a variable from localForage while removing the value from both
       * the localForage and the local variable and sets it to null
       * @param {String/Object} opts - the key name of the variable you are unbinding OR an object with the key and custom
       * options like default value or instance name
       * Here are the available options you can set:
       * * key: the key used in storage and in the scope (if scopeKey isn't defined)
       * * name: name of the instance that should store the data
       * * scopeKey: the key used in the scope
       */
      /*LocalForageInstance.prototype.unbind = function unbind($scope, opts) {
        if (angular.isString(opts)) {
          opts = {
            key: opts
          };
        } else if (!angular.isObject(opts) || angular.isUndefined(opts.key)) {
          throw new Error('You must define a key to unbind');
        }

        var defaultOpts = {
          scopeKey: opts.key,
          name: defaultConfig.name
        };

        // If no defined options we use defaults otherwise extend defaults
        opts = angular.extend({}, defaultOpts, opts);

        var self = lfInstances[opts.name];

        if (angular.isUndefined(self)) {
          throw new Error('You must use the name of an existing instance');
        }

        $parse(opts.scopeKey).assign($scope, null);
        if (angular.isDefined(watchers[opts.key])) {
          watchers[opts.key](); // unwatch
          delete watchers[opts.key];
        }
        return self.removeItem(opts.key);
      };
*/
      LocalForageInstance.prototype.prefix = function() {
        return this.driver() === 'localStorageWrapper' && defaultConfig['oldPrefix'] ? this._localforage['config']()['name'] + '.' : '';
      };

      // Handling errors
      LocalForageInstance.prototype.onError = function(err, args, fct, deferred) {
        // test for private browsing errors in Firefox & Safari
        if (((angular.isObject(err) && err['name'] ? err['name'] === 'InvalidStateError' : (angular.isString(err) &&
            err === 'InvalidStateError')) && this.driver() === 'asyncStorage') ||
            (angular.isObject(err) && err['code'] && err['code'] === 5)) {

          var self = this;

          self.setDriver('localStorageWrapper').then(function() {
            fct.apply(self, args).then(function(item) {
              deferred.resolve(item);
            }, function(data) {
              deferred.reject(data);
            });
          }, function() {
            deferred.reject(err);
          });
        } else {
          deferred.reject(err);
        }
      };

      lfInstances[defaultConfig['name']] = new LocalForageInstance();
      return lfInstances[defaultConfig['name']];
    }];
  });
/*
  angularLocalForage.directive('localForage', ['$localForage', function($localForage) {
    return {
      restrict: 'A',
      link: function($scope, $element, $attrs) {
        var opts = $scope.$eval($attrs['localForage']);
        if (angular.isObject(opts) && angular.isDefined(opts.key)) {
          $localForage.bind($scope, opts);
        } else {
          $localForage.bind($scope, $attrs['localForage']);
        }
      }
    }}]);
    */
  return angularLocalForage;
});


define('lib/ui-scroll', [],function() {
  /*!
   * angular-ui-scroll
   * https://github.com/angular-ui/ui-scroll.git
   * Version: 1.3.1 -- 2015-08-05T13:39:04.079Z
   * License: MIT
   */

  /*!
     globals: angular, window

     List of used element methods available in JQuery but not in JQuery Lite

     element.before(elem)
     element.height()
     element.outerHeight(true)
     element.height(value) = only for Top/Bottom padding elements
     element.scrollTop()
     element.scrollTop(value)
     */
  var uiScroll = angular.module('ui.scroll', []).directive('uiScrollViewport', function() {
    return {
      controller: [
        '$scope', '$element', function(scope, element) {
          this.viewport = element;
          return this;
        }
      ]
    };
  }).directive('uiScroll', [
    '$log', '$injector', '$rootScope', '$timeout', '$q', '$parse', function(console, $injector, $rootScope, $timeout, $q, $parse) {
      var $animate;
      if ($injector.has && $injector.has('$animate')) {
        $animate = $injector.get('$animate');
      }
      return {
        require: ['?^uiScrollViewport'],

        transclude: 'element',

        priority: 1000,

        terminal: true,

        compile: function(elementTemplate, attr, linker) {
          return function($scope, element, $attr, controllers) {
            var adapter,
                adapterOnScope,
                adjustBuffer,
                applyUpdate,
                bof,
                bottomVisiblePos,
                buffer,
                bufferPadding,
                bufferSize,
                builder,
                clipBottom,
                clipTop,
                datasource,
                datasourceName,
                dismissPendingRequests,
                enqueueFetch,
                eof,
                eventListener,
                fetch,
                finalize,
                first,
                insertElement,
                insertElementAnimated,
                insertItem,
                isDatasourceValid,
                itemName,
                loading,
                log,
                match,
                next,
                pending,
                reload,
                removeFromBuffer,
                removeItem,
                resizeAndScrollHandler,
                ridActual,
                scrollHeight,
                shouldLoadBottom,
                shouldLoadTop,
                topVisible,
                topVisiblePos,
                unsupportedMethod,
                viewport,
                viewportScope,
                wheelHandler;

            builder = '';
            ridActual = 0;
            first = 1;
            next = 1;
            buffer = [];
            pending = [];
            eof = false;
            bof = false;

            if (!(match = $attr['uiScroll'].match(/^\s*(\w+)\s+in\s+([\w\.]+)\s*$/))) {
              throw new Error('Expected uiScroll in form of \'_item_ in _datasource_\' but got \' + $attr.uiScroll + \'');
            }
            itemName = match[1];
            datasourceName = match[2];
            datasource = $parse(datasourceName)($scope);

            isDatasourceValid = function() {
              return angular.isObject(datasource) && angular.isFunction(datasource['get']);
            };

            if (!isDatasourceValid()) {
              datasource = $injector.get(datasourceName);
              if (!isDatasourceValid()) {
                throw new Error(datasourceName + ' is not a valid datasource');
              }
            }

            bufferSize = Math.max(3, +$attr['bufferSize'] || 10);

            bufferPadding = function() {
              return viewport.outerHeight() * Math.max(0.1, +$attr['padding'] || 0.1);
            };

            scrollHeight = function(elem) {
              var ref = elem[0].scrollHeight;
              return (ref) != null ? ref : elem[0].document.documentElement.scrollHeight;
            };

            removeItem = $animate ?
                function(wrapper) {
                  buffer.splice(buffer.indexOf(wrapper), 1);
                  return [
                    ($animate['leave'](wrapper.element)).then(function() {
                      return wrapper.scope.$destroy();
                    })
                  ];
                } :
                function(wrapper) {
                  buffer.splice(buffer.indexOf(wrapper), 1);
                  wrapper.element.remove();
                  wrapper.scope.$destroy();
                  return [];
                };

            insertElement = function(newElement, previousElement) {
              element.after.apply(previousElement, newElement);
              return [];
            };

            insertElementAnimated = $animate ?
                function(newElement, previousElement) {
                  return [$animate.enter(newElement, element, previousElement)];
                } : insertElement;

            linker($scope.$new(), function(template) {
              var bottomPadding,
                  padding,
                  repeaterType,
                  topPadding,
                  viewport;

              repeaterType = template[0].localName;

              if (repeaterType === 'dl') {
                throw new Error('ui-scroll directive does not support <' + template[0].localName + '> as a repeating tag: ' + template[0].outerHTML);
              }

              if (repeaterType !== 'li' && repeaterType !== 'tr') {
                repeaterType = 'div';
              }

              viewport = controllers[0] && controllers[0].viewport ? controllers[0].viewport : angular.element(window);

              viewport.css({
                'overflow-y': 'auto',
                'display': 'block'
              });

              padding = function(repeaterType) {
                var div, result, table;
                switch (repeaterType) {
                  case 'tr':
                    table = angular.element('<table><tr><td><div></div></td></tr></table>');
                    div = table.find('div');
                    result = table.find('tr');
                    result.paddingHeight = function() {
                      return div.height.apply(div, arguments);
                    };
                    break;
                  default:
                    result = angular.element('<' + repeaterType + '></' + repeaterType + '>');
                    result.paddingHeight = result.height;
                }
                return result;
              };

              topPadding = padding(repeaterType);

              element.before(topPadding);

              bottomPadding = padding(repeaterType);

              element.after(bottomPadding);

              $scope.$on('$destroy', function() {
                return template.remove();
              });

              return builder = {
                viewport: viewport,

                topPadding: function() {
                  return topPadding.paddingHeight.apply(topPadding, arguments);
                },

                bottomPadding: function() {
                  return bottomPadding.paddingHeight.apply(bottomPadding, arguments);
                },

                bottomDataPos: function() {
                  return scrollHeight(viewport) - bottomPadding.paddingHeight();
                },

                topDataPos: function() {
                  return topPadding.paddingHeight();
                },

                insertElement: function(e, sibling) {
                  return insertElement(e, sibling || topPadding);
                },

                insertElementAnimated: function(e, sibling) {
                  return insertElementAnimated(e, sibling || topPadding);
                }
              };
            });

            viewport = builder.viewport;
            viewportScope = viewport.scope() || $rootScope;

            topVisible = function(item) {
              adapter.topVisible = item.scope[itemName];
              adapter.topVisibleElement = item.element;
              adapter.topVisibleScope = item.scope;
              if ($attr['topVisible']) {
                $parse($attr['topVisible']).assign(viewportScope, adapter.topVisible);
              }
              if ($attr['topVisibleElement']) {
                $parse($attr['topVisibleElement']).assign(viewportScope, adapter.topVisibleElement);
              }
              if ($attr['topVisibleScope']) {
                $parse($attr['topVisibleScope']).assign(viewportScope, adapter.topVisibleScope);
              }
              if (angular.isFunction(datasource.topVisible)) {
                return datasource.topVisible(item);
              }
            };

            loading = function(value) {
              adapter.isLoading = value;
              if ($attr['isLoading']) {
                $parse($attr['isLoading']).assign($scope, value);
              }
              if (angular.isFunction(datasource.loading)) {
                return datasource.loading(value);
              }
            };

            removeFromBuffer = function(start, stop) {
              var i, j, ref, ref1;
              for (i = j = ref = start, ref1 = stop; ref <= ref1 ? j < ref1 : j > ref1; i = ref <= ref1 ? ++j : --j) {
                buffer[i].scope.$destroy();
                buffer[i].element.remove();
              }
              return buffer.splice(start, stop - start);
            };

            dismissPendingRequests = function() {
              ridActual++;
              return pending = [];
            };

            reload = function() {
              dismissPendingRequests();
              first = 1;
              next = 1;
              removeFromBuffer(0, buffer.length);
              builder.topPadding(0);
              builder.bottomPadding(0);
              eof = false;
              bof = false;
              return adjustBuffer(ridActual);
            };

            bottomVisiblePos = function() {
              return viewport.scrollTop() + viewport.outerHeight();
            };

            topVisiblePos = function() {
              return viewport.scrollTop();
            };

            shouldLoadBottom = function() {
              return !eof && builder.bottomDataPos() < bottomVisiblePos() + bufferPadding();
            };

            clipBottom = function() {
              var bottomHeight, i, item, itemHeight, itemTop, j, newRow, overage, ref, rowTop;
              bottomHeight = 0;
              overage = 0;
              for (i = j = ref = buffer.length - 1; ref <= 0 ? j <= 0 : j >= 0; i = ref <= 0 ? ++j : --j) {
                item = buffer[i];
                itemTop = item.element.offset().top;
                newRow = rowTop !== itemTop;
                rowTop = itemTop;
                if (newRow) {
                  itemHeight = item.element.outerHeight(true);
                }
                if (builder.bottomDataPos() - bottomHeight - itemHeight > bottomVisiblePos() + bufferPadding()) {
                  if (newRow) {
                    bottomHeight += itemHeight;
                  }
                  overage++;
                  eof = false;
                } else {
                  if (newRow) {
                    break;
                  }
                  overage++;
                }
              }
              if (overage > 0) {
                builder.bottomPadding(builder.bottomPadding() + bottomHeight);
                removeFromBuffer(buffer.length - overage, buffer.length);
                return next -= overage;
              }
            };

            shouldLoadTop = function() {
              return !bof && (builder.topDataPos() > topVisiblePos() - bufferPadding());
            };

            clipTop = function() {
              var item, itemHeight, itemTop, j, len, newRow, overage, rowTop, topHeight;
              topHeight = 0;
              overage = 0;
              for (j = 0, len = buffer.length; j < len; j++) {
                item = buffer[j];
                itemTop = item.element.offset().top;
                newRow = rowTop !== itemTop;
                rowTop = itemTop;
                if (newRow) {
                  itemHeight = item.element.outerHeight(true);
                }
                if (builder.topDataPos() + topHeight + itemHeight < topVisiblePos() - bufferPadding()) {
                  if (newRow) {
                    topHeight += itemHeight;
                  }
                  overage++;
                  bof = false;
                } else {
                  if (newRow) {
                    break;
                  }
                  overage++;
                }
              }
              if (overage > 0) {
                builder.topPadding(builder.topPadding() + topHeight);
                removeFromBuffer(0, overage);
                return first += overage;
              }
            };

            enqueueFetch = function(rid, direction) {
              if (!adapter.isLoading) {
                loading(true);
              }
              if (pending.push(direction) === 1) {
                return fetch(rid);
              }
            };

            insertItem = function(operation, item) {
              var itemScope, wrapper;
              itemScope = $scope.$new();
              itemScope[itemName] = item;
              wrapper = {
                scope: itemScope
              };
              linker(itemScope, function(clone) {
                return wrapper.element = clone;
              });
              if (operation % 1 === 0) {
                wrapper.op = 'insert';
                return buffer.splice(operation, 0, wrapper);
              } else {
                wrapper.op = operation;
                switch (operation) {
                  case 'append':
                    return buffer.push(wrapper);
                  case 'prepend':
                    return buffer.unshift(wrapper);
                }
              }
            };

            adjustBuffer = function(rid, finalize) {
              var promises, toBePrepended, toBeRemoved;
              promises = [];
              toBePrepended = [];
              toBeRemoved = [];
              return $timeout(function() {
                var bottomPos, heightIncrement, i, item, itemHeight, itemTop, j, k, l, len, len1, len2, len3, len4, m, n, newRow, rowTop, topHeight, wrapper;
                bottomPos = builder.bottomDataPos();
                for (i = j = 0, len = buffer.length; j < len; i = ++j) {
                  wrapper = buffer[i];
                  switch (wrapper.op) {
                    case 'prepend':
                      toBePrepended.unshift(wrapper);
                      break;
                    case 'append':
                      if (i === 0) {
                        builder.insertElement(wrapper.element);
                      } else {
                        builder.insertElement(wrapper.element, buffer[i - 1].element);
                      }
                      wrapper.op = 'none';
                      break;
                    case 'insert':
                      if (i === 0) {
                        promises = promises.concat(builder.insertElementAnimated(wrapper.element));
                      } else {
                        promises = promises.concat(builder.insertElementAnimated(wrapper.element, buffer[i - 1].element));
                      }
                      wrapper.op = 'none';
                      break;
                    case 'remove':
                      toBeRemoved.push(wrapper);
                  }
                }
                for (k = 0, len1 = toBeRemoved.length; k < len1; k++) {
                  wrapper = toBeRemoved[k];
                  promises = promises.concat(removeItem(wrapper));
                }
                builder.bottomPadding(Math.max(0, builder.bottomPadding() - (builder.bottomDataPos() - bottomPos)));
                if (toBePrepended.length) {
                  bottomPos = builder.bottomDataPos();
                  for (l = 0, len2 = toBePrepended.length; l < len2; l++) {
                    wrapper = toBePrepended[l];
                    builder.insertElement(wrapper.element);
                    wrapper.op = 'none';
                  }
                  heightIncrement = builder.bottomDataPos() - bottomPos;
                  if (builder.topPadding() >= heightIncrement) {
                    builder.topPadding(builder.topPadding() - heightIncrement);
                  } else {
                    //viewport.scrollTop(viewport.scrollTop() + heightIncrement);
                  }
                }
                for (i = m = 0, len3 = buffer.length; m < len3; i = ++m) {
                  item = buffer[i];
                  item.scope.$index = first + i;
                }
                if (shouldLoadBottom()) {
                  enqueueFetch(rid, true);
                } else {
                  if (shouldLoadTop()) {
                    enqueueFetch(rid, false);
                  }
                }
                if (finalize) {
                  finalize(rid);
                }
                if (pending.length === 0) {
                  topHeight = 0;
                  for (n = 0, len4 = buffer.length; n < len4; n++) {
                    item = buffer[n];
                    itemTop = item.element.offset().top;
                    newRow = rowTop !== itemTop;
                    rowTop = itemTop;
                    if (newRow) {
                      itemHeight = item.element.outerHeight(true);
                    }
                    if (newRow && (builder.topDataPos() + topHeight + itemHeight < topVisiblePos())) {
                      topHeight += itemHeight;
                    } else {
                      if (newRow) {
                        topVisible(item);
                      }
                      break;
                    }
                  }
                }
                if (promises.length) {
                  return $q.all(promises).then(function() {
                    return adjustBuffer(rid);
                  });
                }
              });
            };

            finalize = function(rid) {
              return adjustBuffer(rid, function() {
                pending.shift();
                if (pending.length === 0) {
                  return loading(false);
                } else {
                  return fetch(rid);
                }
              });
            };

            fetch = function(rid) {
              if (pending[0]) {
                if (buffer.length && !shouldLoadBottom()) {
                  return finalize(rid);
                } else {
                  return datasource.get(next, bufferSize, function(result) {
                    var item, j, len;
                    if ((rid && rid !== ridActual) || $scope['$$destroyed']) {
                      return;
                    }
                    if (result.length < bufferSize) {
                      eof = true;
                      builder.bottomPadding(0);
                    }
                    if (result.length > 0) {
                      clipTop();
                      for (j = 0, len = result.length; j < len; j++) {
                        item = result[j];
                        ++next;
                        insertItem('append', item);
                      }
                    }
                    return finalize(rid);
                  });
                }
              } else {
                if (buffer.length && !shouldLoadTop()) {
                  return finalize(rid);
                } else {
                  return datasource.get(first - bufferSize, bufferSize, function(result) {
                    var i, j, ref;
                    if ((rid && rid !== ridActual) || $scope['$$destroyed']) {
                      return;
                    }
                    if (result.length < bufferSize) {
                      bof = true;
                      builder.topPadding(0);
                    }
                    if (result.length > 0) {
                      if (buffer.length) {
                        clipBottom();
                      }
                      for (i = j = ref = result.length - 1; ref <= 0 ? j <= 0 : j >= 0; i = ref <= 0 ? ++j : --j) {
                        --first;
                        insertItem('prepend', result[i]);
                      }
                    }
                    return finalize(rid);
                  });
                }
              }
            };

            resizeAndScrollHandler = function() {
              if (!$rootScope['$$phase'] && !adapter.isLoading) {
                adjustBuffer();
                return $scope.$apply();
              }
            };

            wheelHandler = function(event) {
              var scrollTop, yMax;
              scrollTop = viewport[0].scrollTop;
              yMax = viewport[0].scrollHeight - viewport[0].clientHeight;
              if ((scrollTop === 0 && !bof) || (scrollTop === yMax && !eof)) {
                return event.preventDefault();
              }
            };

            viewport.bind('resize', resizeAndScrollHandler);
            viewport.bind('scroll', resizeAndScrollHandler);
            viewport.bind('mousewheel', wheelHandler);

            $scope.$watch(datasource.revision, reload);

            $scope.$on('$destroy', function() {
              var item, j, len;
              for (j = 0, len = buffer.length; j < len; j++) {
                item = buffer[j];
                item.scope.$destroy();
                item.element.remove();
              }
              viewport.unbind('resize', resizeAndScrollHandler);
              viewport.unbind('scroll', resizeAndScrollHandler);
              return viewport.unbind('mousewheel', wheelHandler);
            });

            adapter = {
              isLoading: false,

              reload: reload,

              applyUpdate: function(wrapper, newItems) {
                var i,
                    j,
                    keepIt,
                    len,
                    newItem,
                    pos,
                    ref;

                if (angular.isArray(newItems)) {
                  pos = (buffer.indexOf(wrapper)) + 1;
                  ref = newItems.reverse();
                  for (i = j = 0, len = ref.length; j < len; i = ++j) {
                    newItem = ref[i];
                    if (newItem === wrapper.scope[itemName]) {
                      keepIt = true;
                      pos--;
                    } else {
                      insertItem(pos, newItem);
                    }
                  }
                  if (!keepIt) {
                    return wrapper.op = 'remove';
                  }
                }
              },

              applyUpdates: function(arg1, arg2) {
                var bufferClone,
                    i,
                    j,
                    len,
                    ref,
                    wrapper;

                dismissPendingRequests();

                if (angular.isFunction(arg1)) {
                  bufferClone = buffer.slice(0);
                  for (i = j = 0, len = bufferClone.length; j < len; i = ++j) {
                    wrapper = bufferClone[i];
                    applyUpdate(wrapper, arg1(wrapper.scope[itemName], wrapper.scope, wrapper.element));
                  }
                } else {
                  if (arg1 % 1 === 0) {
                    if ((0 <= (ref = arg1 - first) && ref < buffer.length)) {
                      applyUpdate(buffer[arg1 - first], arg2);
                    }
                  } else {
                    throw new Error('applyUpdates - ' + arg1 + ' is not a valid index');
                  }
                }
                return adjustBuffer(ridActual);
              },

              append: function(newItems) {
                var item,
                    j,
                    len;

                dismissPendingRequests();
                for (j = 0, len = newItems.length; j < len; j++) {
                  item = newItems[j];
                  ++next;
                  insertItem('append', item);
                }
                return adjustBuffer(ridActual);
              },

              prepend: function(newItems) {
                var item,
                    j,
                    len,
                    ref;

                dismissPendingRequests();
                ref = newItems.reverse();
                for (j = 0, len = ref.length; j < len; j++) {
                  item = ref[j];
                  --first;
                  insertItem('prepend', item);
                }
                return adjustBuffer(ridActual);
              }
            };

            if ($attr['adapter']) {
              adapterOnScope = $parse($attr['adapter'])($scope);
              if (!adapterOnScope) {
                $parse($attr['adapter']).assign($scope, {});
                adapterOnScope = $parse($attr['adapter'])($scope);
              }
              angular.extend(adapterOnScope, adapter);
              adapter = adapterOnScope;
            }

            datasource.adapter = adapter;

            unsupportedMethod = function(token) {
              throw new Error(token + ' event is no longer supported - use applyUpdates instead');
            };

            eventListener = datasource.scope ? datasource.scope.$new() : $scope.$new();

            eventListener.$on('insert.item', function() {
              return unsupportedMethod('insert');
            });

            eventListener.$on('update.items', function() {
              return unsupportedMethod('update');
            });

            return eventListener.$on('delete.items', function() {
              return unsupportedMethod('delete');
            });
          };
        }
      };
    }
  ]);

  return uiScroll;
});

/**
 * angular-route-segment 1.5.0
 * https://angular-route-segment.com
 * @author Artem Chivchalov
 * @license MIT License http://opensource.org/licenses/MIT
 */

define('lib/RouteSegment', [],function() {
  var mod = angular.module('route-segment', []);
  mod.provider('$routeSegment', ['$routeProvider',
    function($routeProvider) {
      var $routeSegmentProvider = this,
          options = $routeSegmentProvider.options = {

            /**
            * When true, it will resolve `templateUrl` automatically via $http service and put its
            * contents into `template`.
            * @type {boolean}
            */
            autoLoadTemplates: true,

            /**
             * When true, all attempts to call `within` method on non-existing segments will throw an error (you would
             * usually want this behavior in production). When false, it will transparently create new empty segment
             * (can be useful in isolated tests).
             * @type {boolean}
             */
            strictMode: false
          },

          segments = this.segments = {},
          rootPointer = pointer(segments, null),
          segmentRoutes = {};

      function camelCase(name) {
        return name.replace(/([\:\-\_]+(.))/g, function(_, separator, letter, offset) {
          return offset ? letter.toUpperCase() : letter;
        });
      }

      function pointer(segment, parent) {
        if (!segment)
          throw new Error('Invalid pointer segment');

        var lastAddedName;

        return {
          /**
           * Adds new segment at current pointer level.
           *
           * @param {string} name Name of a segment.
           * @param {Object} params Segment's parameters hash. The following params are supported:
           *                        - `template` provides HTML for the given segment view;
           *                        - `templateUrl` is a template should be fetched from network via this URL;
           *                        - `controller` is attached to the given segment view when compiled and linked,
           *                              this can be any controller definition AngularJS supports;
           *                        - `dependencies` is an array of route param names which are forcing the view
           *                              to recreate when changed;
           *                        - `watcher` is a $watch-function for recreating the view when its returning value
           *                              is changed;
           *                        - `resolve` is a hash of functions or injectable names which should be resolved
           *                              prior to instantiating the template and the controller;
           *                        - `untilResolved` is the alternate set of params (e.g. `template` and `controller`)
           *                              which should be used before resolving is completed;
           *                        - `resolveFailed` is the alternate set of params which should be used
           *                              if resolving failed;
           *
           * @return {Object} The same level pointer.
           */
          segment: function(name, params) {
            segment[camelCase(name)] = {name: name, params: params};
            lastAddedName = name;
            return this;
          },

          /**
           * Traverses into an existing segment, so that subsequent `segment` calls
           * will add new segments as its descendants.
           *
           * @param {string} childName An existing segment's name. If undefined, then the last added segment is selected.
           * @return {Object} The pointer to the child segment.
           */
          within: function(childName) {
            var child;
            childName = childName || lastAddedName;

            if (child = segment[camelCase(childName)]) {
              if (child.children == undefined)
                child.children = {};
            }
            else {
              if (options.strictMode)
                throw new Error('Cannot get into unknown `' + childName + '` segment');
              else {
                child = segment[camelCase(childName)] = {params: {}, children: {}};
              }
            }
            return pointer(child.children, this);
          },

          /**
           * Traverses up in the tree.
           * @return {Object} The pointer which are parent to the current one;
           */
          up: function() {
            return parent;
          },

          /**
           * Traverses to the root.
           */
          root: function() {
            return rootPointer;
          }
        };
      }

      /**
       * The shorthand for $routeProvider.when() method with specified route name.
       * @param {string} path Route URL, e.g. '/foo/bar'
       * @param {string} name Fully qualified route name, e.g. 'foo.bar'
       * @param {Object} route Mapping information to be assigned to $route.current on route match.
       */
      $routeSegmentProvider.when = function(path, name, route) {
        if (route == undefined)
          route = {};
        route.segment = name;

        $routeProvider.when(path, route);
        segmentRoutes[name] = path;
        return this;
      };

      // Extending the provider with the methods of rootPointer
      // to start configuration.
      angular.extend($routeSegmentProvider, rootPointer);


      // the service factory
      this.$get = ['$rootScope', '$q', '$http', '$templateCache', '$route', '$routeParams', '$injector',
        function($rootScope, $q, $http, $templateCache, $route, $routeParams, $injector) {

          var $routeSegment = {
            /**
             * Fully qualified name of current active route
             * @type {string}
             */
            name: '',

            /**
             * A copy of `$routeParams` in its state of the latest successful segment update. It may be not equal
             * to `$routeParams` while some resolving is not completed yet. Should be used instead of original
             * `$routeParams` in most cases.
             * @type {Object}
             */
            $routeParams: angular.copy($routeParams),

            /**
             * Array of segments splitted by each level separately. Each item contains the following properties:
             * - `name` is the name of a segment;
             * - `params` is the config params hash of a segment;
             * - `locals` is a hash which contains resolve results if any;
             * - `reload` is a function to reload a segment (restart resolving, reinstantiate a controller, etc)
             *
             * @type {Array.<Object>}
             */
            chain: [],

            /**
             * Helper method for checking whether current route starts with the given string
             * @param {string} val
             * @return {boolean}
             */
            startsWith: function(val) {
              var regexp = new RegExp('^' + val);
              return regexp.test($routeSegment.name);
            },

            /**
             * Helper method for checking whether current route contains the given string
             * @param {string} val
             * @return {Boolean}
             */
            contains: function(val) {
              for (var i = 0; i < this.chain.length; i++) {
                if (this.chain[i] && this.chain[i].name == val) return true;
              }
              return false;
            },

            /**
             * A method for reverse routing which can return the route URL for the specified segment name
             * @param {string} segmentName The name of a segment as defined in `when()`
             * @param {Object} routeParams Route params hash to be put into route URL template
             */
            getSegmentUrl: function(segmentName, routeParams) {
              var url, i, m;
              if (!segmentRoutes[segmentName])
                throw new Error('Can not get URL for segment with name `' + segmentName + '`');

              routeParams = angular.extend({}, $routeParams, routeParams || {});

              url = segmentRoutes[segmentName];
              for (i in routeParams) {
                var regexp = new RegExp('\:'+i+'[\*\?]?','g');
                url = url.replace(regexp, routeParams[i]);
              }
              url = url.replace(/\/\:.*?\?/g, '');

              if (m = url.match(/\/\:([^\/]*)/))
                throw new Error('Route param `' + m[1] + '` is not specified for route `' + segmentRoutes[segmentName] + '`');

              return url;
            }
          };

          var resolvingSemaphoreChain = {};

          // When a route changes, all interested parties should be notified about new segment chain
          $rootScope.$on('$routeChangeSuccess', function(event, args) {

            var route = args.$route || args['$$route'];
            if (route && route.segment) {

              var segmentName = route.segment,
                  segmentNameChain = segmentName.split('.'),
                  updates = [], lastUpdateIndex = -1;

              for (/*var */i = 0; i < segmentNameChain.length; i++) {

                var newSegment = getSegmentInChain(i, segmentNameChain);

                if (resolvingSemaphoreChain[i] != newSegment.name || updates.length > 0 || isDependenciesChanged(newSegment)) {

                  if ($routeSegment.chain[i] && $routeSegment.chain[i].name == newSegment.name &&
                      updates.length == 0 && !isDependenciesChanged(newSegment))
                    // if we went back to the same state as we were before resolving new segment
                    resolvingSemaphoreChain[i] = newSegment.name;
                  else {
                    updates.push({index: i, newSegment: newSegment});
                    lastUpdateIndex = i;
                  }
                }
              }

              var curSegmentPromise = $q.when();

              if (updates.length > 0) {
                for (var i = 0; i < updates.length; i++) {
                  (function(i) {
                    curSegmentPromise = curSegmentPromise.then(function() {
                      return updateSegment(updates[i].index, updates[i].newSegment);
                    }).then(function(result) {
                      if (result.success != undefined) {
                        broadcast(result.success);
                        for (var j = updates[i].index + 1; j < $routeSegment.chain.length; j++) {
                          if ($routeSegment.chain[j]) {
                            $routeSegment.chain[j] = null;
                            updateSegment(j, null);
                          }
                        }
                      }
                    });
                  })(i);
                }
              }
              curSegmentPromise.then(function() {

                // Removing redundant segment in case if new segment chain is shorter than old one
                if ($routeSegment.chain.length > segmentNameChain.length) {
                  var oldLength = $routeSegment.chain.length;
                  var shortenBy = $routeSegment.chain.length - segmentNameChain.length;
                  $routeSegment.chain.splice(-shortenBy, shortenBy);
                  for (var i = segmentNameChain.length; i < oldLength; i++) {
                    updateSegment(i, null);
                    lastUpdateIndex = $routeSegment.chain.length - 1;
                  }
                }
              }).then(function() {
                var defaultChildUpdatePromise = $q.when();
                if (lastUpdateIndex == $routeSegment.chain.length - 1) {
                  var curSegment = getSegmentInChain(lastUpdateIndex, $routeSegment.name.split('.'));

                  while (curSegment) {
                    var children = curSegment.children, index = lastUpdateIndex + 1;
                    curSegment = null;
                    for (var i in children) {
                      (function(i, children, index) {
                        if (children[i].params['default']) {
                          defaultChildUpdatePromise = defaultChildUpdatePromise.then(function() {
                            return updateSegment(index, {name: children[i].name, params: children[i].params})
                              .then(function(result) {
                                  if (result.success) broadcast(result.success);
                                });
                          });
                          curSegment = children[i];
                          lastUpdateIndex = index;
                        }
                      })(i, children, index);
                    }
                  }
                }

                return defaultChildUpdatePromise;
              });
            }
          });

          function isDependenciesChanged(segment) {

            var result = false;
            if (segment.params.dependencies)
              angular.forEach(segment.params.dependencies, function(name) {
                if (!angular.equals($routeSegment.$routeParams[name], $routeParams[name]))
                  result = true;
              });
            return result;
          }

          function updateSegment(index, segment) {

            if ($routeSegment.chain[index] && $routeSegment.chain[index].clearWatcher) {
              $routeSegment.chain[index].clearWatcher();
            }

            if (!segment) {
              resolvingSemaphoreChain[index] = null;
              broadcast(index);
              return;
            }

            resolvingSemaphoreChain[index] = segment.name;

            if (segment.params && segment.params.untilResolved) {
              return resolve(index, segment.name, segment.params.untilResolved)
                .then(function(result) {
                    if (result.success != undefined)
                      broadcast(index);
                    return resolve(index, segment.name, segment.params);
                  });
            }
            else
              return resolve(index, segment.name, segment.params);
          }

          function resolve(index, name, params) {
            var locals = params && params['resolve'] ? angular.extend({}, params['resolve']) : {};
            angular.forEach(locals, function(value, key) {
              locals[key] = angular.isString(value) ? $injector.get(value) : $injector.invoke(value);
            });

            if (params && params.template) {
              locals.$template = params.template;
              if (angular.isFunction(locals.$template))
                locals.$template = $injector.invoke(locals.$template);
            }

            if (options.autoLoadTemplates && params && params.templateUrl) {
              locals.$template = params.templateUrl;
              if (angular.isFunction(locals.$template))
                locals.$template = $injector.invoke(locals.$template);
              locals.$template =
                  $http.get(locals.$template, {cache: $templateCache})
                    .then(function(response) {
                    return response.data;
                  });
            }

            return $q.all(locals).then(
                function(resolvedLocals) {
                  if (resolvingSemaphoreChain[index] != name)
                    return $q.reject();

                  $routeSegment.chain[index] = {
                    name: name,
                    params: params,
                    locals: resolvedLocals,
                    reload: function() {
                      var originalSegment = getSegmentInChain(index, $routeSegment.name.split('.'));
                      updateSegment(index, originalSegment).then(function(result) {
                        if (result.success != undefined)
                          broadcast(index);
                      });
                    }
                  };

                  if (params && params['watcher']) {

                    var getWatcherValue = function() {
                      if (!angular.isFunction(params['watcher']) && !angular.isArray(params['watcher']))
                        throw new Error('Watcher is not a function in segment `' + name + '`');

                      return $injector.invoke(
                          params['watcher'],
                          {},
                          {segment: $routeSegment.chain[index]});
                    };

                    var lastWatcherValue = getWatcherValue();

                    $routeSegment.chain[index].clearWatcher = $rootScope.$watch(
                        getWatcherValue,
                        function(value) {
                          if (value == lastWatcherValue) // should not being run when $digest-ing at first time
                            return;
                          lastWatcherValue = value;
                          $routeSegment.chain[index].reload();
                        });
                  }

                  return {success: index};
                },

                function(error) {

                  if (params && params['resolveFailed']) {
                    var newResolve = {error: function() { return $q.when(error); }};
                    return resolve(index, name, angular.extend({'resolve': newResolve}, params['resolveFailed']));
                  }
                  else
                    throw new Error('Resolving failed with a reason `' + error + '`, but no `resolveFailed` ' +
                        'provided for segment `' + name + '`');
                });
          }

          function broadcast(index) {
            $routeSegment.$routeParams = angular.copy($routeParams);

            $routeSegment.name = '';
            for (var i = 0; i < $routeSegment.chain.length; i++)
              if ($routeSegment.chain[i])
                $routeSegment.name += $routeSegment.chain[i].name + '.';
              $routeSegment.name = $routeSegment.name.substr(0, $routeSegment.name.length - 1);

            $rootScope.$broadcast('routeSegmentChange', {
              index: index,
              segment: $routeSegment.chain[index] || null });
          }

          function getSegmentInChain(segmentIdx, segmentNameChain) {

            if (!segmentNameChain)
              return null;

            if (segmentIdx >= segmentNameChain.length)
              return null;

            var curSegment = segments, nextName;
            for (var i = 0; i <= segmentIdx; i++) {

              nextName = segmentNameChain[i];

              if (curSegment[camelCase(nextName)] != undefined)
                curSegment = curSegment[camelCase(nextName)];

              if (i < segmentIdx)
                curSegment = curSegment.children;
            }

            return {
              name: nextName,
              params: curSegment.params,
              children: curSegment.children
            };
          }
          return $routeSegment;
        }];
    }]);

  /**
   * Usage:
   * <a ng-href="{{ 'index.list' | routeSegmentUrl }}">
   * <a ng-href="{{ 'index.list.itemInfo' | routeSegmentUrl: {id: 123} }}">
   */
  mod.filter('routeSegmentUrl', ['$routeSegment', function($routeSegment) {
    var filter = function(segmentName, params) {
      return $routeSegment.getSegmentUrl(segmentName, params);
    };
    filter.$stateful = true;
    return filter;
  }]);

  /**
   * Usage:
   * <li ng-class="{active: ('index.list' | routeSegmentEqualsTo)}">
   */
  mod.filter('routeSegmentEqualsTo', ['$routeSegment', function($routeSegment) {
    var filter = function(value) {
      return $routeSegment.name == value;
    };
    filter.$stateful = true;
    return filter;
  }]);

  /**
   * Usage:
   * <li ng-class="{active: ('section1' | routeSegmentStartsWith)}">
   */
  mod.filter('routeSegmentStartsWith', ['$routeSegment', function($routeSegment) {
    var filter = function(value) {
      return $routeSegment.startsWith(value);
    };
    filter.$stateful = true;
    return filter;
  }]);

  /**
   * Usage:
   * <li ng-class="{active: ('itemInfo' | routeSegmentContains)}">
   */
  mod.filter('routeSegmentContains', ['$routeSegment', function($routeSegment) {
    var filter = function(value) {
      return $routeSegment.contains(value);
    };
    filter.$stateful = true;
    return filter;
  }]);

  /**
   * Usage:
   * <li ng-class="{active: ('index.list.itemInfo' | routeSegmentEqualsTo) && ('id' | routeSegmentParam) == 123}">
   */
  mod.filter('routeSegmentParam', ['$routeSegment', function($routeSegment) {
    var filter = function(value) {
      return $routeSegment.$routeParams[value];
    };
    filter.$stateful = true;
    return filter;
  }]);

  /**
   * appViewSegment directive
   * It is based on ngView directive code:
   * https://github.com/angular/angular.js/blob/master/src/ngRoute/directive/ngView.js
   */
  angular.module('view-segment', ['route-segment']).directive('appViewSegment',
      ['$route', '$compile', '$controller', '$routeParams', '$routeSegment', '$q', '$injector', '$timeout', '$animate',
        function($route, $compile, $controller, $routeParams, $routeSegment, $q, $injector, $timeout, $animate) {

          return {
            restrict: 'ECA',
            priority: 400,
            transclude: 'element',

            compile: function(tElement, tAttrs) {
              return function($scope, element, attrs, ctrl, $transclude) {
                var currentScope, currentElement, currentSegment = {}, onloadExp = tAttrs.onload || '',
                    viewSegmentIndex = parseInt(tAttrs['appViewSegment']), updatePromise, previousLeaveAnimation;

                if ($routeSegment.chain[viewSegmentIndex]) {
                  updatePromise = $timeout(function() {
                    update($routeSegment.chain[viewSegmentIndex]);
                  }, 0);
                }
                else {
                  update();
                }

                // Watching for the specified route segment and updating contents
                $scope.$on('routeSegmentChange', function(event, args) {

                  if (updatePromise)
                    $timeout.cancel(updatePromise);

                  if (args.index == viewSegmentIndex && currentSegment != args.segment) {
                    update(args.segment);
                  }
                });

                function clearContent() {
                  if (previousLeaveAnimation) {
                    $animate.cancel(previousLeaveAnimation);
                    previousLeaveAnimation = null;
                  }

                  if (currentScope) {
                    currentScope.$destroy();
                    currentScope = null;
                  }
                  if (currentElement) {
                    previousLeaveAnimation = $animate.leave(currentElement);
                    if (previousLeaveAnimation) {
                      previousLeaveAnimation.then(function() {
                        previousLeaveAnimation = null;
                      });
                    }
                    currentElement = null;
                  }
                }

                function update(segment) {
                  currentSegment = segment;
                  var newScope = $scope.$new(),
                      clone = $transclude(newScope, function(clone) {
                        if (segment) {
                          clone.data('viewSegment', segment);
                        }
                        $animate.enter(clone, null, currentElement || element);
                        clearContent();
                      });

                  currentElement = clone;
                  currentScope = newScope;
                  currentScope.$emit('$viewContentLoaded');
                  currentScope.$eval(onloadExp);
                }
              }
            }
          };
        }]);

  angular.module('view-segment').directive('appViewSegment',
      ['$route', '$compile', '$controller', function($route, $compile, $controller) {
        return {
          restrict: 'ECA',
          priority: -400,
          link: function($scope, element) {
            var segment = element.data('viewSegment') || {},
                locals = angular.extend({}, segment.locals),
                template = locals && locals.$template;

            if (template) {
              element.html(template);
            }

            var link = $compile(element.contents());

            if (segment.params && segment.params.controller) {
              locals['$scope'] = $scope;
              var controller = $controller(segment.params.controller, locals);
              if (segment.params.controllerAs)
                $scope[segment.params.controllerAs] = controller;
              element.data('$ngControllerController', controller);
              element.children().data('$ngControllerController', controller);
            }

            link($scope);
          }
        };

      }]);

  return true;
});


define('lib/jqlite-add', [], function() {
  /*!
   * angular-ui-scroll
   * https://github.com/angular-ui/ui-scroll.git
   * Version: 1.3.1 -- 2015-08-05T13:39:04.079Z
   * License: MIT
   */
  angular.module('ui.scroll.jqlite', ['ui.scroll']).service('jqLiteExtras', [
    '$log', '$window', function(console, window) {
      return {
        registerFor: function(element) {
          var css = angular.element.prototype.css;

          function getStyle(elem) {
            return window.getComputedStyle(elem, null);
          }

          function convertToPx(elem, value) {
            return parseFloat(value);
          }

          function isWindow(obj) {
            return obj && obj.document && obj.location && obj.alert && obj.setInterval;
          }

          function scrollTo(self, direction, value) {
            var elem = self[0],
                ref = {
                  top: ['scrollTop', 'pageYOffset', 'scrollLeft'],
                  left: ['scrollLeft', 'pageXOffset', 'scrollTop']
                }[direction],
                method = ref[0],
                prop = ref[1],
                preserve = ref[2];

            if (isWindow(elem)) {
              if (angular.isDefined(value)) {
                return elem.scrollTo(self[preserve].call(self), value);
              } else {
                if (prop in elem) {
                  return elem[prop];
                } else {
                  return elem.document.documentElement[method];
                }
              }
            } else {
              if (angular.isDefined(value)) {
                return elem[method] = value;
              } else {
                return elem[method];
              }
            }
          }

          function getMeasurements(elem, measure) {
            var base, borderA, borderB, computedMarginA, computedMarginB, computedStyle, dirA, dirB, marginA, marginB, paddingA, paddingB, ref;
            if (isWindow(elem)) {
              base = document.documentElement[{
                height: 'clientHeight',
                width: 'clientWidth'
              }[measure]];
              return {
                'base': base,
                'padding': 0,
                'border': 0,
                'margin': 0
              };
            }
            ref = {
              width: [elem.offsetWidth, 'Left', 'Right'],
              height: [elem.offsetHeight, 'Top', 'Bottom']
            }[measure], base = ref[0], dirA = ref[1], dirB = ref[2];
            computedStyle = getStyle(elem);
            paddingA = convertToPx(elem, computedStyle['padding' + dirA]) || 0;
            paddingB = convertToPx(elem, computedStyle['padding' + dirB]) || 0;
            borderA = convertToPx(elem, computedStyle['border' + dirA + 'Width']) || 0;
            borderB = convertToPx(elem, computedStyle['border' + dirB + 'Width']) || 0;
            computedMarginA = computedStyle['margin' + dirA];
            computedMarginB = computedStyle['margin' + dirB];
            marginA = convertToPx(elem, computedMarginA) || 0;
            marginB = convertToPx(elem, computedMarginB) || 0;
            return {
              'base': base,
              'padding': paddingA + paddingB,
              'border': borderA + borderB,
              'margin': marginA + marginB
            };
          }

          function getWidthHeight(elem, direction, measure) {
            var measurements = getMeasurements(elem, direction),
                computedStyle,
                result;

            if (measurements.base > 0) {
              return {
                'base': measurements.base - measurements.padding - measurements.border,
                'outer': measurements.base,
                'outerfull': measurements.base + measurements.margin
              }[measure];
            } else {
              computedStyle = getStyle(elem);
              result = computedStyle[direction];
              if (result < 0 || result === null) {
                result = elem.style[direction] || 0;
              }
              result = parseFloat(result) || 0;
              return {
                'base': result - measurements.padding - measurements.border,
                'outer': result,
                'outerfull': result + measurements.padding + measurements.border + measurements.margin
              }[measure];
            }
          }

          element.prototype.css = function(name, value) {
            var elem, self;
            self = this;
            elem = self[0];
            if (!(!elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style)) {
              return css.call(self, name, value);
            }
          };

          return angular.forEach({

            before: function(newElem) {
              var children, elem, i, j, parent, ref, self;
              self = this;
              elem = self[0];
              parent = self.parent();
              children = parent.contents();
              if (children[0] === elem) {
                return parent.prepend(newElem);
              } else {
                for (i = j = 1, ref = children.length - 1; 1 <= ref ? j <= ref : j >= ref; i = 1 <= ref ? ++j : --j) {
                  if (children[i] === elem) {
                    angular.element(children[i - 1]).after(newElem);
                    return;
                  }
                }
                throw new Error('invalid DOM structure ' + elem.outerHTML);
              }
            },

            height: function(value) {
              var self;
              self = this;
              if (angular.isDefined(value)) {
                if (angular.isNumber(value)) {
                  value = value + 'px';
                }
                return css.call(self, 'height', value);
              } else {
                return getWidthHeight(this[0], 'height', 'base');
              }
            },

            outerHeight: function(option) {
              return getWidthHeight(this[0], 'height', option ? 'outerfull' : 'outer');
            },

            /*
             The offset setter method is not implemented
             */
            offset: function(value) {
              var box, doc, docElem, elem, self, win;
              self = this;
              if (arguments.length) {
                if (value === void 0) {
                  return self;
                } else {
                  throw new Error('offset setter method is not implemented');
                }
              }
              box = {
                top: 0,
                left: 0
              };
              elem = self[0];
              doc = elem && elem.ownerDocument;
              if (!doc) {
                return;
              }
              docElem = doc.documentElement;
              if (elem.getBoundingClientRect != null) {
                box = elem.getBoundingClientRect();
              }
              win = doc.defaultView || doc.parentWindow;
              return {
                top: box.top + (win.pageYOffset || docElem.scrollTop) - (docElem.clientTop || 0),
                left: box.left + (win.pageXOffset || docElem.scrollLeft) - (docElem.clientLeft || 0)
              };
            },

            scrollTop: function(value) {
              return scrollTo(this, 'top', value);
            },

            scrollLeft: function(value) {
              return scrollTo(this, 'left', value);
            }
          }, function(value, key) {
            if (!element.prototype[key]) {
              return element.prototype[key] = value;
            }
          });
        }
      };
    }
  ]).run([
    '$log', '$window', 'jqLiteExtras', function(console, window, jqLiteExtras) {
      return jqLiteExtras.registerFor(angular.element);
    }
  ])});


define('app',
    ['app/RouteResolver',
      'app/controllers/AppController',
      'app/controllers/LoginController',
      'app/controllers/RegisterController',
      'app/controllers/MyBooksController',
      'app/controllers/GenresController',
      'app/controllers/GenreBooksController',
      'app/controllers/LanguagesController',
      'app/controllers/LanguageBooksController',
      'app/controllers/BookDetailsController',
      'app/controllers/NewBooksController',
      'app/controllers/SearchController',
      'app/controllers/ReaderController',
      'app/core/ListController',
      'i18n/translations',
      'app/factories/BookFactory',
      'app/services/SessionService',
      'app/services/AuthService',
      'app/modules/api/ApiService',
      'app/directives/compareTo',
      'app/directives/epub-reader',
      'app/directives/slider',
      'app/modules/api',
      'lib/local-forage',
      'lib/ui-scroll',
      'lib/RouteSegment',
      'lib/jqlite-add'
    ],
    function(RouteResolver,
             ApplicationController,
             LoginController,
             RegisterController,
             MyBooksController,
             GenresController,
             GenreBooksController,
             LanguagesController,
             LanguageBooksController,
             BookDetailsController,
             NewBooksController,
             SearchController,
             ReaderController,
             ListController,
             i18n,
             BookFactory,
             SessionService,
             AuthService,
             ApiService,
             CompareD,
             EpubReaderD,
             SliderD,
             API,
             LOCALFORAGE,
             UISCROLL) {

      function routerConfig($routeSegmentProvider, $routeProvider) {
        $routeSegmentProvider
          .when('/', 'root', {redirectTo: '/nav/shelf'})
          .when('/login', 'login', {public: true})
            .segment('login', {
              templateUrl: 'login',
              controller: LoginController
            })
          .when('/nav/registration', 'nav.registration', {public: true})
          .when('/nav/forgot', 'nav.forgot', {public: true})
          .when('/nav/shelf', 'nav.shelf')
          .when('/nav/new', 'nav.new')
          .when('/nav/genres', 'nav.genres')
          .when('/nav/languages', 'nav.languages')
          .when('/nav/languages/:id', 'nav.languagebooks')
          .when('/nav/genres/:id', 'nav.genrebooks')
          .when('/nav/book/:id', 'nav.bookinfo')
          .when('/nav/book/:id/preview', 'nav.bookpreview')
          .when('/nav/book/:id/buy', 'nav.bookbuy')
          .when('/nav/book/:id/read', 'nav.bookread')
          .when('/nav/search', 'nav.search')
          .segment('nav', {
              templateUrl: 'nav'
            })
            .within()
              .segment('registration', {
                  templateUrl: 'registration',
                  controller: RegisterController
                })
              .segment('forgot', {
                  templateUrl: 'forgot'
                })
              .segment('shelf', {
                  templateUrl: 'mybooks',
                  controller: MyBooksController
                })
              .segment('genres', {
                  templateUrl: 'genres',
                  controller: GenresController,
                  resolve: RouteResolver.GENRES,
                  untilResolved: { template: '' }
                })
              .segment('new', {
                  templateUrl: 'newbooks',
                  controller: NewBooksController
                })
              .segment('genrebooks', {
                  templateUrl: 'dynamicbooks',
                  controller: GenreBooksController,
                  resolve: RouteResolver.GENREBOOKS,
                  untilResolved: {
                    template: ''
                  },
                  dependencies: ['id']
                })
                .segment('languages', {
                  templateUrl: 'languages',
                  controller: LanguagesController,
                  resolve: RouteResolver.LANGUAGES,
                  untilResolved: {
                    template: ''
                  }
                })
                .segment('languagebooks', {
                  templateUrl: 'dynamicbooks',
                  controller: LanguageBooksController,
                  resolve: RouteResolver.LANGUAGEBOOKS,
                  untilResolved: {
                    template: ''
                  },
                  dependencies: ['id']
                })
                .segment('bookinfo', {
                  templateUrl: 'bookinfo',
                  controller: BookDetailsController,
                  dependencies: ['id']
                })
                .segment('search', {
                  controller: SearchController,
                  templateUrl: 'search'
                })
                .segment('bookread', {
                  templateUrl: 'reader',
                  controller: ReaderController,
                  dependencies: ['id']
                });

      }

      function runTextCatalogBlock(getTextCatalog) {
        angular.forEach(i18n, function(value, key) {
          getTextCatalog['currentLanguage'] = 'sw'; //TODO: get from somewhere
          getTextCatalog['debug'] = false;
          getTextCatalog['setStrings'](key, value);
        });
      }

      function preventRouting($rootScope, $location, $authService) {
        $rootScope.$on('$routeChangeStart', function(event, next, current) {
          if (!next) return;
          var nextSegment = next.segment,
              currentSegment = current ? current.segment : '',
              isPublic = next['$$route'].public || false;

          if (currentSegment && nextSegment === 'root' && currentSegment !== nextSegment) {
            event.preventDefault();
            return;
          }

          if (!isPublic) {
            var isLoggedIn = $authService.isLoggedIn();
            if (isLoggedIn !== true) {
              event.preventDefault();
              isLoggedIn.then(
                  function(success) {
                    $rootScope.$broadcast('$routeChangeStart', next, current);
                  },
                  function(error) {
                    $location.path('/login');
                  });
            }
          }
        });
      }

      preventRouting['$inject'] = ['$rootScope', '$location', AuthService.fullName];

      var app = angular.module('app', ['ng',
                                       'ngRoute',
                                       'ngTouch',
                                       LOCALFORAGE.name,
                                       API.name,
                                       UISCROLL.name,
                                       'route-segment',
                                       'view-segment',
                                       'gettext',
                                       'ui.scroll.jqlite']);

      app
        .factory(BookFactory.fullName, [ApiService.fullName, '$localForage', '$q', BookFactory])
        .service(AuthService.fullName, AuthService)
        .service(SessionService.fullName, SessionService)
        .directive('compareTo', CompareD)
        .directive(EpubReaderD.fullName, EpubReaderD)
        .directive(SliderD.fullName, SliderD)
        .run(preventRouting)
        .run(['gettextCatalog', runTextCatalogBlock])
        .config(['$routeSegmentProvider', '$routeProvider', routerConfig])
        .config(['$localForageProvider', function($localForageProvider) {
            $localForageProvider.config({
              //driver: 'localStorageWrapper', // if you want to force a driver
              'name': 'mooqla', // name of the database and prefix for your data, it is "lf" by default
              //version: 1.0, // version of the database, you shouldn't have to use this
              'storeName': 'mstore' // name of the table
              //description: 'some description'
            });
          }])
        .controller(ApplicationController.fullName, ApplicationController);

      return app;

    }
);

define("app.templates", ["app"], function(){angular.module("app").run(["$templateCache", function($templateCache) {  'use strict';

  $templateCache.put('bookinfo',
    "<div id=\"book-details\" class=\"page-container container\" ng-if=\"::book.id\"><section class=\"book-details hairline-border-bottom\"><div class=\"book-field\"><div class=\"book-cover\"><img ng-src=\"{{book.thumb}}\"></div><div class=\"book-info\"><h2>{{::book.title}}</h2><h3>{{::book.author}}</h3><div class=\"button\"><button class=\"btn btn-buy round blue\" translate>Preview</button> <button class=\"btn btn-buy round green\" translate ng-click=\"navigate.buyBook(book)\">Buy</button></div></div></div><p><strong translate>Number of pages:</strong> {{::book.pages}}</p></section><section class=\"genres hairline-border-bottom\"><h1>Genres</h1><ul class=\"taglist hlist\"><li ng-repeat=\"tag in book.genres\">{{tag}}</li></ul></section><section class=\"handling hairline-border-bottom\" ng-if=\"::book.summary\"><h1 translate>Summary</h1><p ng-if=\"book.shortSummary\">{{book.shortSummary}} <a href=\"#\" class=\"blue no-underline\" ng-click=\"showSummary()\"><strong translate>More</strong></a></p><p ng-if=\"::book.fullSummary\">{{book.fullSummary}}</p></section><section class=\"bookshelf\"><h1 translate>Similar books</h1><div class=\"book-field hairline-border-bottom\" ng-repeat=\"book in book.similar\" ng-click=\"navigate.showBook(book)\"><div class=\"book-cover\"><img ng-src=\"{{::book.thumb}}\"></div><div class=\"book-info\"><h2>{{::book.title}}</h2><h3>{{::book.author}}</h3><div class=\"button\"><button class=\"btn btn-buy round\" ng-click=\"navigate.buyBook(book, $event)\">{{::book.price}}:-</button></div></div></div></section></div>"
  );


  $templateCache.put('booklist',
    "<section class=\"bookshelf\"><div class=\"book-field hairline-border-bottom\" ui-scroll=\"item in provider\" adapter=\"booksAdapter\" padding=\"0\" ng-click=\"addToShelf ? addToShelf(item) : navigate.showBook(item)\"><div class=\"book-cover\"><img ng-src=\"{{item.thumb}}\"></div><div class=\"book-info\"><h2>{{::item.title}}</h2><h3>{{::item.author}}</h3><div class=\"button\" ng-if=\"::item.price\"><button class=\"btn btn-buy round\" ng-click=\"navigate.buyBook(item, $event)\">{{::item.price}}:-</button></div></div></div></section>"
  );


  $templateCache.put('checkout',
    ""
  );


  $templateCache.put('dynamicbooks',
    "<div id=\"bookshelf\" class=\"page-container container\" ui-scroll-viewport><h1>{{::title}}</h1><ng-include src=\"'booklist'\"></ng-include></div>"
  );


  $templateCache.put('epub',
    "<div id=\"ebook\"><div class=\"info author\"></div><div class=\"info page\" ng-if=\"bookData.book.ready\">{{bookData.book.currentPage}} / {{::bookData.book.totalPages}}</div><div class=\"slider-container\" epub-slider></div><div class=\"slider-info\"><div class=\"chapter\"></div><div class=\"number\"></div></div></div>"
  );


  $templateCache.put('forgot',
    "<div id=\"restore-pwd\" class=\"page-container container\"><section class=\"login-register\"><h1 translate>Reset password</h1><h2 translate>If you have lost your password you can request that a new password be assigned to your account and e-mailed to you by filling out the following form:</h2><form novalidate method=\"post\"><div class=\"form-field\"><input class=\"hairline-border\" type=\"email\" placeholder=\"{{'Enter email'|translate}}\"></div><div class=\"form-field button-field\"><button class=\"btn btn-green w100 round\" type=\"submit\" translate>Continue</button></div></form></section></div>"
  );


  $templateCache.put('genres',
    "<div id=\"genres\" class=\"page-container container\" ui-scroll-viewport><h1 translate>Genres</h1><section class=\"genres-list link hairline-border-bottom\" ui-scroll=\"item in provider\" padding=\"0\" ng-click=\"navigate.showGenreBooks(item)\"><p><strong>{{item.title}}</strong></p><span>{{item.quantity}}</span></section></div>"
  );


  $templateCache.put('languages',
    "<div id=\"languages\" class=\"page-container container\" ui-scroll-viewport><h1 translate>Languages</h1><section class=\"genres-list link hairline-border-bottom\" ui-scroll=\"item in provider\" padding=\"0\" ng-click=\"navigate.showLangBooks(item)\"><p><strong>{{item.title}}</strong></p><span>{{item.quantity}}</span></section></div>"
  );


  $templateCache.put('login',
    "<div id=\"pages-container\" class=\"container\"><div id=\"start\" class=\"page-container container\"><div class=\"logo-container\"><div class=\"logo\"></div></div><form name=\"loginForm\" novalidate method=\"post\" ng-submit=\"login(credentials)\"><div class=\"form-field\" ng-class=\"{'highlight': loginForm.email.$dirty && loginForm.email.$touched, 'has-success': loginForm.email.$valid, 'has-error': !loginForm.email.$valid}\"><input name=\"email\" ng-model=\"credentials.email\" class=\"hairline-border\" type=\"email\" placeholder=\"{{'Enter email'|translate}}\" required></div><div class=\"form-field\" ng-class=\"{'highlight': loginForm.password.$dirty && loginForm.password.$touched, 'has-success': loginForm.password.$valid, 'has-error': !loginForm.password.$valid}\"><input name=\"password\" ng-model=\"credentials.password\" class=\"hairline-border\" type=\"password\" placeholder=\"{{'Password'|translate}}\" required></div><div class=\"form-field button-field\"><button class=\"btn btn-green w100\" type=\"submit\" translate>Sign in</button></div></form><br><a class=\"text-centered wb100 white\" href=\"#/nav/forgot\" translate>Forgot your password?</a><br><a class=\"text-centered wb100 white uppercase no-underline\" href=\"#/nav/registration\" translate>Sign up</a></div></div>"
  );


  $templateCache.put('mybooks',
    "<div id=\"main-bookshelf\" class=\"page-container container\" ui-scroll-viewport><section class=\"chosen-books\"><div class=\"book-cover\" ng-class=\"{empty: !book.thumb}\" ng-repeat=\"book in bookShelf\" ng-click=\"navigate.readBook(book)\"><img ng-if=\"book.thumb\" ng-src=\"{{book.thumb}}\"></div></section><h1 translate>Your books</h1><ng-include src=\"'booklist'\"></ng-include></div>"
  );


  $templateCache.put('nav',
    "<div id=\"app-menu\" ng-class=\"{'opened': menuState.opened}\"><div class=\"search\"><input class=\"hairline-border\" type=\"search\" placeholder=\"{{'Search'|translate}}\" ng-blur=\"searchString.length ? navigate.search(searchString) : ''\" ng-model=\"searchString\"></div><ul class=\"vlist\"><li class=\"books hairline-border-bottom\" translate ng-click=\"navigate.home()\">My books</li><li class=\"genres hairline-border-bottom\" translate ng-click=\"navigate.genres()\">Genres</li><li class=\"new-books hairline-border-bottom\" translate ng-click=\"navigate.new()\">New books</li><li class=\"book-language hairline-border-bottom\" translate ng-click=\"navigate.langs()\">Books by language</li></ul></div><div id=\"pages-container\" class=\"container\"><nav id=\"navbar-main\"><ul class=\"hlist\"><li id=\"nav-menu\" ng-click=\"toggleMenu()\"></li><li id=\"nav-reader\"></li><li id=\"nav-settings\"></li><li id=\"nav-seek\" ng-click=\"navigate.search()\"></li><li id=\"nav-home\" ng-click=\"navigate.home()\"></li></ul></nav><div app-view-segment=\"1\"></div></div>"
  );


  $templateCache.put('newbooks',
    "<div id=\"bookshelf\" class=\"page-container container\" ui-scroll-viewport><h1 translate>New books</h1><ng-include src=\"'booklist'\"></ng-include></div>"
  );


  $templateCache.put('reader',
    "<div id=\"book-reader\" class=\"page-container container\"><span ng-if=\"loadStatus.loading\">Loaded {{loadStatus.progress}} %</span><div class=\"content-container\" epub-reader epub-source=\"bookData.book.epubBlob\"></div></div>"
  );


  $templateCache.put('registration',
    "<div id=\"register\" class=\"page-container container\"><section class=\"login-register\"><h1 translate>Create an account</h1><h2 translate>Register at Booqlas webapp and take advantage of all the books that are available. Your active books are available free online.</h2><form name=\"registerForm\" novalidate method=\"post\" ng-submit=\"register(regInfo)\"><div class=\"form-field\" ng-class=\"{'highlight': registerForm.email.$dirty && registerForm.email.$touched, 'has-success': registerForm.email.$valid, 'has-error': !registerForm.email.$valid}\"><input name=\"email\" class=\"hairline-border\" type=\"email\" placeholder=\"{{'Enter email'|translate}}\" ng-model=\"regInfo.email\" required></div><div class=\"form-field\" ng-class=\"{'highlight': registerForm.password.$dirty && registerForm.password.$touched, 'has-success': registerForm.password.$valid, 'has-error': !registerForm.password.$valid}\"><input name=\"password\" class=\"hairline-border\" type=\"password\" placeholder=\"{{'Password'|translate}}\" ng-model=\"regInfo.password\" required></div><div class=\"form-field\" ng-class=\"{'highlight': registerForm.password1.$dirty && registerForm.password1.$touched, 'has-success': registerForm.password1.$valid, 'has-error': !registerForm.password1.$valid}\"><input name=\"password1\" class=\"hairline-border\" type=\"password\" placeholder=\"{{'Repeat password'|translate}}\" ng-model=\"regInfo.password1\" required compare-to=\"regInfo.password\"></div><div class=\"form-field\" ng-class=\"{'highlight': registerForm.country.$dirty && registerForm.country.$touched, 'has-success': registerForm.country.$valid, 'has-error': !registerForm.country.$valid}\"><div class=\"select-field\"><div class=\"select-arrows\"></div><select name=\"country\" class=\"hairline-border\" ng-model=\"regInfo.country\" required convert-to-number><option value=\"\" disabled selected translate>Your country</option><option value=\"1\">Something</option><option value=\"2\">Something 2</option></select></div></div><div class=\"form-field button-field\"><button class=\"btn btn-green w100 round\" type=\"submit\" translate>Continue</button></div></form></section></div>"
  );


  $templateCache.put('search',
    "<div id=\"search\" class=\"page-container container\"><section class=\"search\"><h1 translate>Search</h1><h2 translate>Search book titles, author, ISBN number or take you on via some of our most popular searches below.</h2><div class=\"search\"><input class=\"hairline-border\" type=\"search\" placeholder=\"{{'Search'|translate}}\" ng-model=\"searchString\" ng-blur=\"searchString.length ? search(searchString) :''\"> <button class=\"btn\" translate>Search</button></div></section><ng-include src=\"'booklist'\"></ng-include><section class=\"search-popular\" ng-if=\"similar.length\"><h1 translate>Popular searches</h1><div><a ng-click=\"search(request)\" ng-repeat=\"request in similar\">{{::request}}</a></div></section></div>"
  );


  $templateCache.put('slider',
    "<div class=\"fixer\"></div><div class=\"slider\"><div class=\"viewed\"></div><div class=\"handler\"></div></div>"
  );
}]); return true});

define('main-dev', ['app', 'app.templates'], function(app) {

  var genresData = [{'id': 0, 'title': 'Action', 'quantity': 17},
        {'id': 1, 'title': 'Classic', 'quantity': 10},
        {'id': 2, 'title': 'Comic/Graphic Novel', 'quantity': 4},
        {'id': 3, 'title': 'Crime/Detective', 'quantity': 25},
        {'id': 4, 'title': 'Fable', 'quantity': 3},
        {'id': 5, 'title': 'Fairy tale', 'quantity': 44},
        {'id': 6, 'title': 'Fanfiction', 'quantity': 28},
        {'id': 7, 'title': 'Fantasy', 'quantity': 12},
        {'id': 8, 'title': 'Folklore', 'quantity': 34},
        {'id': 9, 'title': 'Historical fiction', 'quantity': 14},
        {'id': 10, 'title': 'Horror', 'quantity': 22},
        {'id': 11, 'title': 'Humor', 'quantity': 1},
        {'id': 12, 'title': 'Legend', 'quantity': 34},
        {'id': 13, 'title': 'Crime/Detective', 'quantity': 33},
        {'id': 14, 'title': 'Magical Realism', 'quantity': 67},
        {'id': 15, 'title': 'Mystery', 'quantity': 11},
        {'id': 16, 'title': 'Mythology', 'quantity': 5},
        {'id': 17, 'title': 'Science fiction', 'quantity': 32},
        {'id': 18, 'title': 'Short story', 'quantity': 19},
        {'id': 19, 'title': 'Tall tale', 'quantity': 27},
        {'id': 20, 'title': 'Western', 'quantity': 56}
      ],
      langData = [
        {'id': 0, 'title': 'English', 'quantity': 100},
        {'id': 1, 'title': 'Russian', 'quantity': 5},
        {'id': 2, 'title': 'Swedish', 'quantity': 5},
        {'id': 3, 'title': 'Some other', 'quantity': 24}
      ],
      myBooksData = [
        {'id': 14, 'title': 'MyBook 1', 'author': 'Some author 1', 'thumb': 'i/book-cover-1.jpg', 'status': ''},
        {'id': 15, 'title': 'MyBook 2', 'author': 'Some author 1', 'thumb': 'i/book-cover-2.jpg', 'status': ''},
        {'id': 16, 'title': 'MyBook 3', 'author': 'Some author 5', 'thumb': 'i/bc3.jpg', 'status': ''},
        {'id': 17, 'title': 'MyBook 4', 'author': 'Some author 1', 'thumb': 'i/book-cover-1.jpg', 'status': ''},
        {'id': 18, 'title': 'MyBook 5', 'author': 'Some author 1', 'thumb': 'i/bc5.jpg', 'status': ''},
        {'id': 19, 'title': 'MyBook 6', 'author': 'Some author 2', 'thumb': 'i/book-cover-2.jpg', 'status': ''},
        {'id': 20, 'title': 'MyBook 7', 'author': 'Some author 1', 'thumb': 'i/book-cover-1.jpg', 'status': ''},
        {'id': 21, 'title': 'MyBook 8', 'author': 'Some author 1', 'thumb': 'i/bc1.jpg', 'status': ''},
        {'id': 22, 'title': 'MyBook 9', 'author': 'Some author 5', 'thumb': 'i/bc2.jpg', 'status': ''},
        {'id': 23, 'title': 'MyBook 10', 'author': 'Some author 6', 'thumb': 'i/book-cover-1.jpg', 'status': ''},
        {'id': 24, 'title': 'MyBook 11', 'author': 'Some author 1', 'thumb': 'i/bc4.jpg', 'status': ''},
        {'id': 25, 'title': 'MyBook 12', 'author': 'Some author 8', 'thumb': 'i/book-cover-1.jpg', 'status': ''}
      ],
      bookListData = [
        {'id': 0, 'title': 'Cognitive Cooking with chef Watson', 'author': 'Gunn, Deana, Miniati, Wona', 'thumb': 'i/bc1.jpg', 'status': '', 'price': 123},
        {'id': 1, 'title': 'The Kindergarten Wars', 'author': 'Eisenstock, Alan', 'thumb': 'i/bc2.jpg', 'status': '', 'price': 120},
        {'id': 2, 'title': 'Go Set a Watchman', 'author': 'Lee, Harper', 'thumb': 'i/bc3.jpg', 'status': '', 'price': 100},
        {'id': 3, 'title': 'The Girl on the Train', 'author': 'Hawkins, Paula', 'thumb': 'i/bc4.jpg', 'status': '', 'price': 200},
        {'id': 4, 'title': 'Luckiest Girl Alive', 'author': 'Knoll, Jessica', 'thumb': 'i/bc5.jpg', 'status': '', 'price': 200},
        {'id': 5, 'title': 'Everything I Never Told You', 'author': 'Ng, Celeste', 'thumb': 'i/bc6.jpg', 'status': '', 'price': 123},
        {'id': 6, 'title': 'Dummy title-6', 'author': 'Javascript', 'thumb': 'i/book-cover-1.jpg', 'status': '', 'price': 120},
        {'id': 7, 'title': 'Dummy title-7', 'author': 'Lee, Harper', 'thumb': 'i/book-cover-2.jpg', 'status': '', 'price': 100},
        {'id': 8, 'title': 'Dummy title-8', 'author': 'Hawkins, Paula', 'thumb': 'i/book-cover-1.jpg', 'status': '', 'price': 200},
        {'id': 9, 'title': 'Dummy title-9', 'author': 'Knoll, Jessica', 'thumb': 'i/book-cover-2.jpg', 'status': '', 'price': 200},
        {'id': 10, 'title': 'Dummy title-10', 'author': 'Eisenstock, Alan', 'thumb': 'i/book-cover-1.jpg', 'status': '', 'price': 120},
        {'id': 11, 'title': 'Dummy title-11', 'author': 'Lee, Harper', 'thumb': 'i/book-cover-2.jpg', 'status': '', 'price': 100},
        {'id': 12, 'title': 'Dummy title-12', 'author': 'Hawkins, Paula', 'thumb': 'i/book-cover-1.jpg', 'status': '', 'price': 200},
        {'id': 13, 'title': 'Dummy title-13', 'author': 'Knoll, Jessica', 'thumb': 'i/book-cover-2.jpg', 'status': '', 'price': 200},
        {'id': 14, 'title': 'Dummy title-14', 'author': 'Gunn, Deana, Miniati, Wona', 'thumb': 'i/book-cover-1.jpg', 'status': '', 'price': 123},
        {'id': 15, 'title': 'Dummy title-15', 'author': 'Eisenstock, Alan', 'thumb': 'i/book-cover-2.jpg', 'status': '', 'price': 120},
        {'id': 16, 'title': 'Dummy title-16', 'author': 'Lee, Harper', 'thumb': 'i/bc3.jpg', 'status': '', 'price': 100},
        {'id': 17, 'title': 'Dummy title-17', 'author': 'Hawkins, Paula', 'thumb': 'i/bc4.jpg', 'status': '', 'price': 200},
        {'id': 18, 'title': 'Dummy title-18', 'author': 'Knoll, Jessica', 'thumb': 'i/bc5.jpg', 'status': '', 'price': 200},
        {'id': 19, 'title': 'Dummy title-19', 'author': 'Hawkins, Paula', 'thumb': 'i/bc4.jpg', 'status': '', 'price': 200},
        {'id': 20, 'title': 'Dummy title-20', 'author': 'Knoll, Jessica', 'thumb': 'i/bc5.jpg', 'status': '', 'price': 200},
        {'id': 21, 'title': 'Dummy title-21', 'author': 'Gunn, Deana, Miniati, Wona', 'thumb': 'i/bc1.jpg', 'status': '', 'price': 123},
        {'id': 22, 'title': 'Dummy title-22', 'author': 'Eisenstock, Alan', 'thumb': 'i/bc2.jpg', 'status': '', 'price': 120},
        {'id': 23, 'title': 'Dummy title-23', 'author': 'Lee, Harper', 'thumb': 'i/bc3.jpg', 'status': '', 'price': 100},
        {'id': 24, 'title': 'Dummy title-24', 'author': 'Hawkins, Paula', 'thumb': 'i/bc4.jpg', 'status': '', 'price': 200},
        {'id': 25, 'title': 'Dummy title-25', 'author': 'Knoll, Jessica', 'thumb': 'i/bc5.jpg', 'status': '', 'price': 200}
      ],
      bookInfoData = {
        'id': '0',
        'title': 'Cognitive Cooking with chef Watson',
        'author': 'Gunn, Deana, Miniati, Wona',
        'thumb': 'i/bc1.jpg',
        'status': '',
        'price': 123,
        'pages': 256,
        'genres': ['Action', 'Detective', 'Fantasy'],
        'summary': 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?',
        'similar': [
          {'id': 2, 'title': 'Go Set a Watchman', 'author': 'Lee, Harper', 'thumb': 'i/bc3.jpg', 'status': '', 'price': 100},
          {'id': 3, 'title': 'The Girl on the Train', 'author': 'Hawkins, Paula', 'thumb': 'i/bc4.jpg', 'status': '', 'price': 200},
          {'id': 4, 'title': 'Luckiest Girl Alive', 'author': 'Knoll, Jessica', 'thumb': 'i/bc5.jpg', 'status': '', 'price': 200}
        ]
      },
      searchData = {
        'result': bookListData,
        'similar': ['Something', 'How can I delete account?']
      };

  app.requires.push('ngMockE2E');

  app.config(['$provide', function($provide) {
    $provide.decorator('$httpBackend', ['$delegate', function($delegate) {
      var proxy = function(method, url, post, callback, headers, timeout, withCredentials, responseType) {
        var interceptor = function() {
          var _this = this,
              _arguments = arguments;
          setTimeout(function() {
            callback.apply(_this, _arguments);
          }, 700);
        };
        return $delegate.call(this, method, url, post, interceptor, headers, timeout, withCredentials, responseType);
      };
      for (var key in $delegate) {
        proxy[key] = $delegate[key];
      }
      return proxy;
    }]);
  }]);

  app.run(['$httpBackend', function($httpBackend) {
    //$httpBackend.whenGET(/tmpl/)['passThrough']();
    $httpBackend.whenGET(/.jpg/)['passThrough']();
    $httpBackend.whenGET(/.epub/)['passThrough']();
    $httpBackend.whenPOST(/ajax.php/).respond(function(method, url, data) {
      var response = {'result': true, 'data': '', 'token': null, 'errorCode': null, 'errorMessage': null},
          code = 200;
      data = JSON.parse(data);
      switch (data['ajaxCall']) {
        case 'authGetToken': response['data'] = 'f2ba10a9cfee436e85e119fc8bf25540'; break;
        //case 'getSession': response['data'] = {'userID': '123', 'email': 'e@gmail.com'}; break;
        case 'getSession': response = {'result': false, 'data': '', 'token': null, 'errorCode': 401, 'errorMessage': 'Not logged in'}; break;
        case 'getGenresList':
          var result;
          if (data.start >= genresData.length) result = [];
          else result = genresData.slice(data.start, data.end);
          response['token'] = 'f2ba10a9cfee436e85e119fc8bf25540';
          response['data'] = genresData;
          break;
        case 'getBooksByGenre':
        case 'getNewBooks':
        case 'getBooks':
          var result;
          if (data.start && data.start >= bookListData.length) result = [];
          else result = bookListData.slice(data.start, data.end);
          response['token'] = 'f2ba10a9cfee436e85e119fc8bf25540';
          response['data'] = result;
          break;
        case 'getUserBooks':
          var result;
          if (data.start && data.start >= myBooksData.length) result = [];
          else result = myBooksData.slice(data.start, data.end);
          response['token'] = 'f2ba10a9cfee436e85e119fc8bf25540';
          response['data'] = result;
          break;
        case 'getAvailableLanguagesForBooks':
          response['token'] = 'f2ba10a9cfee436e85e119fc8bf25540';
          response['data'] = langData;
          break;
        case 'getBookDetails':
          response['token'] = 'f2ba10a9cfee436e85e119fc8bf25540';
          response['data'] = bookInfoData;
          break;
        case 'getSearchResults':
          response['token'] = 'f2ba10a9cfee436e85e119fc8bf25540';
          response['data'] = searchData;
          break;
        default: response = {}; code = 404;
      }

      return [code, response];
    });

    $httpBackend.whenPOST(/forms.php/).respond(function(method, url, data) {
      var response = {'result': true, 'data': '', 'token': null, 'errorCode': null, 'errorMessage': null},
          code = 200;
      data = JSON.parse(data);
      switch (data['formName']) {
        case 'g-themes-webapp-login': response['token'] = 'f2ba10a9cfee436e85e119fc8bf25541'; break;
        case 'g-themes-webapp-register': response['token'] = 'f2ba10a9cfee436e85e119fc8bf25542'; break;
        default: code = 404; response = {};
      }

      return [code, response];
    });

  }]);

  angular.bootstrap(window.document, ['app'], {strictDi: true, debugInfoEnabled: true});
});


}());
